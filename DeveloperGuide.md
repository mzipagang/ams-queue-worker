# Developer Guide - Queue Worker

Getting Started
---------

Prerequisites:
 - Node.js = 8.9, It is recommended that you use [NVM](https://github.com/creationix/nvm).
 - [Git](https://git-scm.com/)
 - MongoDB >= 3.4.

Clone the codebase and initialize it by running the following commands:
```shell
$ git clone https://github.cms.gov/CMMI/AMS-queue-worker
$ cd AMS-queue-worker
$ npm install
```

#### For development

Start up the app
```shell
npm run start:dev
```
Note: `start:dev` uses nodemon to automatically restart your node service when files change.

#### For production
```shell
export NODE_ENV=production
export AWS_ACCESS_KEY_ID="youraccesskeyid"
export AWS_SECRET_ACCESS_KEY="yoursecretaccesskey"
npm run start
```


Environment variables
---------

Configuration of the app is based entirely on environment variables. The app does not store any variables for any environment at all. All configuration from environment variables is applied in `./app/config/index.js`.

| Environment Variable  | Default                                   | Options                                             |
| --------------------- | ----------------------------------------- | --------------------------------------------------- |
| NODE_ENV              | `"dev"`                                   | `production`, `dev`, or any other environment name  |
| AWS_ACCESS_KEY_ID     | `""`                                      | Access Key ID for AWS                               |
| AWS_SECRET_ACCESS_KEY | `""`                                      | Secret Access Key for AWS                           |
| SQS_QUEUE_URL         | `""`                                      | SQS URL for processing file uploads                 |
| API_URL               | `"http://localhost:5000"`                 | URL to the API worker                               |

Note that development values are used when both `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` are not set. However, you can always override the development values by setting each environment variable as needed.

If `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` are not set:

| Environment Variable  | Development Default                       |
| --------------------- | ----------------------------------------- |
| AWS_ACCESS_KEY_ID     | `"id"`                                    |
| AWS_SECRET_ACCESS_KEY | `"key"`                                   |
| SQS_QUEUE_URL         | `"http://0.0.0.0:4100/ams-uploads-queue"` |


Tests
---------
```
npm run test
```
AMS-queue-worker is using Mocha to run tests. The test pattern is `test/**/*.spec.js` and is using Mocha's `bdd` interface. Feel free to use any Mocha runner to run the tests.


Lint
---------
```
npm run lint
```
The linting rules are based on Airbnb's Javascript Style Guide which can be found here: https://github.com/airbnb/javascript. Please run the linter before each commit.


Code Coverage
---------
```
npm run coverage:dev
```
`npm run coverage:dev` will automatically open the generated HTML file in your browser. For CI, run `npm run coverage` to generate but not open. The generated docs will be available at `./coverage/lcov-report/index.html`.


Additional Notes
---------

#### Accessing GitHub w/ 2FA
Because CMS GitHub requires 2FA, you will need to checkout the repository using HTTPS, your username, and a Personal Access Token. Go to Settings -> Personal Access Tokens to create a Personal Access Token with "repo" access. Use this password, along with your GitHub username to access the repo. [More Info](https://help.github.com/articles/providing-your-2fa-authentication-code/#when-youll-be-asked-for-a-personal-access-token-as-a-password)
