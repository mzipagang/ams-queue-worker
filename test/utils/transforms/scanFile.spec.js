const checkFileForWarningTransform =
  require('../../../app/utils/transforms/scanFile');

const { expect } = require('chai');
const through = require('through2');

describe('checkFileForWarningTransform', () => {
  it('should just pass-through data if it does not need a check', (done) => {
    const transform = checkFileForWarningTransform(false);
    const all = [];

    transform.pipe(through.obj((chunk, enc, cb) => {
      all.push(chunk);
      cb();
    }, () => {
      expect(all[0]).to.eql('foo');
      done();
    }));

    transform.end('foo');
  });

  it('should not emit warning if the record is a header with trailing pipes if file does not need check', (done) => {
    const transform = checkFileForWarningTransform(false);
    const all = [];
    let warningCalled = false;
    transform.on('warning', () => {
      warningCalled = true;
    });

    transform.pipe(through.obj((chunk, enc, cb) => {
      all.push(chunk);
      cb();
    }, () => {
      done();
    }));

    transform.end({ text: 'H|Foo|' });
    expect(warningCalled).to.be.false;
  });

  it('should emit warning if the record is a header with trailing pipes', (done) => {
    const transform = checkFileForWarningTransform(true);
    const all = [];

    transform.on('warning', (...args) => {
      expect(args[0]).to.equal('Too many fields in the Header Record (excess pipes "|" at the end of the record)');
      done();
    });

    transform.pipe(through.obj((chunk, enc, cb) => {
      all.push(chunk);
      cb();
    }));

    transform.end({ text: 'H|Foo|' });
  });

  it('should emit warning if the record is a trailer with trailing pipes', (done) => {
    const transform = checkFileForWarningTransform(true);
    const all = [];

    transform.on('warning', (...args) => {
      expect(args[0]).to.equal('Too many fields in the Trailer Record (excess pipes "|" at the end of the record)');
      done();
    });

    transform.pipe(through.obj((chunk, enc, cb) => {
      all.push(chunk);
      cb();
    }));

    transform.end({ text: 'T|Foo|' });
  });

  it('should still pass data along when warning is emitted', (done) => {
    const transform = checkFileForWarningTransform(true);
    const all = [];
    let warningCalled = false;

    transform.on('warning', () => {
      warningCalled = true;
    });

    transform.pipe(through.obj((chunk, enc, cb) => {
      all.push(chunk);
      cb();
    }, () => {
      expect(warningCalled).to.equal(true);
      expect(all.length).to.equal(1);
      expect(all[0]).to.eql({ text: 'H|Foo|' });
      done();
    }));

    transform.end({ text: 'H|Foo|' });
  });

  it('should not throw an error if the regex fails', (done) => {
    const transform = checkFileForWarningTransform();
    const all = [];

    transform.pipe(through.obj((chunk, enc, cb) => {
      all.push(chunk);
      cb();
    }, () => {
      expect(all[0]).to.eql('X');
      done();
    }))
    .on('error', (e) => {
      done(e);
    });

    transform.end('X');
  });
});
