const metadataTransform = require('../../../app/utils/transforms/addMetaData');
const { expect } = require('chai');
const through = require('through2');

const transform = metadataTransform({
  performance_year: '2000',
  run_number: '10'
});

describe('addMetaData transform', () => {
  it('should supplement the object with the passed in metadata', (done) => {
    const all = [];

    transform.pipe(through.obj((chunk, enc, cb) => {
      all.push(chunk);
      cb();
    }, () => {
      expect(all[0].data).to.include({
        test: 'foo',
        'Performance Year': '2000',
        'Run Number': '10'
      });
      done();
    }));
    transform.write({ data: { test: 'foo' } });
    transform.write({ data: { test: 'fizz' } });
    transform.end({ data: { test: 'buzz' } });
  });
});

