const filterRows = require('../../../app/utils/transforms/filterRows');
const through = require('through2');
const { expect } = require('chai');

const evenFilter = () => row => row % 2 === 0;
const greaterThan3Async = () => row => new Promise((resolve) => {
  setTimeout(() => {
    resolve(row > 3);
  }, 10);
});


describe('filterRows transform', () => {
  it('use given filters', (done) => {
    const all = [];

    filterRows([evenFilter]).then((transform) => {
      transform.pipe(through.obj((chunk, enc, cb) => {
        all.push(chunk);
        cb();
      }, () => {
        expect(all).to.eql([2, 4]);
        done();
      }));
      transform.write(1);
      transform.write(2);
      transform.write(3);
      transform.write(4);
      transform.end(5);
    });
  });

  it('should handle async filters', (done) => {
    const all = [];

    filterRows([greaterThan3Async]).then((transform) => {
      transform.pipe(through.obj((chunk, enc, cb) => {
        all.push(chunk);
        cb();
      }, () => {
        expect(all).to.eql([4, 5]);
        done();
      }));
      transform.write(1);
      transform.write(2);
      transform.write(3);
      transform.write(4);
      transform.end(5);
    });
  });

  it('should handle multiple filters', (done) => {
    const all = [];

    filterRows([evenFilter, greaterThan3Async]).then((transform) => {
      transform.pipe(through.obj((chunk, enc, cb) => {
        all.push(chunk);
        cb();
      }, () => {
        expect(all).to.eql([4]);
        done();
      }));
      transform.write(1);
      transform.write(2);
      transform.write(3);
      transform.write(4);
      transform.end(5);
    });
  });
});
