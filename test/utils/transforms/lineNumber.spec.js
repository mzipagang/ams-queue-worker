const lineNumber = require('../../../app/utils/transforms/lineNumber');
const { expect } = require('chai');
const through = require('through2');

const transform = lineNumber();

describe('lineNumber transform', () => {
  it('should add line numbers to rows', (done) => {
    const all = [];

    transform.pipe(through.obj((chunk, enc, cb) => {
      all.push(chunk);
      cb();
    }, () => {
      expect(all[0].lineNumber).to.equal(1);
      expect(all[1].lineNumber).to.equal(2);
      expect(all[2].lineNumber).to.equal(3);
      done();
    }));
    transform.write('hello');
    transform.write('foo');
    transform.end('bar');
  });
});

