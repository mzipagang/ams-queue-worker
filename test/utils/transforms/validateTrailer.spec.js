const validateTrailer = require('../../../app/utils/transforms/validateTrailer');
const { expect } = require('chai');
const through = require('through2');

const validateTrailerFunction = () => true;

let transform = validateTrailer(validateTrailerFunction);

describe('validateTrailer transform', () => {
  beforeEach(() => {
    transform = validateTrailer(validateTrailerFunction);
  });

  it('should not throw if no error', (done) => {
    const all = [];

    transform.pipe(through.obj((chunk, enc, cb) => {
      all.push(chunk);
      cb();
    }, () => {
      done();
    }));
    transform.write({ data: { 'Record Identifier': 'H' } });
    transform.write({ data: { 'Record Identifier': 'D' } });
    transform.write({ data: { 'Record Identifier': 'S' } });
    transform.end({ data: { 'Record Identifier': 'T' } });
  });

  it('should call error function when too many header rows', (done) => {
    const all = [];
    let errorMessage;

    transform = validateTrailer(validateTrailerFunction, (message) => {
      errorMessage = message;
      return Promise.resolve(errorMessage);
    });

    transform.pipe(through.obj((chunk, enc, cb) => {
      all.push(chunk);
      cb();
    }, () => {
      done();
    }));

    transform.on('error', () => {
      expect(errorMessage).to.equal('More than 1 header was found in file.');
      done();
    });

    transform.write({ data: { 'Record Identifier': 'H' } });
    transform.write({ data: { 'Record Identifier': 'D' } });
    transform.end({ data: { 'Record Identifier': 'H' } });
  });

  it('should call error function when too many trailer rows', (done) => {
    const all = [];
    let errorMessage;

    transform = validateTrailer(validateTrailerFunction, (message) => {
      errorMessage = message;
      return Promise.resolve(errorMessage);
    });

    transform.pipe(through.obj((chunk, enc, cb) => {
      all.push(chunk);
      cb();
    }, () => {
      done();
    }));

    transform.on('error', () => {
      expect(errorMessage).to.equal('More than 1 trailer was found in file.');
      done();
    });

    transform.write({ data: { 'Record Identifier': 'H' } });
    transform.write({ data: { 'Record Identifier': 'T' } });
    transform.end({ data: { 'Record Identifier': 'T' } });
  });

  it('should call error function if no detail rows', (done) => {
    const all = [];
    let errorMessage;

    transform = validateTrailer(validateTrailerFunction, (message) => {
      errorMessage = message;
      return Promise.resolve(errorMessage);
    });

    transform.pipe(through.obj((chunk, enc, cb) => {
      all.push(chunk);
      cb();
    }, () => {
      done();
    }));

    transform.on('error', () => {
      expect(errorMessage).to.equal('No detail records found in file.');
      done();
    });

    transform.write({ data: { 'Record Identifier': 'H' } });
    transform.end({ data: { 'Record Identifier': 'T' } });
  });

  it('should call error function if validation function fails', (done) => {
    const all = [];
    let errorMessage;

    transform = validateTrailer(() => false, (message) => {
      errorMessage = message;
      return Promise.resolve(errorMessage);
    });

    transform.pipe(through.obj((chunk, enc, cb) => {
      all.push(chunk);
      cb();
    }, () => {
      done();
    }));

    transform.on('error', () => {
      expect(errorMessage).to.equal('Invalid trailer in file.');
      done();
    });

    transform.write({ data: { 'Record Identifier': 'H' } });
    transform.write({ data: { 'Record Identifier': 'D' } });
    transform.end({ data: { 'Record Identifier': 'T' } });
  });

  it('should not pass along header or trailer rows', (done) => {
    const all = [];

    transform.pipe(through.obj((chunk, enc, cb) => {
      all.push(chunk);
      cb();
    }, () => {
      expect(all.length).to.equal(1);
      expect(all[0].data['Record Identifier']).to.equal('D');
      done();
    }));
    transform.write({ data: { 'Record Identifier': 'H' } });
    transform.write({ data: { 'Record Identifier': 'D' } });
    transform.end({ data: { 'Record Identifier': 'T' } });
  });

  it('should emit a warning if handler returns a warning value', (done) => {
    const all = [];
    const warningMessage = 'fake warning';

    transform = validateTrailer(() => ({ warning: true, message: warningMessage }), () => Promise.resolve('something'));

    transform.pipe(through.obj((chunk, _enc, cb) => {
      all.push(chunk);
      cb();
    }, () => { }));

    transform.on('warning', (message) => {
      expect(message).to.equal(warningMessage);
      done();
    });

    transform.write({ data: { 'Record Identifier': 'H' } });
    transform.write({ data: { 'Record Identifier': 'D' } });
    transform.end({ data: { 'Record Identifier': 'T', 'Record Count': '1' } });
  });

  it('should call error handler if an object with error message is returned from validation function', (done) => {
    const all = [];
    let actualMessage;
    const expectedErrorMessage = 'This is the message';

    transform = validateTrailer(() => ({ error: true, message: expectedErrorMessage }), (message) => {
      actualMessage = message;
      return Promise.resolve();
    });

    transform.pipe(through.obj((chunk, enc, cb) => {
      all.push(chunk);
      cb();
    }, () => {
      expect(actualMessage).to.equal(expectedErrorMessage);
      done();
    }));

    transform.write({ data: { 'Record Identifier': 'H' } });
    transform.write({ data: { 'Record Identifier': 'D' } });
    transform.end({ data: { 'Record Identifier': 'T' } });
  });

  it('should emit error if error handler returns a value', (done) => {
    const all = [];

    transform = validateTrailer(() => ({ error: true, message: 'fake error' }), () => Promise.resolve('something'));

    transform.pipe(through.obj((chunk, _enc, cb) => {
      all.push(chunk);
      cb();
    }, () => {
      done(new Error('Should not have been called'));
    }));

    transform.on('error', () => {
      done();
    });

    transform.write({ data: { 'Record Identifier': 'H' } });
    transform.write({ data: { 'Record Identifier': 'D' } });
    transform.end({ data: { 'Record Identifier': 'T', 'Record Count': '1' } });
  });
});

