const sendToDataWorker = require('../../../app/utils/transforms/sendToDataWorker');
const rowsProcessor = require('../../../app/services/rowsProcessor');
const through = require('through2');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const chai = require('chai');

const expect = chai.expect;
chai.use(sinonChai);

const lineParseCallback = () => {};
const transform = sendToDataWorker('FIG123', 'xyz', 'file.txt', lineParseCallback);

describe('sendToDataWorker transform', () => {
  let sandbox;

  before(() => {
    sandbox = sinon.sandbox.create();
    sandbox.stub(rowsProcessor, 'onBatch').resolves();
  });

  after(() => {
    sandbox.restore();
  });

  afterEach(() => {
    sandbox.resetHistory();
    rowsProcessor.onBatch.restore();
  });

  it('should make call to the batch processor with given metadata', (done) => {
    const all = [];

    transform.pipe(through.obj((chunk, enc, cb) => {
      all.push(chunk);
      cb();
    }, () => {
      const args = rowsProcessor.onBatch.getCall(0).args;
      expect(rowsProcessor.onBatch.calledOnce).to.equal(true);
      expect(args[0]).to.equal('FIG123');
      expect(args[1]).to.equal('xyz');
      expect(args[2]).to.equal('file.txt');
      expect(args[3]).to.equal(lineParseCallback);
      expect(args[4]).to.equal('foo');
      done();
    }));
    transform.end('foo');
  });
});
