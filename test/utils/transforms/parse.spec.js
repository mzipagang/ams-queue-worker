const parse = require('../../../app/utils/transforms/parse');
const chai = require('chai');
const { expect } = require('chai');
const through = require('through2');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.use(sinonChai);

const parseFunction = row => ({
  name: row.name.toUpperCase()
});

const parseFunctionWithError = () => {
  throw new Error('Some Error');
};

describe('Parse Transform', () => {
  describe('ParserUtility class', () => {
    const ParserUtility = parse.ParserUtility;

    beforeEach(() => {
      sinon.spy(ParserUtility, 'getStreamHandler');
      sinon.spy(ParserUtility, 'getStreamChunkHandler');
    });

    afterEach(() => {
      ParserUtility.getStreamHandler.restore();
      ParserUtility.getStreamChunkHandler.restore();
    });

    it('should return ParserUtility', () => {
      expect(ParserUtility).to.not.be.undefined;
    });

    describe('getStreamParser()', () => {
      it('should get the stream handler', () => {
        ParserUtility.getStreamParser(() => {}, () => {});
        expect(ParserUtility.getStreamHandler).to.have.been.called;
      });

      it('should get the stream chunk handler', () => {
        ParserUtility.getStreamParser(() => {}, () => {});
        expect(ParserUtility.getStreamChunkHandler).to.have.been.called;
      });

      it('should handle a stream with the returned stream parser', (done) => {
        const all = [];
        const transform = ParserUtility.getStreamParser(parseFunction, () => {});

        transform.pipe(through.obj((chunk, enc, cb) => {
          all.push(chunk);
          cb();
        }, () => {
          expect(all[0].name).to.equal('FOO');
          expect(all[1].name).to.equal('FIZZ');
          expect(all[2].name).to.equal('BUZZ');
          done();
        }));
        transform.write({ name: 'foo' });
        transform.write({ name: 'fizz' });
        transform.end({ name: 'buzz' });
      });


      it('should ignore rows that fail to parse with the returned stream parser', (done) => {
        const errorHandler = errorMessage => errorMessage;
        const transform = ParserUtility.getStreamParser(parseFunctionWithError, errorHandler);

        const all = [];

        transform.pipe(through.obj((chunk, enc, cb) => {
          all.push(chunk);
          cb();
        }, () => {
          expect(all.length).to.equal(0);
          done();
        }));
        transform.end({ name: 'foo' });
      });
    });

    describe('getStreamChunkHandler()', () => {
      it('should return a valid object/function', () => {
        const streamChunkHandler = ParserUtility.getStreamChunkHandler(parseFunction);
        expect(streamChunkHandler).to.not.be.undefined;
      });

      describe('with the returned stream chunk handling function', () => {
        let parseFunctionWithSpy;

        beforeEach(() => {
          parseFunctionWithSpy = sinon.spy(parseFunction);
        });

        afterEach(() => {
          parseFunctionWithSpy.reset();
        });

        it('should execute the parseFunction', () => {
          const streamChunkHandler = ParserUtility.getStreamChunkHandler(parseFunctionWithSpy, () => {});
          streamChunkHandler({ name: 'foo' }, null, () => {});
          expect(parseFunctionWithSpy).to.have.been.called;
        });

        it('should execute the callback if data is parsed', () => {
          const streamChunkHandler = ParserUtility.getStreamChunkHandler(parseFunctionWithSpy, () => {});
          const cb = sinon.spy();
          streamChunkHandler({ name: 'foo' }, null, cb);
          expect(cb).to.have.been.called;
          expect(cb.getCall(0).args[0]).to.be.null;
          expect(cb.getCall(0).args[1].name).to.equal('FOO');
        });

        it('should execute errorFunction if there is an error in parsing', () => {
          const errFn = sinon.spy();
          const streamChunkHandler = ParserUtility.getStreamChunkHandler(parseFunctionWithSpy, errFn);
          streamChunkHandler({ name: undefined, lineNumber: 1, text: '' }, null, () => {});
          expect(errFn).to.have.been.called;
          expect(errFn.getCall(0).args[0]).to.equal(1);
          expect(errFn.getCall(0).args[1]).to.equal('');
          expect(errFn.getCall(0).args[2].message).to.equal('Cannot read property \'toUpperCase\' of undefined');
        });
      });
    });
  });
});

