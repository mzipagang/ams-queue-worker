const headerUtilities = require('../../app/utils/headerUtilities');
const { expect } = require('chai');

const mockHeader = 'First | Second | Third | PY2000 | 123 | Sixth';

describe('headerUtilities', () => {
  it('should extract the performance year', () => {
    const expected = 2000;
    const actual = headerUtilities.getPerformanceYear(mockHeader);

    expect(actual).to.equal(expected);
  });

  it('should extract the run number', () => {
    const expected = '123';
    const actual = headerUtilities.getRunNumber(mockHeader);

    expect(actual).to.equal(expected);
  });

  it('should extract the run snapshot', () => {
    const expected = '123';
    const actual = headerUtilities.getRunSnapshot(mockHeader);

    expect(actual).to.equal(expected);
  });

  it('should extract the snapshot id', () => {
    const expected = 'Sixth';
    const actual = headerUtilities.getSnapshotId(mockHeader);

    expect(actual).to.equal(expected);
  });

  it('should extract the execution', () => {
    const expected = 'Sixth';
    const actual = headerUtilities.getExecution(mockHeader);

    expect(actual).to.equal(expected);
  });
});
