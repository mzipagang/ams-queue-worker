const FileValidator = require('../../app/utils/FileValidator');
const { expect } = require('chai');

describe('FileValidator', () => {
  describe('FileValidator.validateParsedTrailerUtility', () => {
    it('should throw exception for missing header', () => {
      const wrapped = FileValidator.validateParsedTrailerUtility.bind(FileValidator, {});
      expect(wrapped).to.throw();
    });

    it('should throw exception for missing trailer', () => {
      const wrapped = FileValidator.validateParsedTrailerUtility.bind(FileValidator, {
        header: 'test'
      });
      expect(wrapped).to.throw();
    });

    it('should throw exception for missing fileType', () => {
      const wrapped = FileValidator.validateParsedTrailerUtility.bind(FileValidator, {
        header: 'test',
        trailer: {}
      });
      expect(wrapped).to.throw();
    });

    it('should throw exception for missing fileType', () => {
      const wrapped = FileValidator.validateParsedTrailerUtility.bind({
        header: 'test',
        trailer: {},
        fileType: 'test'
      });
      expect(wrapped).to.throw();
    });

    it('should indicate an warning when summary record counts do not match count in trailer', () => {
      const expected = {
        error: false,
        warning: true,
        message: 'Summary Record count in the Trailer does not equal the number of Summary records.'
      };
      const actual = FileValidator.validateParsedTrailerUtility({
        validateRecordCount: false,
        validateRecordIdentifierPattern: false,
        validateSnapshotPattern: false,
        validateExecutionPattern: false,
        header: 'test',
        fileType: 'test',
        recordsCount: 1,
        trailer: {
          'File Type': 'test',
          'Summary Record Count': 1
        },
        validateSummaryCount: true,
        summaryRecordCount: 5
      });
      expect(actual).to.deep.equal(expected);
    });

    it('should indicate an error when no summary records provided', () => {
      const expected = {
        error: true,
        warning: false,
        message: 'File must contain at least one Summary Record.'
      };
      const actual = FileValidator.validateParsedTrailerUtility({
        validateRecordCount: false,
        validateRecordIdentifierPattern: false,
        validateSnapshotPattern: false,
        validateExecutionPattern: false,
        header: 'test',
        fileType: 'test',
        recordsCount: 1,
        trailer: {
          'File Type': 'test',
          'Summary Record Count': 1
        },
        validateSummaryCount: true,
        summaryRecordCount: 0
      });
      expect(actual).to.deep.equal(expected);
    });
  });
});
