const readFirstLineofStream = require('../../app/utils/readFirstLineofStream');
const { expect } = require('chai');
const fs = require('fs');
const path = require('path');

const getTestFile = filename => path.join(__dirname, '../test_files/', filename);

describe('readFirstLineOfStream', () => {
  it('should read the first line', (done) => {
    readFirstLineofStream(fs.createReadStream(getTestFile('file_with_first_line_text.txt'))).then((line) => {
      expect(line).to.equal('Foobar');
      done();
    }).catch(done);
  });

  it('should return null if file is empty', (done) => {
    readFirstLineofStream(fs.createReadStream(getTestFile('empty.txt'))).then((line) => {
      expect(line).to.be.null;
      done();
    }).catch(done);
  });
});
