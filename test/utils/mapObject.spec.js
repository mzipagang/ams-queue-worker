const mapObject = require('../../app/utils/mapObject');
const { expect } = require('chai');

describe('mapObject', () => {
  it('should map object to desired key names', () => {
    const map = {
      'SOME UNDESIRABLE KEY NAME': 'desired_key_name'
    };
    const rawObject = {
      'SOME UNDESIRABLE KEY NAME': 'a value'
    };

    const expected = {
      desired_key_name: 'a value'
    };
    const actual = mapObject(rawObject, map);

    expect(actual).to.deep.equal(expected);
  });

  it('should ignore keys not in the map', () => {
    const map = {
      'SOME UNDESIRABLE KEY NAME': 'desired_key_name'
    };
    const rawObject = {
      'SOME UNDESIRABLE KEY NAME': 'a value',
      'A KEY NAME THAT DOES NOT EXIST IN MAP': 'should not be in final result'
    };

    const expected = {
      desired_key_name: 'a value'
    };
    const actual = mapObject(rawObject, map);

    expect(actual).to.deep.equal(expected);
  });

  it('should create nested objects if map entry contains a "."', () => {
    const map = {
      city: 'address.city',
      state: 'address.state',
      first: 'name'
    };
    const rawObject = {
      city: 'Somewhere',
      state: 'AK',
      first: 'Foobar'
    };

    const expected = {
      name: 'Foobar',
      address: {
        city: 'Somewhere',
        state: 'AK'
      }
    };
    const actual = mapObject(rawObject, map);

    expect(actual).to.deep.equal(expected);
  });

  it('should not do anything to provided object if map is not supplied', () => {
    const rawObject = {
      city: 'Somewhere',
      state: 'AK',
      first: 'Foobar'
    };

    const actual = mapObject(rawObject);

    expect(actual).to.deep.equal(rawObject);
  });
});
