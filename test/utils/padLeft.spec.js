const { padLeftIfNumber } = require('../../app/utils');
const { expect } = require('chai');

describe('padLeftIfNumber', () => {
  it('should return the given value if it is not a number', () => {
    const expected = 'foo';
    const actual = padLeftIfNumber('foo', 5);

    expect(actual).to.equal(expected);
  });

  it('should pad the given number with zeros up to given length', () => {
    const expected = '00100';
    const actual = padLeftIfNumber(100, 5);

    expect(actual).to.equal(expected);
  });

  it('should not pad if length already exceeds pad length', () => {
    const expected = '123456789';
    const actual = padLeftIfNumber(123456789, 3);

    expect(actual).to.equal(expected);
  });
});
