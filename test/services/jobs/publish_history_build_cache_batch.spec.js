const sinon = require('sinon');
const chai = require('chai');
const sinonChai = require('sinon-chai');

chai.use(sinonChai);
const expect = chai.expect;

const services = require('../../../app/services');
const job = require('../../../app/jobs/publish_history_build_cache_batch');

describe('Job: PUBLISH_HISTORY_BUILD_CACHE_BATCH', () => {
  let dataWorkerMock;

  beforeEach(() => {
    dataWorkerMock = sinon.mock(services.dataWorker);
  });

  afterEach(() => {
    dataWorkerMock.restore();
  });

  it('should go through all tin and npis', (done) => {
    dataWorkerMock
        .expects('post')
        .atLeast(1)
        .resolves(null);

    dataWorkerMock
        .expects('put')
        .once()
        .resolves({
          body: {
            data: {
              publish_history: {
                id: '1',
                cache: {
                  finished: 2,
                  expected: 4
                }
              }
            }
          }
        });

    dataWorkerMock
        .expects('patch')
        .once()
        .resolves({
          body: {
            data: {
              publish_history: {
                id: '1',
                cache: {
                  finished: 2,
                  expected: 4
                }
              }
            }
          }
        });

    job.handler({
      publishHistoryId: '1',
      npis: ['1234567890'],
      tins: ['123456789']
    }, (err) => {
      done(err);
    });
  });

  it('should go through all tin and npis and finish the cache building', (done) => {
    dataWorkerMock
        .expects('post')
        .atLeast(1)
        .resolves(null);

    dataWorkerMock
        .expects('put')
        .once()
        .resolves({
          body: {
            data: {
              publish_history: {
                id: '1',
                cache: {
                  finished: 2,
                  expected: 2
                }
              }
            }
          }
        });

    dataWorkerMock
        .expects('patch')
        .once()
        .resolves({
          body: {
            data: {
              publish_history: {
                id: '1',
                cache: {
                  finished: 2,
                  expected: 2
                }
              }
            }
          }
        });

    job.handler({
      publishHistoryId: '1',
      tins: ['123456789']
    }, (err) => {
      done(err);
    });
  });

  it('should handle an error', (done) => {
    dataWorkerMock
        .expects('post')
        .rejects(new Error('Fake Error'));

    job.handler({
      publishHistoryId: '1',
      npis: ['1234567890']
    }, (err) => {
      try {
        expect(err).to.not.be.undefined;
        done();
      } catch (error) {
        done(error);
      }
    });
  });
});
