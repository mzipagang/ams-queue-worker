const chai = require('chai');
const job = require('../../../app/jobs/generate_qp_metrics_report');
const dataWorker = require('../../../app/services/dataWorker');

const expect = chai.expect;

describe('Job: GENERATE_QP_METRICS_REPORT', () => {
  describe('Job Name', () => {
    it('should have the job name we expect', () => {
      expect(job.jobName).to.equal('GENERATE_QP_METRICS_REPORT');
    });
  });
  describe('handle error', () => {
    it('should handle invalid report', async () => {
      await job.handler({ performanceYear: new Date().getFullYear() + 1, runSnapshot: '1' }, () => {});
    });
  });
  describe('handle success', () => {
    let originalGet;
    before(() => {
      originalGet = dataWorker.get;
      dataWorker.get = () => {};
    });
    after(() => {
      dataWorker.get = originalGet;
    });
    it('should handle valid report', async () => {
      await job.handler({ performanceYear: '2007', runSnapshot: 'F' }, () => {});
    });
  });
});
