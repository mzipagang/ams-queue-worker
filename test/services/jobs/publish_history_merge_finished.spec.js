const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

const services = require('../../../app/services');
const config = require('../../../app/config');
const job = require('../../../app/jobs/publish_history_merge_finished');

const expect = chai.expect;
chai.use(sinonChai);

describe('Job: PUBLISH_HISTORY_MERGE_FINISHED', () => {
  describe('Job Name', () => {
    it('should have the job name we expect', () => {
      expect(job.jobName).to.equal('PUBLISH_HISTORY_MERGE_FINISHED');
    });
  });

  describe('Handler', () => {
    const fileImportGroupId = 'FILE_IMPORT_GROUP_ID';
    const publishHistoryId = 'PUBLISH_HISTORY_ID';
    const updatedAt = 'UPDATED DATE';
    const handler = job.handler;

    let originalCacheConfig;
    let body;
    let done;
    let incompleteFile;
    let finishedFile;
    let files;
    let fileImportGroupUrl;
    let publishHistoryUrl;
    let fileImportGroupResponse;
    let publishHistoryResponse;
    let publishHistory;
    let _dataWorkerGet;
    let _dataWorkerPatch;
    let _createSQSAPIMessage;

    beforeEach(() => {
      originalCacheConfig = config.CACHE;

      fileImportGroupUrl = `/api/v1/file_import_group/${fileImportGroupId}`;
      publishHistoryUrl = `/api/v1/publish_history/${publishHistoryId}`;

      body = {
        fileImportGroupId,
        publishHistoryId
      };

      done = sinon.stub();

      incompleteFile = {
        status: 'incomplete'
      };

      finishedFile = {
        status: 'finished'
      };

      files = [];

      fileImportGroupResponse = {
        body: {
          data: {
            file_import_group: {
              files,
              updatedAt
            }
          }
        }
      };

      publishHistory = {
        group_type: 'invalid'
      };

      publishHistoryResponse = {
        body: {
          data: {
            publish_history: publishHistory
          }
        }
      };

      _dataWorkerGet = sinon.stub(services.dataWorker, 'get');
      _dataWorkerPatch = sinon.stub(services.dataWorker, 'patch');
      _createSQSAPIMessage = sinon.stub(services, 'createSQSAPIMessage');

      _dataWorkerGet.withArgs(fileImportGroupUrl).resolves(fileImportGroupResponse);
      _dataWorkerGet.withArgs(publishHistoryUrl).resolves(publishHistoryResponse);
    });

    afterEach(() => {
      config.CACHE = originalCacheConfig;
      _dataWorkerGet.restore();
      _dataWorkerPatch.restore();
      _createSQSAPIMessage.restore();
    });

    function assertMocks(finishedFiles, sqsCache, publishHistory2) {
      expect(_dataWorkerGet.getCall(0)).to.have.been.calledWithExactly(fileImportGroupUrl);
      expect(_dataWorkerPatch.getCall(0)).to.have.been.calledWithExactly(fileImportGroupUrl, {
        status: 'published',
        files: finishedFiles
      });
      expect(_dataWorkerGet.getCall(1)).to.have.been.calledWithExactly(publishHistoryUrl);
      expect(_dataWorkerGet).to.have.callCount(2);

      if (publishHistory2) {
        expect(_dataWorkerPatch).to.have.callCount(2);
        expect(_dataWorkerPatch.getCall(1)).to.have.been.calledWithExactly(publishHistoryUrl, { status: 'published' });
      } else {
        expect(_dataWorkerPatch).to.have.callCount(1);
      }

      if (sqsCache) {
        expect(_createSQSAPIMessage).to.have.been.calledWithExactly(1, 'PUBLISH_HISTORY_BUILD_CACHE_QUEUE', {
          fileImportGroupId,
          publishHistoryId
        });
      } else {
        expect(_createSQSAPIMessage).to.have.callCount(0);
      }

      expect(done).to.have.been.calledWithExactly();
    }

    it('should handle regular errors', async () => {
      const err = new Error('A regular error');
      _dataWorkerGet.withArgs(fileImportGroupUrl).throws(err);
      await handler(body, done);
      expect(done).to.have.been.calledWithExactly(err);
    });

    it('should handle async rejected promises', async () => {
      const err = new Error('A rejected promise error');
      _dataWorkerGet.withArgs(fileImportGroupUrl).rejects(err);
      await handler(body, done);
      expect(done).to.have.been.calledWithExactly(err);
    });

    it('should handle file import group result with no files', async () => {
      await handler(body, done);
      assertMocks([], false, true);
    });

    it('should handle files that are not finished', async () => {
      files.push(incompleteFile, incompleteFile);
      await handler(body, done);
      assertMocks([incompleteFile, incompleteFile], false, true);
    });

    it('should handle files that are finished', async () => {
      files.push(finishedFile, incompleteFile);
      await handler(body, done);
      expect(finishedFile.publishedAt).to.equal(updatedAt);
      assertMocks([finishedFile, incompleteFile], false, true);
    });

    it('should not send SQS message if cache is disabled', async () => {
      config.CACHE.BUILD_CACHE_ENABLED = false;
      publishHistory.group_type = 'pac';
      config.CACHE.WAIT_FOR_CACHE_TO_BUILD = false;

      await handler(body, done);
      assertMocks([], false, true);
    });

    it('should not send SQS message if not PAC group type', async () => {
      config.CACHE.BUILD_CACHE_ENABLED = true;
      publishHistory.group_type = 'not pac';
      config.CACHE.WAIT_FOR_CACHE_TO_BUILD = false;

      await handler(body, done);
      assertMocks([], false, true);
    });

    it('should send SQS message if cache enabled and PAC group type', async () => {
      config.CACHE.BUILD_CACHE_ENABLED = true;
      publishHistory.group_type = 'pac';
      config.CACHE.WAIT_FOR_CACHE_TO_BUILD = false;

      await handler(body, done);
      assertMocks([], true, true);
    });

    it('should patch to publish history if not PAC group type', async () => {
      config.CACHE.BUILD_CACHE_ENABLED = false;
      publishHistory.group_type = 'not pac';
      config.CACHE.WAIT_FOR_CACHE_TO_BUILD = true;

      await handler(body, done);
      assertMocks([], false, true);
    });

    it('should patch to publish history if we do not want to wait for cache to build ', async () => {
      config.CACHE.BUILD_CACHE_ENABLED = false;
      publishHistory.group_type = 'pac';
      config.CACHE.WAIT_FOR_CACHE_TO_BUILD = false;

      await handler(body, done);
      assertMocks([], false, true);
    });

    it('should not patch to publish history if PAC we want to wait for cache to build ', async () => {
      config.CACHE.BUILD_CACHE_ENABLED = false;
      publishHistory.group_type = 'pac';
      config.CACHE.WAIT_FOR_CACHE_TO_BUILD = true;

      await handler(body, done);
      assertMocks([], false, false);
    });
  });
});
