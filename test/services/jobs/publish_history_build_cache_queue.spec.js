const sinon = require('sinon');
const chai = require('chai');
const sinonChai = require('sinon-chai');

chai.use(sinonChai);
const expect = chai.expect;

const services = require('../../../app/services');
const aws = require('../../../app/services/aws');
const job = require('../../../app/jobs/publish_history_build_cache_queue');

describe('Job: PUBLISH_HISTORY_BUILD_CACHE_QUEUE', () => {
  let dataWorkerMock;
  let sendMessageBatchStub;

  beforeEach(() => {
    dataWorkerMock = sinon.mock(services.dataWorker);
    sendMessageBatchStub = sinon.stub(aws.SQS, 'sendMessageBatch');
  });

  afterEach(() => {
    dataWorkerMock.restore();
    sendMessageBatchStub.restore();
  });

  it('should publish group of APM files', (done) => {
    dataWorkerMock
        .expects('patch')
        .atLeast(1)
        .resolves({
          body: {
            data: {
              publish_history: {
                id: '1'
              }
            }
          }
        });

    dataWorkerMock
        .expects('get')
        .twice()
        .resolves({
          body: {
            data: {
              tins: ['123456789'],
              npis: ['1234567890']
            }
          }
        });

    sendMessageBatchStub.callsFake((params, cb) => {
      cb(null, 1);
    });

    job.handler({
      publishHistoryId: '1'
    }, (err) => {
      done(err);
    });
  });

  it('should publish group of PAC files', (done) => {
    dataWorkerMock
        .expects('patch')
        .atLeast(1)
        .resolves({
          body: {
            data: {
              publish_history: {
                id: '1'
              }
            }
          }
        });

    dataWorkerMock
        .expects('get')
        .twice()
        .resolves({
          body: {
            data: {
              tins: ['123456789'],
              npis: ['1234567890']
            }
          }
        });

    sendMessageBatchStub.callsFake((params, cb) => {
      cb(null, 1);
    });

    job.handler({
      publishHistoryId: '1'
    }, (err) => {
      done(err);
    });
  });

  it('should handle an error', (done) => {
    dataWorkerMock
        .expects('get')
        .twice()
        .rejects(new Error('Fake Error'));

    job.handler({
      publishHistoryId: '1'
    }, (err) => {
      try {
        expect(err).to.not.be.undefined;
        done();
      } catch (error) {
        done(error);
      }
    });
  });
});
