const sinon = require('sinon');
const chai = require('chai');
const sinonChai = require('sinon-chai');

chai.use(sinonChai);
const expect = chai.expect;

const services = require('../../../app/services');
const job = require('../../../app/jobs/publish_history_merge_file_batch');

describe('Job: PUBLISH_HISTORY_MERGE_FILE_BATCH', () => {
  let doneSpy;
  let dataWorkerPost;
  let createSQSAPIMessageStub;

  beforeEach(() => {
    doneSpy = sinon.spy();
    dataWorkerPost = sinon.stub(services.dataWorker, 'post');
    createSQSAPIMessageStub = sinon.stub(services.createSQSAPIMessage.SQSMessageApi, 'create');
  });

  afterEach(() => {
    doneSpy.reset();
    dataWorkerPost.restore();
    createSQSAPIMessageStub.restore();
  });

  it('should load correctly', () => {
    expect(job).to.not.be.undefined;
    expect(job.handler).to.not.be.undefined;
    expect(job.jobName).to.equal('PUBLISH_HISTORY_MERGE_FILE_BATCH');
  });

  it('should call done callback', async () => {
    dataWorkerPost.callsFake(() => ({
      body: {
        data: {
          publish_history: {
            status: 'publishing',
            files: [{ status: 'published' }]
          }
        }
      }
    }));
    const body = {
      publishHistoryId: '1234567890',
      fileImportGroupId: '1234567890',
      fileIds: ['1234567890'],
      fromRowNumber: '1',
      toRowNumber: '100'
    };

    await job.handler(body, doneSpy);
    expect(doneSpy).to.have.been.called;
  });

  it('should create post request to dataworker', async () => {
    dataWorkerPost.callsFake(() => ({
      body: {
        data: {
          publish_history: {
            status: 'publishing',
            files: [{ status: 'published' }]
          }
        }
      }
    }));

    const body = {
      publishHistoryId: '1234567890',
      fileImportGroupId: '1234567890',
      fileIds: ['1234567890'],
      fromRowNumber: '1',
      toRowNumber: '100'
    };

    await job.handler(body, doneSpy);
    expect(dataWorkerPost).to.have.been.called;
  });

  it('should send a PUBLISH_HISTORY_MERGE_FINISHED message if all files are published', async () => {
    dataWorkerPost.callsFake(() => ({
      body: {
        data: {
          publish_history: {
            status: 'publishing',
            files: [{ status: 'published' }]
          }
        }
      }
    }));

    const body = {
      publishHistoryId: '1234567890',
      fileImportGroupId: '1234567890',
      fileIds: ['1234567890'],
      fromRowNumber: '1',
      toRowNumber: '100'
    };

    await job.handler(body, doneSpy);
    expect(createSQSAPIMessageStub).to.have.been.called;
    expect(createSQSAPIMessageStub.getCall(0).args[1]).to.equal('PUBLISH_HISTORY_MERGE_FINISHED');
  });

  it('should not send message if all files are not published', async () => {
    dataWorkerPost.callsFake(() => ({
      body: {
        data: {
          publish_history: {
            status: 'publishing',
            files: [{ status: 'publishing' }, { status: 'published' }]
          }
        }
      }
    }));

    const body = {
      publishHistoryId: '1234567890',
      fileImportGroupId: '1234567890',
      fileIds: ['1234567890'],
      fromRowNumber: '1',
      toRowNumber: '100'
    };

    await job.handler(body, doneSpy);
    expect(createSQSAPIMessageStub).to.not.have.been.called;
  });
});
