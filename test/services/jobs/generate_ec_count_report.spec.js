const chai = require('chai');
const dataWorker = require('../../../app/services/dataWorker');
const job = require('../../../app/jobs/generate_ec_count_report');

const expect = chai.expect;

describe('Job: GENERATE_EC_COUNT_REPORT', () => {
  describe('Job Name', () => {
    it('should have the job name we expect', () => {
      expect(job.jobName).to.equal('GENERATE_EC_COUNT_REPORT');
    });
  });
  describe('handle error', () => {
    it('should handle invalid report', async () => {
      await job.handler({ performanceYear: new Date().getFullYear() + 1, runSnapshot: '1' }, () => {});
    });
  });
  describe('handle success', () => {
    let originalGet;
    before(() => {
      originalGet = dataWorker.get;
      dataWorker.get = () => {};
    });
    after(() => {
      dataWorker.get = originalGet;
    });
    it('should handle valid report', async () => {
      await job.handler({ performanceYear: '2007', runSnapshot: 'F' }, () => {});
    });
  });
});
