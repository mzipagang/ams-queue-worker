const sinon = require('sinon');
const chai = require('chai');
const sinonChai = require('sinon-chai');

chai.use(sinonChai);
const expect = chai.expect;

const services = require('../../../app/services');
const job = require('../../../app/jobs/publish_history_merge_file_queue');

describe('Job: PUBLISH_HISTORY_MERGE_FILE_QUEUE', () => {
  let dataWorkerStub;
  let doneSpy;
  let createSQSAPIMessageStub;

  beforeEach(() => {
    dataWorkerStub = sinon.stub(services.dataWorker, 'get');
    doneSpy = sinon.spy();
    createSQSAPIMessageStub = sinon.stub(services.createSQSAPIMessage.SQSMessageApi, 'create');
  });

  afterEach(() => {
    dataWorkerStub.restore();
    doneSpy.reset();
    createSQSAPIMessageStub.restore();
  });

  it('should load correctly', () => {
    expect(job).to.not.be.undefined;
    expect(job.handler).to.not.be.undefined;
    expect(job.jobName).to.equal('PUBLISH_HISTORY_MERGE_FILE_QUEUE');
    expect(job.BATCH_SIZE).to.equal(5000);
  });

  describe('getFromRow()', () => {
    it('should return the correct firstRowIndex if it is the first batch message', () => {
      const fromRow = job.getFromRow(1, 10);
      expect(fromRow).to.equal(10);
    });

    it('should return the 2nd batch through properly adding the BATCH_SIZE', () => {
      const fromRow = job.getFromRow(2, 10);
      expect(fromRow).to.equal(5001);
    });

    it('should return the 3rd batch through properly adding the BATCH_SIZE', () => {
      const fromRow = job.getFromRow(3, 10);
      expect(fromRow).to.equal(10001);
    });
  });

  describe('getToRow()', () => {
    it('should match the lastBatch for the first batch message', () => {
      const toRow = job.getToRow(1, 125);
      expect(toRow).to.equal(125);
    });

    it('should return the 2nd batch through properly adding the BATCH_SIZE', () => {
      const toRow = job.getToRow(2, 125);
      expect(toRow).to.equal(5125);
    });

    it('should return the 3rd batch through properly adding the BATCH_SIZE', () => {
      const toRow = job.getToRow(3, 125);
      expect(toRow).to.equal(10125);
    });
  });

  describe('getDetailRowsInformation()', () => {
    it('should return the first and last detail rows', async () => {
      dataWorkerStub.callsFake(() => ({
        body: {
          file: {
            first_detail_row: 2,
            last_detail_row: 10
          }
        }
      }));

      const rowsInfo = await job.getDetailRowsInformation('1234567890', '1234567890');
      expect(rowsInfo.first_detail_row_index).to.equal(2);
      expect(rowsInfo.last_detail_row_index).to.equal(10);
    });

    it('should return 0 if field does not exist', async () => {
      dataWorkerStub.callsFake(() => ({
        body: {
          file: {
            first_detail_row: null,
            last_detail_row: null
          }
        }
      }));

      const rowsInfo = await job.getDetailRowsInformation('1234567890', '1234567890');
      expect(rowsInfo.first_detail_row_index).to.equal(0);
      expect(rowsInfo.last_detail_row_index).to.equal(0);
    });
  });

  describe('handler()', () => {
    it('should call done function', async () => {
      dataWorkerStub.callsFake(() => ({
        body: {
          file: {
            first_detail_row: 1,
            last_detail_row: 10
          }
        }
      }));

      await job.handler({
        publishHistoryId: '123456789',
        fileImportGroupId: '123456789',
        fileIds: ['123456789']
      }, doneSpy);

      expect(doneSpy).to.have.been.called;
    });

    it('should create a single message for under 5000 records', async () => {
      dataWorkerStub.callsFake(() => ({
        body: {
          file: {
            first_detail_row: 2,
            last_detail_row: 100
          }
        }
      }));

      await job.handler({
        publishHistoryId: '123456789',
        fileImportGroupId: '123456789',
        fileIds: ['123456789']
      }, doneSpy);

      expect(createSQSAPIMessageStub).to.have.callCount(1);
      expect(createSQSAPIMessageStub.getCall(0).args[1]).to.equal('PUBLISH_HISTORY_MERGE_FILE_BATCH');
      expect(createSQSAPIMessageStub.getCall(0).args[2].fromRowNumber).to.equal(2);
      expect(createSQSAPIMessageStub.getCall(0).args[2].toRowNumber).to.equal(100);
    });

    it('should create two messages for an odd number of records (9999)', async () => {
      dataWorkerStub.callsFake(() => ({
        body: {
          file: {
            first_detail_row: 2,
            last_detail_row: 9999
          }
        }
      }));

      await job.handler({
        publishHistoryId: '123456789',
        fileImportGroupId: '123456789',
        fileIds: ['123456789']
      }, doneSpy);

      expect(createSQSAPIMessageStub).to.have.callCount(2);
      expect(createSQSAPIMessageStub.getCall(0).args[1]).to.equal('PUBLISH_HISTORY_MERGE_FILE_BATCH');
      expect(createSQSAPIMessageStub.getCall(0).args[2].fromRowNumber).to.equal(2);
      expect(createSQSAPIMessageStub.getCall(0).args[2].toRowNumber).to.equal(5000);
      expect(createSQSAPIMessageStub.getCall(1).args[2].fromRowNumber).to.equal(5001);
      expect(createSQSAPIMessageStub.getCall(1).args[2].toRowNumber).to.equal(9999);
    });

    it('should create two messages for an even number of records (10000)', async () => {
      dataWorkerStub.callsFake(() => ({
        body: {
          file: {
            first_detail_row: 2,
            last_detail_row: 10000
          }
        }
      }));

      await job.handler({
        publishHistoryId: '123456789',
        fileImportGroupId: '123456789',
        fileIds: ['123456789']
      }, doneSpy);

      expect(createSQSAPIMessageStub).to.have.callCount(2);
      expect(createSQSAPIMessageStub.getCall(0).args[1]).to.equal('PUBLISH_HISTORY_MERGE_FILE_BATCH');
      expect(createSQSAPIMessageStub.getCall(0).args[2].fromRowNumber).to.equal(2);
      expect(createSQSAPIMessageStub.getCall(0).args[2].toRowNumber).to.equal(5000);
      expect(createSQSAPIMessageStub.getCall(1).args[2].fromRowNumber).to.equal(5001);
      expect(createSQSAPIMessageStub.getCall(1).args[2].toRowNumber).to.equal(10000);
    });
  });
});
