const rowParser = require('../../app/services/rowParser');
const { expect } = require('chai');

describe('Service: Row Parser', () => {
  it('should properly load', () => {
    expect(rowParser).to.not.be.undefined;
  });

  it('should parse a comma separated list', () => {
    const result = rowParser('test1, test2, test3, test4', ',');

    expect(result).to.not.be.undefined;
    expect(result.sort()).to.deep.equal(['test1', 'test2', 'test3', 'test4']);
  });

  it('should parse a comma separated list with ,', () => {
    const result = rowParser('\'tes,t1\', \'test2\', \'test3\', \'test4\'', ',');

    expect(result).to.not.be.undefined;
    expect(result.sort()).to.deep.equal(['tes,t1', 'test2', 'test3', 'test4']);
  });

  it('should parse a comma separated list with escaped \'', () => {
    const result = rowParser('\'tes \\\' t1\', \'test2\', \'test3\', \'test4\'', ',');

    expect(result).to.not.be.undefined;
    expect(result.sort()).to.deep.equal(['tes \' t1', 'test2', 'test3', 'test4']);
  });

  it('should parse a comma separated list with escaped "', () => {
    const result = rowParser('"tes \\" t1", "test2", "test3","test4"', ',');

    expect(result).to.not.be.undefined;
    expect(result.sort()).to.deep.equal(['tes " t1', 'test2', 'test3', 'test4']);
  });

  it('should return null for invalid parse', () => {
    const result = rowParser('te" " "st', ',');

    expect(result).to.be.null;
  });

  it('should handle empty values', () => {
    const result = rowParser('   one  ,  \'two\'  ,  , \' four\' ,, \'six \', \' seven \' ,  ', ',');

    expect(result).to.not.be.undefined;
  });

  it('should handle special characters', () => {
    const result = rowParser('test1.test2.test3, test', ',');

    expect(result).to.not.be.undefined;
  });

  it('should parse a pipe delimited list', () => {
    const result = rowParser('test1 | test2 | test3 | test4', '|');

    expect(result).to.not.be.undefined;
    expect(result.sort()).to.deep.equal(['test1', 'test2', 'test3', 'test4']);
  });

  it('should parse a pipe delimited list with a \' in a value', () => {
    const result = rowParser('test1 | test2\'s | test3 | test4', '|');

    expect(result).to.not.be.undefined;
    expect(result.sort()).to.deep.equal(['test1', 'test2\'s', 'test3', 'test4']);
  });

  it('should parse a pipe delimited list with a " in a value', () => {
    const result = rowParser('test1 | test2"s | test3 | test4', '|');

    expect(result).to.not.be.undefined;
    expect(result.sort()).to.deep.equal(['test1', 'test2"s', 'test3', 'test4']);
  });
});
