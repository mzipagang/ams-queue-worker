const { expect } = require('chai');
const recordIdentifierFileParser = require('../../app/services/fileParser/recordIdentifierFileParser');

const mockLines = [
  { lineNumber: 1, text: 'H|George|Washington|Unaffiliated' },
  { lineNumber: 2, text: 'D|John|Adams|Federalist' },
  { lineNumber: 3, text: 'T|Thomas|Jefferson|Democratic-Republican' }
];

describe('Record Identifer File Parser', () => {
  it('should return null for lines that have an H or T as the first character', () => {
    const parse = recordIdentifierFileParser('|', ['Identifier', 'First', 'Last', 'Party']).parser();
    const results = mockLines.map(line => parse(line));

    expect(results.length).to.equal(3);
    expect(results[0]).to.be.null;
    expect(results[2]).to.be.null;
    expect(results[1]).to.eql({
      data: {
        Identifier: 'D',
        First: 'John',
        Last: 'Adams',
        Party: 'Federalist'
      },
      lineNumber: 2
    });
  });
});

