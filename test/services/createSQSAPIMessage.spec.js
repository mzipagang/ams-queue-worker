const aws = require('../../app/services/aws');
const sinon = require('sinon');
const chai = require('chai');
const sinonChai = require('sinon-chai');

const createSQSAPIMessage = require('../../app/services/createSQSAPIMessage');

const expect = chai.expect;
const SQSMessageApi = createSQSAPIMessage.SQSMessageApi;

chai.use(sinonChai);

describe('Service: createSQSAPIMessage', () => {
  let sqsStub;
  let sendMessageSpy;

  beforeEach(() => {
    sendMessageSpy = sinon.spy();
    sqsStub = sinon.stub(aws.SQS, 'sendMessage').callsFake((params, cb) => { cb(null, sendMessageSpy()); });
  });

  afterEach(() => {
    sqsStub.restore();
    sendMessageSpy.reset();
  });

  describe('create()', () => {
    it('should call SQS sendMessage method', async () => {
      const priority = 1;
      const jobName = 'PUBLISH_HISTORY_MERGE_FILE_BATCH';
      const body = { };
      await SQSMessageApi.create(priority, jobName, body);
      expect(sendMessageSpy).to.have.been.called;
    });

    it('should throw an error for priority that is not 1 or 2', async () => {
      const priority = 3;
      const jobName = 'PUBLISH_HISTORY_MERGE_FILE_BATCH';
      const body = { };
      try {
        await SQSMessageApi.create(priority, jobName, body);
      } catch (err) {
        expect(err.message).to.equal('Priority can only be values 1 to 2.');
      }
    });
  });
});
