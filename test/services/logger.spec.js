const { expect } = require('chai');

const logger = () => require('../../app/services/logger');
const environment = require('../environment');

describe('Service: Logger', () => {
  describe('in dev', () => {
    let loggerService;
    beforeEach(() => {
      environment.reset(__dirname,
        ['../../app/services/logger', '../../app/config']);
      environment.use('dev');
    });
    it('should have logging functions', () => {
      loggerService = logger();
      expect(loggerService.error).to.not.be.undefined;
      expect(loggerService.warn).to.not.be.undefined;
      expect(loggerService.info).to.not.be.undefined;
      expect(loggerService.verbose).to.not.be.undefined;
      expect(loggerService.debug).to.not.be.undefined;
      expect(loggerService.silly).to.not.be.undefined;
    });
  });

  describe('in production', () => {
    let loggerService;
    beforeEach(() => {
      environment.reset(__dirname,
        ['../../app/services/logger', '../../app/config']);
      environment.use('production');
    });
    it('should have logging functions', () => {
      loggerService = logger();
      expect(loggerService.error).to.not.be.undefined;
      expect(loggerService.warn).to.not.be.undefined;
      expect(loggerService.info).to.not.be.undefined;
      expect(loggerService.verbose).to.not.be.undefined;
      expect(loggerService.debug).to.not.be.undefined;
      expect(loggerService.silly).to.not.be.undefined;
    });
  });
});
