const emailService = require('../../app/services/email');
const nock = require('nock');

describe('Email Service', () => {
  it('should send the status', (done) => {
    const scope = nock('http://localhost:5000')
      .post('/api/v1/email', {
        id: 'abc123',
        fileName: 'foobar',
        status: 'good'
      })
      .reply(201);

    emailService.sendStatus('abc123', 'foobar', 'good').then(() => {
      /* nock is handling most of the testing here
         if the parameters don't match up it will throw an error
       */
      scope.done();
      done();
    }).catch(done);
  });
});
