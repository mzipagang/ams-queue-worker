const sinon = require('sinon');
const chai = require('chai');
const proxyquire = require('proxyquire').noCallThru();
const sinonChai = require('sinon-chai');
const sqsConsumer = require('sqs-consumer');
const EventEmitter = require('events');
const jobs = require('../../app/services/jobs');

chai.use(sinonChai);
const expect = chai.expect;

const createValidFileMessage = () => {
  const BodyMessage = {
    scanResult: {
      bucket: 'test',
      key: '1234/key.txt',
      infected: false
    }
  };
  const Body = {
    Message: JSON.stringify(BodyMessage)
  };
  return {
    ReceiptHandle: '132434',
    Body: JSON.stringify(Body)
  };
};

const createValidJobMessage = () => {
  const body = {
    jobName: 'TEST_JOB',
    body: {
      data: 1
    }
  };

  return {
    Body: JSON.stringify(body)
  };
};

describe('Service: Queue', () => {
  let queue;
  const consumers = [];
  let processResult = null;

  before(() => {

  });

  beforeEach(() => {
    const createConsumer = () => {
      const eventEmitter = new EventEmitter();
      eventEmitter.stop = sinon.stub();
      eventEmitter.start = sinon.stub();
      return eventEmitter;
    };

    sinon.stub(sqsConsumer, 'create').callsFake((options) => {
      const consumer = {
        options,
        consumer: createConsumer()
      };
      consumers.push(consumer);
      return consumer.consumer;
    });

    queue = proxyquire('../../app/services/queue', {
      '../../process-file': (data, cb) => {
        cb(processResult);
      }
    });
  });

  afterEach(() => {
    sqsConsumer.create.restore();
  });

  it('should load properly', () => {
    expect(queue.start).to.not.be.undefined;
    expect(queue.stop).to.not.be.undefined;
  });

  it('should start properly', () => queue.start());

  describe('job messages', () => {
    beforeEach(() => {
      processResult = null;
    });

    before(() => {
      jobs.TEST_JOB = {
        jobName: 'TEST_JOB',
        handler: (body, cb) => {
          cb(processResult);
        }
      };
    });

    after(() => {
      delete jobs.TEST_JOB;
    });

    it('should be processed properly', () => {
      const fileConsumer = consumers[0];
      const handler = fileConsumer.options.handleMessage;
      const message = createValidJobMessage();

      handler(message, (err) => {
        expect(err).to.be.undefinred;
      });
    });

    it('should handle an unknown message', () => {
      const fileConsumer = consumers[0];
      const handler = fileConsumer.options.handleMessage;
      const message = {};

      handler(message, (err) => {
        expect(err).to.be.definred;
      });
    });

    it('should handle an error while processing', () => {
      const fileConsumer = consumers[0];
      const handler = fileConsumer.options.handleMessage;
      const message = createValidJobMessage();
      processResult = new Error('Fake error');

      handler(message, (err) => {
        expect(err).to.equal(processResult);
      });
    });
  });

  describe('file messages', () => {
    beforeEach(() => {
      processResult = null;
    });

    it('should be processed properly', () => {
      const fileConsumer = consumers[5];
      const handler = fileConsumer.options.handleMessage;
      const message = createValidFileMessage();

      handler(message, (err) => {
        expect(err).to.be.undefinred;
      });
    });

    it('should handle an unknown messages', () => {
      const fileConsumer = consumers[5];
      const handler = fileConsumer.options.handleMessage;
      const message = {};

      handler(message, (err) => {
        expect(err).to.not.be.undefined;
      });
    });

    it('should handle an error while processing', () => {
      const fileConsumer = consumers[5];
      const handler = fileConsumer.options.handleMessage;
      const message = createValidFileMessage();
      processResult = new Error('Fake Error');

      handler(message, (err) => {
        expect(err).to.equal(processResult);
      });
    });
  });

  it('should stop when called', () => {
    consumers.forEach((consumer) => {
      sinon.stub(consumer.consumer, 'on').callsFake((eventName, cb) => {
        setImmediate(() => cb());
      });
    });

    return queue.stop();
  });
});
