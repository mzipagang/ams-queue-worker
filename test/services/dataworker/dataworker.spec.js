const mockery = require('mockery');
const sinon = require('sinon');
const Bluebird = require('bluebird');
const chai = require('chai');

const MockLogger = require('../../mocks/services/logger-mock')();

describe('Service: dataWorker', () => {
  let warnSpy = null;
  let errorSpy = null;

  beforeEach(async () => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });

    mockery.registerMock(
        '../logger',
        MockLogger
      );
    warnSpy = sinon.spy(MockLogger, 'warn');
    errorSpy = sinon.spy(MockLogger, 'error');
  });

  afterEach(async () => {
    mockery.disable();
    mockery.deregisterAll();
    warnSpy.restore();
    errorSpy.restore();
  });

  it('should work on 200 response', async () => {
    mockery.registerMock('request-promise', () =>
        Bluebird.resolve({
          statusCode: 200,
          body:
            'a body',
          headers: {
            'content-type': 'text/html; charset=UTF-8',
            'referrer-policy': 'no-referrer',
            'content-length': '1590',
            date: 'Wed, 22 Aug 2018 15:52:18 GMT',
            connection: 'close'
          },
          request: {
            uri: {
              protocol: 'http:',
              slashes: true,
              auth: null,
              host: 'www.google.com',
              port: 80,
              hostname: 'www.google.com',
              hash: null,
              search: null,
              query: null,
              pathname: '/this-page-does-not-exist.html',
              path: '/this-page-does-not-exist.html',
              href: 'http://www.google.com/this-page-does-not-exist.html'
            },
            method: 'GET',
            headers: { accept: 'application/json' }
          }
        })
      );

    const dataWorker = require('../../../app/services/dataWorker');
    await dataWorker.get('/', {}, {});
    chai.expect(warnSpy.called).to.be.false;
    chai.expect(errorSpy.called).to.be.false;
  });

  it('should work on 304 response', async () => {
    mockery.registerMock('request-promise', () =>
        Bluebird.resolve({
          statusCode: 304,
          body:
            'a body',
          headers: {
            'content-type': 'text/html; charset=UTF-8',
            'referrer-policy': 'no-referrer',
            'content-length': '1590',
            date: 'Wed, 22 Aug 2018 15:52:18 GMT',
            connection: 'close'
          },
          request: {
            uri: {
              protocol: 'http:',
              slashes: true,
              auth: null,
              host: 'www.google.com',
              port: 80,
              hostname: 'www.google.com',
              hash: null,
              search: null,
              query: null,
              pathname: '/this-page-does-not-exist.html',
              path: '/this-page-does-not-exist.html',
              href: 'http://www.google.com/this-page-does-not-exist.html'
            },
            method: 'GET',
            headers: { accept: 'application/json' }
          }
        })
      );

    const dataWorker = require('../../../app/services/dataWorker');
    await dataWorker.get('/', {}, {});
    chai.expect(warnSpy.called).to.be.true;
    chai.expect(errorSpy.called).to.be.false;
  });

  it('should work on 500 response', async () => {
    mockery.registerMock('request-promise', () =>
        Bluebird.resolve({
          statusCode: 500,
          body:
            'a body',
          headers: {
            'content-type': 'text/html; charset=UTF-8',
            'referrer-policy': 'no-referrer',
            'content-length': '1590',
            date: 'Wed, 22 Aug 2018 15:52:18 GMT',
            connection: 'close'
          },
          request: {
            uri: {
              protocol: 'http:',
              slashes: true,
              auth: null,
              host: 'www.google.com',
              port: 80,
              hostname: 'www.google.com',
              hash: null,
              search: null,
              query: null,
              pathname: '/this-page-does-not-exist.html',
              path: '/this-page-does-not-exist.html',
              href: 'http://www.google.com/this-page-does-not-exist.html'
            },
            method: 'GET',
            headers: { accept: 'application/json' }
          }
        })
      );

    const dataWorker = require('../../../app/services/dataWorker');

    try {
      // Chai has trouble with expecting an exception from an async function.
      // So we call the dataworker, and use the try catch to catch the error, then in the catch, we continue our test.
      await dataWorker.get('/', {}, {});
    } catch (ex) {
      chai.expect(warnSpy.called).to.be.false;
      chai.expect(errorSpy.called).to.be.true;
      return;
    }
    // if we get here, an exception was not thrown, so we need to fail the test.
    chai.assert.fail(0, 1, 'dataWorker did not throw an error on a 500');
  });
});
