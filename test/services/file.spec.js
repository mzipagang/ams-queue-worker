const fileService = require('../../app/services/file');
const awsService = require('../../app/services/aws');
const dataWorkerHost = require('../../app/config').DATA_WORKER.ROOT_URL;
const nock = require('nock');
const fs = require('fs');
const tmp = require('tmp');
const sinon = require('sinon');
const chai = require('chai');
const sinonChai = require('sinon-chai');

chai.use(sinonChai);
const expect = chai.expect;

describe('File Service', () => {
  it('should get the file import group by id', (done) => {
    const expectedFileImportGroup = { files: [] };
    const scope = nock(dataWorkerHost)
        .get('/api/v1/file_import_group/abc123')
        .reply(200, {
          data: {
            file_import_group: expectedFileImportGroup
          }
        });

    fileService.getFileImportGroup('abc123').then((fileImportGroup) => {
      expect(fileImportGroup).to.eql(expectedFileImportGroup);
      scope.done();
      done();
    }).catch(done);
  });

  it('should transfer the file to the data worker', (done) => {
    const key = 'abc123';
    sinon.stub(awsService, 'getS3FileFromBucket').callsFake(() => ({
      createReadStream: () => fs.createReadStream(tmp.fileSync().name)
    }));

    const scope = nock(dataWorkerHost)
        .post(`/api/v1/file_transfer/${key}`)
        .reply(201);

    fileService.transferFileToDataWorker('bucket', 'abc123').then(() => {
      scope.done();
      done();
    }).catch(done).then(() => awsService.getS3FileFromBucket.restore());
  });

  it('should download the file from the data worker', (done) => {
    const tmpPath = tmp.fileSync().name;
    const key = 'abc123';

    const stream = fs.createReadStream(tmp.fileSync().name);

    const scope = nock(dataWorkerHost)
        .get(`/api/v1/file_transfer/${key}`)
        .reply(200, () => stream);

    fileService.downloadFileFromDataWorker(key, tmpPath).then(() => {
      scope.done();
      done();
    }).catch(done);
  });

  it('should update the status of the file', (done) => {
    const fileImportGroupId = 'abc123';
    const fileId = 'xyz321';
    const data = {
      status: 'someStatus',
      fileValidations: [
        { message: 'warning1', row: 2 },
        { message: 'warning2' }
      ]
    };
    const expectedFile = { id: fileId };
    const scope = nock(dataWorkerHost)
        .patch(`/api/v1/file_import_group/${fileImportGroupId}/file/${fileId}`, data)
        .reply(200, {
          data: {
            file_import_group: {
              files: [expectedFile]
            }
          }
        });

    fileService.updateFile(fileImportGroupId, fileId, data).then((response) => {
      expect(response).to.eql(expectedFile);
      scope.done();
      done();
    }).catch(done);
  });
});
