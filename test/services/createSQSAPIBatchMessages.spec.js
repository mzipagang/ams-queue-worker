const aws = require('../../app/services/aws');
const sinon = require('sinon');
const chai = require('chai');
const sinonChai = require('sinon-chai');

const createSQSAPIBatchMessages = require('../../app/services/createSQSAPIBatchMessages');

const expect = chai.expect;

chai.use(sinonChai);

describe('Service: createSQSAPIBatchMessages', () => {
  let sqsStub;
  let sendMessageSpy;

  beforeEach(() => {
    sendMessageSpy = sinon.spy();
    sqsStub = sinon.stub(aws.SQS, 'sendMessageBatch').callsFake((params, cb) => { cb(null, sendMessageSpy()); });
  });

  afterEach(() => {
    sqsStub.restore();
    sendMessageSpy.reset();
  });

  describe('create()', () => {
    it('should call SQS sendMessage method', async () => {
      const priority = 1;
      const requests = [];
      await createSQSAPIBatchMessages(priority, requests);
      expect(sendMessageSpy).to.have.been.called;
    });

    it('should throw an error for priority that is not 1 or 2', async () => {
      const priority = 3;
      const requests = [];
      try {
        await createSQSAPIBatchMessages(priority, requests);
      } catch (err) {
        expect(err.message).to.equal('Priority can only be values 1 to 2.');
      }
    });
  });
});
