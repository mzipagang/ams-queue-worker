const { expect } = require('chai');
const characterDelimitedFileParser = require('../../app/services/fileParser/characterDelimitedFileParser');

const mockLines = [
  { lineNumber: 1, text: 'George|Washington|Unaffiliated' },
  { lineNumber: 2, text: 'John|Adams|Federalist' },
  { lineNumber: 3, text: 'Thomas|Jefferson|Democratic-Republican' }
];

const linesWithHeaders = [{ text: 'First|Last|Party' }]
  .concat(mockLines)
  .map((line, index) => Object.assign({}, line, { lineNumber: index + 1 }));

describe('Character Delimited File Parser', () => {
  it('should use passed in headers', () => {
    const parse = characterDelimitedFileParser('|', ['First', 'Last', 'Party']).parser();
    const results = mockLines.map(line => parse(line));

    expect(results.length).to.equal(3);
    expect(results[0]).to.eql({
      data: {
        First: 'George',
        Last: 'Washington',
        Party: 'Unaffiliated'
      },
      lineNumber: 1
    });
  });

  it('should use the first row as headers if none are supplied', () => {
    const parse = characterDelimitedFileParser('|').parser();
    const results = linesWithHeaders.map(line => parse(line));

    expect(results.length).to.equal(4);
    expect(results[1]).to.eql({
      data: {
        First: 'George',
        Last: 'Washington',
        Party: 'Unaffiliated'
      },
      lineNumber: 2
    });
  });

  it('should return null for lines with no text', () => {
    const linesWithOneMissingData = mockLines.concat({ text: '', lineNumber: 4 });
    const parse = characterDelimitedFileParser('|', ['First', 'Last', 'Party']).parser();
    const results = linesWithOneMissingData.map(line => parse(line));

    expect(results.length).to.equal(4);
    expect(results[3]).to.be.null;
  });

  it('should throw an error', () => {
    const invalidRow = { lineNumber: 1, text: '\\' };
    // need to use `.bind` here because chai expects a function expression and we need to still be able
    // to pass arguments -- in this case the invalid row. Alternatively we could use a function in the expect
    // `expect(() => parse(invalidRow)).to.throw()`
    const parse = characterDelimitedFileParser('|', ['First', 'Last', 'Party']).parser().bind(null, invalidRow);

    // not sure how to combine these into one assertion
    expect(parse).to.throw(/Failed to parse row/).with.property('lineNumber', 1);
    expect(parse).to.throw(/Failed to parse row/).with.property('text', '\\');
  });
});

