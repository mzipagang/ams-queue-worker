const path = require('path');

const getTestFile = filename => path.join(__dirname, filename);

module.exports = {
  getFile: getTestFile
};
