const sinon = require('sinon');
const chai = require('chai');
const sinonChai = require('sinon-chai');
const proxyquire = require('proxyquire').noCallThru();

const queue = require('../app/services/queue');

chai.use(sinonChai);

describe('App', () => {
  let processOnStub;
  let processExitStub;
  let queueMock;

  beforeEach(() => {
    processOnStub = sinon.stub(process, 'on');
    processExitStub = sinon.stub(process, 'exit');

    queueMock = sinon.mock(queue);
  });

  afterEach(() => {
    processOnStub.restore();
    processExitStub.restore();
    queueMock.restore();
  });

  it('should require index.js', () => {
    queueMock
        .expects('start')
        .resolves(true);

    const app = proxyquire('../app', {});
    return app.queue;
  });

  it('should handle an error when starting queue', () => {
    queueMock
        .expects('start')
        .rejects(new Error('Failed to start queue'));

    const app = proxyquire('../app', {});

    return app.queue.then(() => {

    });
  });

  it('should handle SIGTERM', (done) => {
    queueMock
        .expects('start')
        .resolves(true);

    queueMock
        .expects('stop')
        .resolves(true);

    processExitStub.callsFake(() => {
      done();
    });

    processOnStub.callsFake((val, cb) => {
      cb();
    });

    proxyquire('../app', {});
  });

  it('should handle SIGTERM with an error', (done) => {
    queueMock
        .expects('start')
        .resolves(true);

    queueMock
        .expects('stop')
        .rejects(new Error('Failed to start queue'));

    processExitStub.callsFake(() => {
      done();
    });

    processOnStub.callsFake((val, cb) => {
      cb();
    });

    proxyquire('../app', {});
  });
});
