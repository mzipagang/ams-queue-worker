const ignoreApmId = require('../../app/FilterStrategies/IgnoreApmId');
const { expect } = require('chai');

const strategyFunction = ignoreApmId();

const mockRows = Object.freeze([
  { data: { 'APM ID': '01' } },
  { data: { 'APM ID': '05' } },
  { data: { 'APM ID': '08' } },
  { data: { 'APM ID': '08' } },
  { data: { 'APM ID': '01' } }
]);

describe('IgnoreApmId Filter Strategy', () => {
  it('should ignore objects with an APM ID of 08', () => {
    const expected = [true, true, false, false, true];
    const results = mockRows.map(row => strategyFunction(row));

    expect(results).to.eql(expected);
  });
});

