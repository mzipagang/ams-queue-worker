const programIdHasMatchingApmId = require('../../app/FilterStrategies/ProgramIdHasMatchingApmId');
const dataWorkerUtils = require('../../app/services/dataWorkerUtils');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const chai = require('chai');

const expect = chai.expect;
chai.use(sinonChai);

const mockRows = Object.freeze([
  { data: { 'Program ID': '01' } },
  { data: { 'Program ID': '05' } },
  { data: { 'Program ID': '08' } },
  { data: { 'Program ID': '08' } },
  { data: { 'Program ID': '01' } }
]);

describe('ProgramIdHasMatchingApmId Filter Strategy', () => {
  let sandbox;

  before(() => {
    sandbox = sinon.sandbox.create();
    sandbox.stub(dataWorkerUtils, 'getAMSModels').resolves([
      { id: '01' },
      { id: '05' }
    ]);
  });

  after(() => {
    sandbox.restore();
  });

  afterEach(() => {
    sandbox.resetHistory();
    dataWorkerUtils.getAMSModels.restore();
  });


  it('should ignore rows where Program ID does not have corresponding Model ID', (done) => {
    programIdHasMatchingApmId().then((strategyFunction) => {
      const expected = [true, true, false, false, true];
      const results = mockRows.map(row => strategyFunction(row));

      expect(results).to.eql(expected);
      done();
    });
  });
});

