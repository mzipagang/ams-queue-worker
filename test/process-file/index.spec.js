const sinon = require('sinon');
const chai = require('chai');
const fs = require('fs');
const sinonChai = require('sinon-chai');
const config = require('../../app/config');
const emailService = require('../../app/services/email');
const awsService = require('../../app/services/aws');
const fileService = require('../../app/services/file');
const logger = require('../../app/services/logger');
const PacEntity = require('../../app/apm-files/PacEntity');

const { getFile } = require('../test_files');

const nock = require('nock');

chai.use(sinonChai);
const expect = chai.expect;

const handler = require('../../app/process-file');

function MockRequest() {
  Object.assign(this, {
    key: 'some/sample/key',
    bucket: 'sample-bucket',
    infected: false
  });
}

describe('process-file', () => {
  beforeEach(() => {
    nock(`${config.DATA_WORKER.ROOT_URL}`)
      .get('/api/v1/publish_history/execution_number') // query string based on sample-file.txt
      .query(true)
      .reply(200, {
        execution: 0
      });
  });

  describe('with infected file', () => {
    before(() => {
      sinon.stub(awsService, 'deleteS3FileInBucket').resolves();
      sinon.stub(emailService, 'sendStatus').resolves();
      sinon.stub(fileService, 'updateFile').resolves();
      sinon.stub(fileService, 'transferFileToDataWorker').resolves();
    });

    after(() => {
      awsService.deleteS3FileInBucket.restore();
      emailService.sendStatus.restore();
      fileService.updateFile.restore();
      fileService.transferFileToDataWorker.restore();
    });

    afterEach(() => {
      awsService.deleteS3FileInBucket.resetHistory();
      emailService.sendStatus.resetHistory();
      fileService.updateFile.resetHistory();
      fileService.transferFileToDataWorker.resetHistory();
    });

    const request = new MockRequest();
    request.infected = true;

    it('should delete infected file', (done) => {
      handler(request, () => {
        const args = awsService.deleteS3FileInBucket.getCall(0).args;

        expect(awsService.deleteS3FileInBucket.calledOnce).to.equal(true);
        expect(args[0]).to.equal('sample-bucket');
        expect(args[1]).to.equal('some/sample/key');
        done();
      });
    });

    it('should send a status email', (done) => {
      handler(request, () => {
        const args = emailService.sendStatus.getCall(0).args;

        expect(emailService.sendStatus.calledOnce).to.equal(true);
        expect(args[0]).to.equal('sample');
        expect(args[1]).to.equal('some/sample/key');
        expect(args[2]).to.equal('File has been detected as a virus, deleting infected file');
        done();
      });
    });

    it('should send a file update', (done) => {
      handler(request, () => {
        const args = fileService.updateFile.getCall(0).args;

        expect(fileService.updateFile.calledOnce).to.equal(true);
        expect(args[0]).to.equal('some');
        expect(args[1]).to.equal('sample');
        expect(JSON.stringify(args[2])).to.equal(JSON.stringify({ status: 'error', error: 'Virus detected.' }));
        done();
      });
    });

    it('should continue without an error', (done) => {
      handler(request, (error) => {
        expect(fileService.transferFileToDataWorker.called).to.equal(false);
        expect(error).to.be.undefined;
        done();
      });
    });
  });

  describe('with valid file', () => {
    describe('with invalid file import group', () => {
      const request = new MockRequest();
      let sandbox;

      before(() => {
        sandbox = sinon.sandbox.create();
        sandbox.stub(fileService, 'transferFileToDataWorker').resolves();
      });

      after(() => {
        sandbox.restore();
      });

      afterEach(() => {
        sandbox.resetHistory();
        fileService.getFileImportGroup.restore();
      });

      it('should finish with an error if file import group does not exist', (done) => {
        sandbox.stub(fileService, 'getFileImportGroup').resolves(false);

        handler(request, (error) => {
          expect(fileService.transferFileToDataWorker.called).to.equal(false);
          expect(error).to.be.undefined;
          done();
        });
      });

      it('should finish with an error if file import group is not open', (done) => {
        sandbox.stub(fileService, 'getFileImportGroup').resolves({ status: 'something-not-open' });

        handler(request, (error) => {
          expect(fileService.transferFileToDataWorker.called).to.equal(false);
          expect(error).to.be.undefined;
          done();
        });
      });
    });

    describe('with valid file import group', () => {
      const mockRequest = new MockRequest();
      let sandbox;

      before(() => {
        sandbox = sinon.sandbox.create();
        sandbox.stub(fileService, 'getFileImportGroup').resolves({
          status: 'open',
          files: [{ id: 'sample', import_file_type: 'pac-entity' }]
        });
        sandbox.stub(fileService, 'transferFileToDataWorker').resolves();
        sandbox.stub(fileService, 'updateFile').resolves({
          file_name: 'something',
          file_location: 'somewhere',
          import_file_type: 'pac-entity'
        });
        sandbox.stub(fileService, 'downloadFileFromDataWorker').resolves();
        sandbox.stub(emailService, 'sendStatus').resolves();
        const createReadStream = fs.createReadStream.bind(fs);
        sandbox.stub(fs, 'createReadStream').callsFake(() => createReadStream(`${__dirname}/sample-file.txt`));
      });

      beforeEach(() => {
        nock(`${config.DATA_WORKER.ROOT_URL}`)
        .post('/api/v1/file_import_group/some/file/sample/pac/entity/batch')
        .reply(200, {
          data: {
            values: [
              { success: true }
            ]
          }
        });
      });

      after(() => {
        sandbox.restore();
      });

      afterEach(() => {
        sandbox.resetHistory();
      });

      it('should transfer the file to the data worker', (done) => {
        handler(mockRequest, (err) => {
          expect(fileService.transferFileToDataWorker.calledWith('sample-bucket', 'some/sample/key')).to.equal(true);
          done(err);
        });
      });

      it('should send a status email that the file has been uploaded to s3', (done) => {
        handler(mockRequest, (err) => {
          expect(emailService.sendStatus.calledWith('sample', 'some/sample/key',
            'File has successfully uploaded to s3, processing file')).to.equal(true);
          done(err);
        });
      });

      it('should update the file location', (done) => {
        const expectedBody = {
          file_location: `${config.DATA_WORKER.S3_BUCKET}:some/sample/key`
        };
        handler(mockRequest, (err) => {
          expect(fileService.updateFile.firstCall.calledWith('some', 'sample', expectedBody)).to.equal(true);
          done(err);
        });
      });
    });

    describe('reading', () => {
      const mockRequest = new MockRequest();

      let sandbox;

      before(() => {
        sandbox = sinon.sandbox.create();
        sandbox.stub(fileService, 'getFileImportGroup').resolves({
          status: 'open',
          files: [{ id: 'sample', import_file_type: 'pac-entity' }]
        });
        sandbox.stub(fileService, 'transferFileToDataWorker').resolves();
        sandbox.stub(fileService, 'updateFile').resolves({
          file_name: 'something',
          file_location: 'somewhere',
          import_file_type: 'pac-entity'
        });
        sandbox.stub(fileService, 'downloadFileFromDataWorker').resolves();
        sandbox.stub(emailService, 'sendStatus').resolves();
        sandbox.stub(PacEntity, 'validateHeader').resolves(true);
      });

      beforeEach(() => {
        const createReadStream = fs.createReadStream.bind(fs);
        sandbox.stub(fs, 'createReadStream').callsFake(() => createReadStream(`${__dirname}/sample-file.txt`));
      });

      after(() => {
        sandbox.restore();
      });

      afterEach(() => {
        fs.createReadStream.restore();
        sandbox.resetHistory();
      });

      it('should handle the successful values', (done) => {
        const scope = nock(`${config.DATA_WORKER.ROOT_URL}`)
          .post('/api/v1/file_import_group/some/file/sample/pac/entity/batch')
          .reply(200, {
            data: {
              values: [
                { success: true }
              ]
            }
          });

        handler(mockRequest, (err) => {
          expect(scope.isDone()).to.equal(true);
          done(err);
        });
      });

      it('should handle the unsuccessful values', (done) => {
        nock(`${config.DATA_WORKER.ROOT_URL}`)
          .post('/api/v1/file_import_group/some/file/sample/pac/entity/batch')
          .reply(200, {
            data: {
              values: [
                { success: false }
              ]
            }
          });

        const scope = nock(`${config.DATA_WORKER.ROOT_URL}`)
          .post('/api/v1/file_import_group/some/error', {
            file_import_group_id: 'some',
            file_id: 'sample',
            error: 'There was an error while processing this line.',
            line_number: 2
          })
          .reply();
        handler(mockRequest, (err) => {
          expect(scope.isDone()).to.equal(true);
          done(err);
        });
      });

      it('should handle non 200 responses', (done) => {
        nock(`${config.DATA_WORKER.ROOT_URL}`)
        .get('/api/v1/publish_history/execution_number') // query string based on sample-file.txt
        .query({
          performance_year: 2017,
          snapshot: 'P',
          import_file_type: 'pac-entity'
        })
        .reply(200, {
          execution: 0
        });

        nock(`${config.DATA_WORKER.ROOT_URL}`)
          .post('/api/v1/file_import_group/some/file/sample/pac/entity/batch')
          .reply(400, 'foobar');

        const scope = nock(`${config.DATA_WORKER.ROOT_URL}`)
          .post('/api/v1/file_import_group/some/error', {
            file_import_group_id: 'some',
            file_id: 'sample',
            // the following is not correct but there is not currently
            // a way with nock to mock the statusMessage of the response
            error: 'null',
            line_number: 0
          })
          .reply();

        handler(mockRequest, (err) => {
          expect(scope.isDone()).to.equal(true);
          done(err);
        });
      });
    });
  });

  describe('on error', () => {
    const mockRequest = new MockRequest();
    let sandbox;

    before(() => {
      sandbox = sinon.sandbox.create();
      sandbox.stub(fileService, 'transferFileToDataWorker').rejects();
      sandbox.stub(fileService, 'updateFile').resolves();
      sandbox.stub(emailService, 'sendStatus').resolves();
    });

    afterEach(() => {
      sandbox.resetHistory();
    });

    after(() => {
      sandbox.restore();
    });

    it('should send an email', (done) => {
      nock(`${config.DATA_WORKER.ROOT_URL}`)
        .get('/api/v1/file_import_group/some')
        .reply(200, {
          data: {
            file_import_group: {
              status: 'open',
              files: [{ id: 'sample', import_file_type: 'pac-entity' }]
            }
          }
        });

      handler(mockRequest, (err) => {
        expect(err).to.be.an('error');
        expect(emailService.sendStatus.calledWith('sample', 'some/sample/key',
          'Error in file uploading and processing')).to.equal(true);
        done();
      });
    });
  });

  describe('on error when handling an error', () => {
    const mockRequest = new MockRequest();
    let sandbox;

    before(() => {
      sandbox = sinon.sandbox.create();
      sandbox.stub(fileService, 'transferFileToDataWorker').rejects();
      sandbox.stub(emailService, 'sendStatus').rejects();
      sandbox.stub(logger, 'error');
    });

    afterEach(() => {
      sandbox.resetHistory();
    });

    after(() => {
      sandbox.restore();
    });

    it('should write to logger', (done) => {
      nock(`${config.DATA_WORKER.ROOT_URL}`)
        .get('/api/v1/file_import_group/some')
        .reply(200, {
          data: {
            file_import_group: {
              status: 'open',
              files: [{ id: 'sample', import_file_type: 'pac-entity' }]
            }
          }
        });

      handler(mockRequest, (err) => {
        expect(err).to.not.be.undefined;
        expect(logger.error.called).to.equal(true);
        done();
      });
    });
  });

  describe('for all apm file types', () => {
    const mockRequest = new MockRequest();
    let sandbox;

    const realCreateReadStream = fs.createReadStream;

    beforeEach(() => {
      sandbox = sinon.sandbox.create();

      sandbox.stub(fileService, 'transferFileToDataWorker').resolves();

      sandbox.stub(fileService, 'downloadFileFromDataWorker').resolves();
      sandbox.stub(emailService, 'sendStatus').resolves();
      config.LOG_IF_MORE_THEN_LINES = 1;
      config.MAX_ERROR_BYTES = 48 * 1024 * 1024;
    });

    afterEach(() => {
      sandbox.restore();
      nock.cleanAll();
    });

    const fileTypes = [
      {
        importFileType: 'aco-entity',
        filelocation: 'aco_entity'
      },
      {
        importFileType: 'cmmi-entity',
        filelocation: 'cmmi_entity'
      },
      {
        importFileType: 'cmmi-model',
        filelocation: 'cmmi_model'
      },
      {
        importFileType: 'cmmi-provider',
        filelocation: 'cmmi_provider'
      },
      {
        importFileType: 'cmmi-subdivision',
        filelocation: 'cmmi_subdivision'
      },
      {
        importFileType: 'ngaco-preferred-provider',
        filelocation: 'ngaco_provider'
      },
      {
        importFileType: 'pac-entity',
        filelocation: 'pac_entity'
      },
      {
        importFileType: 'pac-lvt',
        filelocation: 'pac_lvt'
      },
      {
        importFileType: 'pac-model',
        filelocation: 'pac_model'
      },
      {
        importFileType: 'pac-qp_score',
        filelocation: 'pac_qp-score'
      },
      {
        importFileType: 'individual_qp_status',
        filelocation: 'individual_qp_status'
      },
      {
        importFileType: 'individual_qp_threshold',
        filelocation: 'individual_qp_threshold'
      },
      {
        importFileType: 'entity_qp_status',
        filelocation: 'entity_qp_status'
      },
      {
        importFileType: 'entity_qp_threshold',
        filelocation: 'entity_qp_threshold'
      },
      {
        importFileType: 'entity-eligibility',
        filelocation: 'entity_eligibility'
      },
      {
        importFileType: 'pac-qp_status',
        filelocation: 'pac_qp-status'
      },
      {
        importFileType: 'pac-subdivision',
        filelocation: 'pac_subdivision'
      },
      {
        importFileType: 'abridged_qp_status',
        filelocation: 'abridged_qp_status'
      },
      {
        importFileType: 'abridged_lvt',
        filelocation: 'abridged_lvt'
      },
      {
        importFileType: 'abridged_qp_score',
        filelocation: 'abridged_qp_score'
      },
      {
        importFileType: 'pac-qp_category_score',
        filelocation: 'pac-qp_category_score'
      }
    ];

    fileTypes.forEach((fileType) => {
      it(`should handle the ${fileType.importFileType} file`, (done) => {
        const importFileType = fileType.importFileType;
        const filelocation = fileType.filelocation;

        sandbox.stub(fileService, 'getFileImportGroup').resolves({
          status: 'open',
          files: [{ id: 'sample', import_file_type: importFileType }]
        });

        sandbox.stub(fileService, 'updateFile').resolves({
          file_name: 'something',
          file_location: 'somewhere',
          import_file_type: importFileType
        });

        sandbox.stub(fs, 'createReadStream').callsFake(() =>
          realCreateReadStream(getFile(`mock_files/${filelocation}.txt`))
        );

        nock(`${config.DATA_WORKER.ROOT_URL}`)
          .post(() => true)
          .reply(200, {
            data: {
              values: [
                { success: true }
              ]
            }
          });

        handler(mockRequest, (err) => {
          done(err);
        });
      });
    });

    it('should handle the mdm provider file', (done) => {
      const importFileType = 'mdm-provider';
      const filelocation = 'mdm_provider';

      sandbox.stub(fileService, 'getFileImportGroup').resolves({
        status: 'open',
        files: [{ id: 'sample', import_file_type: importFileType }]
      });

      sandbox.stub(fileService, 'updateFile').resolves({
        file_name: 'something',
        file_location: 'somewhere',
        import_file_type: importFileType
      });

      sandbox.stub(fs, 'createReadStream').callsFake(() =>
        realCreateReadStream(getFile(`mock_files/${filelocation}.txt`))
      );

      nock(`${config.DATA_WORKER.ROOT_URL}`)
          .post(() => true)
          .reply(200, {
            data: {
              values: [
                { success: true }
              ]
            }
          });

      nock(`${config.DATA_WORKER.ROOT_URL}`)
          .get('/api/v1/pac/model')
          .reply(200, {
            pac_models: [
              {
                id: '07',
                name: 'Independence at Home Demonstration',
                advanced_apm_flag: 'N',
                mips_apm_flag: 'N',
                quality_reporting_category_code: '3'
              }
            ]
          });

      handler(mockRequest, (err) => {
        done(err);
      });
    });

    it('should handle the pac provider file', (done) => {
      const importFileType = 'pac-provider';
      const filelocation = 'pac_provider';

      sandbox.stub(fileService, 'getFileImportGroup').resolves({
        status: 'open',
        files: [{ id: 'sample', import_file_type: importFileType }]
      });

      sandbox.stub(fileService, 'updateFile').resolves({
        file_name: 'something',
        file_location: 'somewhere',
        import_file_type: importFileType
      });

      sandbox.stub(fs, 'createReadStream').callsFake(() =>
        realCreateReadStream(getFile(`mock_files/${filelocation}.txt`))
      );

      nock(`${config.DATA_WORKER.ROOT_URL}`)
        .post(() => true)
        .reply(200, {
          data: {
            values: [
              { success: true }
            ]
          }
        });

      const npis = new Array(5002).fill('1234567890');

      nock(`${config.DATA_WORKER.ROOT_URL}`)
          .get('/api/v1/file_import_group/some/file/sample/duplicates')
          .reply(200, {
            data: {
              duplicates: [
                {
                  tin: '123456789',
                  entities: [
                    {
                      entity_id: '1',
                      npis
                    }
                  ]
                }
              ]
            }
          });

      nock(`${config.DATA_WORKER.ROOT_URL}`)
          .post('/api/v1/file_import_group/some/file/sample/duplicates')
          .reply(200, {
            data: {
              success: true
            }
          });

      nock(`${config.DATA_WORKER.ROOT_URL}`)
          .post('/api/v1/file_import_group/some/file/sample/duplicates')
          .reply(200, {
            data: {
              success: true
            }
          });

      nock(`${config.DATA_WORKER.ROOT_URL}`)
        .get('/api/v1/publish_history/execution_number')
        .query(true) // because of various files used, will not test query string
        .reply(200, {
          execution: 0
        });

      handler(mockRequest, (err) => {
        done(err);
      });
    });

    it('should handle a manual file with warnings in file', (done) => {
      const importFileType = 'pac-entity';
      const filelocation = 'pac_entity_file_warning';

      sandbox.stub(fileService, 'getFileImportGroup').resolves({
        status: 'open',
        files: [{ id: 'sample', import_file_type: importFileType }]
      });

      sandbox.stub(fileService, 'updateFile').resolves({
        file_name: 'something',
        file_location: 'somewhere',
        import_file_type: importFileType
      });

      sandbox.stub(fs, 'createReadStream').callsFake(() =>
        realCreateReadStream(getFile(`mock_files/${filelocation}.txt`))
      );

      nock(`${config.DATA_WORKER.ROOT_URL}`)
        .post(() => true)
        .reply(200, {
          data: {
            values: [
              { success: true }
            ]
          }
        });

      handler(mockRequest, err => done(err));
    });

    it('should handle a file with too many validation messages', (done) => {
      const importFileType = 'pac-entity';
      const filelocation = 'pac_entity_file_warning';

      config.MAX_ERROR_BYTES = 0;

      sandbox.stub(fileService, 'getFileImportGroup').resolves({
        status: 'open',
        files: [{ id: 'sample', import_file_type: importFileType }]
      });

      sandbox.stub(fileService, 'updateFile').resolves({
        file_name: 'something',
        file_location: 'somewhere',
        import_file_type: importFileType
      });

      sandbox.stub(fs, 'createReadStream').callsFake(() =>
        realCreateReadStream(getFile(`mock_files/${filelocation}.txt`))
      );

      nock(`${config.DATA_WORKER.ROOT_URL}`)
        .post(() => true)
        .reply(200, {
          data: {
            values: [
              { success: true }
            ]
          }
        });

      handler(mockRequest, (err) => {
        expect(err).not.be.null;
        done();
      });
    });

    it('should handle beneficiary file', (done) => {
      const importFileType = 'beneficiary';
      const filelocation = 'beneficiary';

      nock(`${config.DATA_WORKER.ROOT_URL}`)
          .get('/api/v1/pac/model')
          .reply(200, {
            pac_models: [
              {
                id: '07',
                name: 'Independence at Home Demonstration',
                advanced_apm_flag: 'N',
                mips_apm_flag: 'N',
                quality_reporting_category_code: '3'
              }
            ]
          });

      sandbox.stub(fileService, 'getFileImportGroup').resolves({
        status: 'open',
        files: [{ id: 'sample', import_file_type: importFileType }]
      });

      sandbox.stub(fileService, 'updateFile').resolves({
        file_name: 'something',
        file_location: 'somewhere',
        import_file_type: importFileType
      });

      sandbox.stub(fs, 'createReadStream').callsFake(() =>
        realCreateReadStream(getFile(`mock_files/${filelocation}.txt`))
      );

      nock(`${config.DATA_WORKER.ROOT_URL}`)
        .post(() => true)
        .reply(200, {
          data: {
            values: [
              { success: true }
            ]
          }
        });

      handler(mockRequest, (err) => {
        done(err);
      });
    });

    it('should handle beneficiary file with model id of 23', (done) => {
      const importFileType = 'beneficiary';
      const filelocation = 'beneficiary_with_23_apmId';

      nock(`${config.DATA_WORKER.ROOT_URL}`)
          .get('/api/v1/pac/model')
          .reply(200, {
            pac_models: [
              {
                id: '22',
                name: 'Independence at Home Demonstration',
                advanced_apm_flag: 'N',
                mips_apm_flag: 'N',
                quality_reporting_category_code: '3'
              }
            ]
          });

      sandbox.stub(fileService, 'getFileImportGroup').resolves({
        status: 'open',
        files: [{ id: 'sample', import_file_type: importFileType }]
      });

      sandbox.stub(fileService, 'updateFile').resolves({
        file_name: 'something',
        file_location: 'somewhere',
        import_file_type: importFileType
      });

      sandbox.stub(fs, 'createReadStream').callsFake(() =>
        realCreateReadStream(getFile(`mock_files/${filelocation}.txt`))
      );

      nock(`${config.DATA_WORKER.ROOT_URL}`)
        .post(() => true)
        .reply(200, {
          data: {
            values: [
              { success: true }
            ]
          }
        });

      handler(mockRequest, (err) => {
        done(err);
      });
    });

    it('should handle mdm beneficiary file', (done) => {
      const importFileType = 'beneficiary_mdm';
      const filelocation = 'beneficiary_mdm';

      sandbox.stub(fileService, 'getFileImportGroup').resolves({
        status: 'open',
        files: [{ id: 'sample', import_file_type: importFileType }]
      });

      sandbox.stub(fileService, 'updateFile').resolves({
        file_name: 'something',
        file_location: 'somewhere',
        import_file_type: importFileType
      });

      sandbox.stub(fs, 'createReadStream').callsFake(() =>
        realCreateReadStream(getFile(`mock_files/${filelocation}.txt`))
      );


      nock(`${config.DATA_WORKER.ROOT_URL}`)
        .post('/api/v1/file_import_group/some/file/sample/apm/beneficiary/batch')
        .reply(200, {
          data: {
            values: [
              { success: true }
            ]
          }
        });

      handler(mockRequest, (err) => {
        done(err);
      });
    });
  });
});

