module.exports = () => () => ({
  pipe: () => ({
    on: (eventName, cb) => {
      if (eventName === 'finish') {
        setTimeout(cb);
      }
    }
  })
});
