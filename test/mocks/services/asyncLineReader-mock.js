const Promise = require('bluebird');

module.exports = lines => ({
  asyncLineReader: (readStream, onLines) => new Promise((resolve, reject) => {
    const linesCopy = (lines || []).concat([]);
    let lineNumber = 0;
    const send = () => {
      const linesToSend = linesCopy.splice(0, 10).map((line) => {
        lineNumber += 1;

        return {
          text: line,
          lineNumber
        };
      });
      if (linesToSend.length > 0) {
        return Promise.try(() => onLines(linesToSend))
            .then(() => send())
            .catch(err => reject(err));
      }

      return resolve();
    };

    send();
  })
});
