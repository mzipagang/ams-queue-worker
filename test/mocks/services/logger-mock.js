module.exports = () => ({
  error: () => { },
  warn: () => { },
  info: () => { },
  verbose: () => { },
  debug: () => { },
  silly: () => { }
});
