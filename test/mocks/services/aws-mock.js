const Readable = require('stream').Readable;

module.exports = () => ({
  S3: {
    getObject: (params) => {
      const stream = new Readable();
      stream.push('testh1, testh2, testh3\n test1,test2,test3');
      stream.push(null);

      if (params) {
        return { createReadStream: () => stream };
      }
      throw new Error('no params supplied to get object from s3');
    },
    headObject: (data, cb) => {
      setImmediate(() => {
        cb(null, { Metadata: {} });
      });
    },
    deleteObject: (data, cb) => {
      setImmediate(() => {
        cb(null);
      });
    }
  },
  SQS: {
    sendMessage: (params, cb) => {
      setTimeout(cb, 1);
    }
  }
});
