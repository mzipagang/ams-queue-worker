module.exports = {
  use: () => {
    delete process.env.AWS_ACCESS_KEY_ID;
    delete process.env.AWS_SECRET_ACCESS_KEY;
    process.env.NODE_ENV = 'dev';
    process.env.MONGODB = 'mongodb://localhost:12345/fake';
    process.env.SQS_QUEUE_URL = 'http://0.0.0.0:12345/fake-queue';
  }
};
