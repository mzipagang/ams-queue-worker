module.exports = {
  use: () => {
    process.env.AWS_ACCESS_KEY_ID = 'somekey';
    process.env.AWS_SECRET_ACCESS_KEY = 'somesecretkey';
    process.env.NODE_ENV = 'production';
    process.env.MONGODB = 'mongodb://localhost:12345/fake';
    process.env.SQS_QUEUE_URL = 'http://0.0.0.0:12345/fake-queue';
    process.env.API_URL = 'http://ams-api:2000';
  }
};
