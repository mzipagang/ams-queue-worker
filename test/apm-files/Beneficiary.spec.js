const { expect } = require('chai');
const Beneficiary = require('../../app/apm-files/Beneficiary');

describe('Beneficiary Mdm File', () => {
  describe('schemaMap', () => {
    it('should get the schema map', () => {
      const schemaMap = Beneficiary.schemaMap;
      expect(schemaMap).to.be.defined;
      expect(schemaMap['Entity ID']).to.equal('entity_id');
    });
  });

  describe('fromRawFile', () => {
    const rawBeneficiaryParsedData = {
      entity_id: 'A1234',
      link_key: '',
      hicn: '1234567890',
      mbi: '',
      dob: '20180101',
      gender: 'm',
      start_date: '20180101',
      end_date: '20190101'
    };

    it('should should return a Beneficiary object', () => {
      const parsedData = Beneficiary.fromRawFile(rawBeneficiaryParsedData);
      expect(parsedData).to.be.defined;
      expect(parsedData.entity_id).to.equal(rawBeneficiaryParsedData.entity_id);
    });
  });

  describe('validateHeader', () => {
    const sampleHeader = 'H|20180614|BENE|18';
    const sampleInvalidHeader = 'H|20180614|BENEZZ|18';

    it('should return true for a valid header', () => {
      const validationResult = Beneficiary.validateHeader(sampleHeader);
      expect(validationResult).to.be.true;
    });

    it('should return false for in invalid header', () => {
      const validationResult = Beneficiary.validateHeader(sampleInvalidHeader);
      expect(validationResult).to.be.false;
    });
  });

  describe('validateParsedTrailer', () => {
    const header = {
      'Record Identifier': 'T',
      Date: '20180101',
      'File Type': 'BENE',
      'APM ID': '01'
    };

    it('should return error value false for a valid trailer', () => {
      const trailer = {
        'Record Identifier': 'T',
        Date: '20180101',
        'File Type': 'BENE',
        'APM ID': '01',
        'Record Count': '1'
      };
      const trailerValidationResult = Beneficiary.validateParsedTrailer(trailer, header, 1);
      expect(trailerValidationResult.error).to.be.false;
    });

    it('should return error value true, warning value false for a missing `T` record identifier', () => {
      const trailer = {
        'Record Identifier': null,
        Date: '20180101',
        'File Type': 'BENE',
        'APM ID': '01',
        'Record Count': '1'
      };
      const trailerValidationResult = Beneficiary.validateParsedTrailer(trailer, header, 1);
      expect(trailerValidationResult.warning).to.be.false;
      expect(trailerValidationResult.error).to.be.true;
      expect(trailerValidationResult.message).to.equal('Invalid trailer in file.');
    });

    it('should return an error for an invalid file type', () => {
      const trailer = {
        'Record Identifier': 'T',
        Date: '20180101',
        'File Type': 'BENEZZ',
        'APM ID': '01',
        'Record Count': '1'
      };
      const trailerValidationResult = Beneficiary.validateParsedTrailer(trailer, header, 1);
      expect(trailerValidationResult).to.be.defined;
      expect(trailerValidationResult.error).to.be.defined;
      expect(trailerValidationResult.error).to.be.true;
      expect(trailerValidationResult.message).to.equal('File Type must be "BENE".');
    });

    it('should return an error when the apm id in the header does not match the apm id in the trailer', () => {
      const trailer = {
        'Record Identifier': 'T',
        Date: '20180102',
        'File Type': 'BENE',
        'APM ID': '02',
        'Record Count': '1'
      };
      const trailerValidationResult = Beneficiary.validateParsedTrailer(trailer, header, 1);
      expect(trailerValidationResult).to.be.defined;
      expect(trailerValidationResult.error).to.be.defined;
      expect(trailerValidationResult.error).to.be.true;
      expect(trailerValidationResult.message).to.equal('APM ID must match APM ID in Header Record and exist in AMS.');
    });

    it('should return warning:true error: false when count in the trailer does not match the # of detail rows', () => {
      const trailer = {
        'Record Identifier': 'T',
        Date: '20180102',
        'File Type': 'BENE',
        'APM ID': '01',
        'Record Count': '2'
      };
      const trailerValidationResult = Beneficiary.validateParsedTrailer(trailer, header, 1);
      expect(trailerValidationResult.error).to.be.false;
      expect(trailerValidationResult.warning).to.be.true;
      expect(trailerValidationResult.message)
        .to.equal('Record count in the Trailer does not equal the number of Detail records');
    });
  });
});
