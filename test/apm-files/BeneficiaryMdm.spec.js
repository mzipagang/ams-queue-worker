const { expect } = require('chai');
const BeneficiaryMdm = require('../../app/apm-files/BeneficiaryMdm');

describe('Beneficiary Mdm File', () => {
  describe('schemaMap', () => {
    it('should get the schema map', () => {
      const schemaMap = BeneficiaryMdm.schemaMap;
      expect(schemaMap).to.be.defined;
      expect(schemaMap['Program ID']).to.equal('program_id');
      expect(schemaMap['Beneficiary MBI']).to.equal('mbi');
    });
  });

  describe('fromRawFile', () => {
    const rawBeneficiaryMdmParsedData = {
      program_id: '08',
      program_org_id: 'A12345',
      id: '01',
      hicn: '1234567890',
      first_name: 'test-first-name',
      middle_name: '',
      last_name: 'test-last-name',
      dob: '20180101',
      gender: 'M',
      start_date: '20170101',
      end_date: '20181231',
      category_code: '',
      program_org_payment_track: '',
      program_org_payment_track_start_date: '',
      program_org_payment_track_end_date: '',
      voluntary_alignment: '',
      other_id: '',
      other_id_type: '',
      mbi: ''
    };

    it('should should return a BeneficiaryMdm object', () => {
      const beneficiaryMdm = BeneficiaryMdm.fromRawFile(rawBeneficiaryMdmParsedData);
      expect(beneficiaryMdm).to.be.defined;
      expect(beneficiaryMdm.program_id).to.equal(rawBeneficiaryMdmParsedData.program_id);
    });
  });

  describe('validateHeader', () => {
    const sampleHeader = 'H|20180611080651|BENEXT';
    const sampleInvalidHeader = 'H|20180611080651|BENE';

    it('should return true for a valid header', () => {
      const validationResult = BeneficiaryMdm.validateHeader(sampleHeader);
      expect(validationResult).to.be.true;
    });

    it('should return false for in invalid header', () => {
      const validationResult = BeneficiaryMdm.validateHeader(sampleInvalidHeader);
      expect(validationResult).to.be.false;
    });
  });

  describe('validateParsedTrailer', () => {
    const header = {
      Date: '20180101'
    };

    it('should return error value false for a valid trailer', () => {
      const trailer = {
        'Record Identifier': 'T',
        'File Type': 'BENEXT',
        Date: '20180101',
        'Record Count': '1'
      };
      const trailerValidationResult = BeneficiaryMdm.validateParsedTrailer(trailer, header, 1);
      expect(trailerValidationResult.error).to.be.false;
    });

    it('should return error value true for missing `T` record identifier', () => {
      const trailer = {
        'Record Identifier': null,
        'File Type': 'BENEXT',
        Date: '20180101',
        'Record Count': '1'
      };
      const trailerValidationResult = BeneficiaryMdm.validateParsedTrailer(trailer, header, 1);
      expect(trailerValidationResult.error).to.be.true;
      expect(trailerValidationResult.message).to.equal('Invalid trailer in file.');
    });

    it('should return an error for an invalid file type', () => {
      const trailer = {
        'Record Identifier': 'T',
        'File Type': 'BENE',
        Date: '20180101',
        'Record Count': '1'
      };
      const trailerValidationResult = BeneficiaryMdm.validateParsedTrailer(trailer, header, 1);
      expect(trailerValidationResult).to.be.defined;
      expect(trailerValidationResult.error).to.be.defined;
      expect(trailerValidationResult.error).to.be.true;
      expect(trailerValidationResult.message).to.equal('File Type must be "BENEXT".');
    });

    it('should return an error when the date in the header does not match the date in the trailer', () => {
      const trailer = {
        'Record Identifier': 'T',
        'File Type': 'BENEXT',
        Date: '20180102',
        'Record Count': '1'
      };
      const trailerValidationResult = BeneficiaryMdm.validateParsedTrailer(trailer, header, 1);
      expect(trailerValidationResult).to.be.defined;
      expect(trailerValidationResult.error).to.be.defined;
      expect(trailerValidationResult.error).to.be.true;
      expect(trailerValidationResult.message).to.equal('Date in Trailer Record must match Date in Header Record.');
    });

    it('should return warning value true when count in the trailer does not match the number of detail rows', () => {
      const trailer = {
        'Record Identifier': 'T',
        'File Type': 'BENEXT',
        Date: '20180101',
        'Record Count': '1'
      };
      const trailerValidationResult = BeneficiaryMdm.validateParsedTrailer(trailer, header, 2);
      expect(trailerValidationResult.warning).to.be.true;
      expect(trailerValidationResult.message)
        .to.equal('Record count in the Trailer does not equal the number of Detail records');
    });
  });
});
