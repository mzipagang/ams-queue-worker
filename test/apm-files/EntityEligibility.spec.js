const { expect } = require('chai');
const EntityEligibility = require('../../app/apm-files/EntityEligibility');

describe('Entity Eligibility File', () => {
  describe('schemaMap', () => {
    it('should get the schema map', () => {
      const schemaMap = EntityEligibility.schemaMap;
      expect(schemaMap).to.be.defined;
      expect(schemaMap['APM Entity Id']).to.equal('entity_id');
    });
  });

  describe('fromRawFile', () => {
    const rawEntityEligibilityData = {
      entity_id: '01',
      claims_types: '',
      lv_status_code: '',
      lv_status_reason: '',
      lv_part_b_expenditures: '10.555',
      lv_bene_count: '100',
      small_status_apm_entity_code: '',
      small_status_clinician_count: '100',
      performance_year: '2018',
      run_number: '1'
    };

    it('should should return a EntityEligibility object', () => {
      const parsedData = EntityEligibility.fromRawFile(rawEntityEligibilityData);
      expect(parsedData).to.be.defined;
      expect(parsedData.lv_part_b_expenditures).to.equal(10.56);
      expect(parsedData.lv_bene_count).to.equal(100);
    });
  });
});
