const chai = require('chai');
const fs = require('fs');
const path = require('path');

const expect = chai.expect;

describe('APM Files', () => {
  const fileClasses = [
    { fileClass: require('../../app/apm-files/AcoEntity'),
      type: 'aco-entity',
      sampleFile: 'aco_entity.txt',
      detailParserRule: allLines => allLines.map(row => row.trim())
    },
    { fileClass: require('../../app/apm-files/CmmiEntity'), type: 'cmmi-entity', sampleFile: 'cmmi_entity.txt' },
    { fileClass: require('../../app/apm-files/CmmiProvider'), type: 'cmmi-provider', sampleFile: 'cmmi_provider.txt' },
    { fileClass: require('../../app/apm-files/MdmProvider'), type: 'mdm-provider', sampleFile: 'mdm_provider.txt' },
    { fileClass: require('../../app/apm-files/NgacoPreferredProvider'),
      type: 'ngaco-preferred-provider',
      sampleFile: 'ngaco_provider.txt',
      detailParserRule: allLines => allLines.filter(line => !line.includes('#PRVDR_TYPE_CD'))
    },
    { fileClass: require('../../app/apm-files/PacEntity'), type: 'pac-entity', sampleFile: 'pac_entity.txt' },
    {
      fileClass: require('../../app/apm-files/PacEntity'),
      type: 'pac-entity',
      sampleFile: 'pac_entity.txt',
      fileType: 'APME'
    },
    {
      fileClass: require('../../app/apm-files/PacLvt'),
      type: 'pac-lvt',
      sampleFile: 'pac_lvt.txt',
      fileType: 'ALVT'
    },
    {
      fileClass: require('../../app/apm-files/PacLvt'),
      type: 'pac-lvt',
      sampleFile: 'pac_lvt_character_run_snapshot.txt'
    },
    {
      fileClass: require('../../app/apm-files/PacModel'),
      type: 'pac-model',
      sampleFile: 'pac_model.txt',
      fileType: 'APM'
    },
    {
      fileClass: require('../../app/apm-files/PacProvider'),
      type: 'pac-provider',
      sampleFile: 'pac_provider.txt',
      fileType: 'PRVD'
    },
    {
      fileClass: require('../../app/apm-files/PacQpScore'),
      type: 'pac-qp_score',
      sampleFile: 'pac_qp-score.txt',
      fileType: 'QPSC'
    },
    {
      fileClass: require('../../app/apm-files/PacQpStatus'),
      type: 'pac-qp_status',
      sampleFile: 'pac_qp-status.txt',
      fileType: 'QPST'
    },
    {
      fileClass: require('../../app/apm-files/PacQpcScore'),
      type: 'pac-qp_category_score',
      sampleFile: 'pac-qp_category_score.txt',
      fileType: 'QPCS'
    },
    {
      fileClass: require('../../app/apm-files/PacSubdivision'),
      type: 'pac-subdivision',
      sampleFile: 'pac_subdivision.txt',
      fileType: 'SUB'
    },
    {
      fileClass: require('../../app/apm-files/IndividualQpStatus'),
      type: 'individual_qp_status',
      sampleFile: 'individual_qp_status.txt',
      fileType: 'IANT'
    },
    {
      fileClass: require('../../app/apm-files/IndividualQpThreshold'),
      type: 'individual_qp_threshold',
      sampleFile: 'individual_qp_threshold.txt',
      fileType: 'IANC'
    },
    {
      fileClass: require('../../app/apm-files/EntityQpStatus'),
      type: 'entity_qp_status',
      sampleFile: 'entity_qp_status.txt',
      fileType: 'ANST'
    },
    {
      fileClass: require('../../app/apm-files/EntityQpThreshold'),
      type: 'entity_qp_threshold',
      sampleFile: 'entity_qp_threshold.txt',
      fileType: 'ANSC'
    },
    {
      fileClass: require('../../app/apm-files/EntityEligibility'),
      type: 'entity-eligibility',
      sampleFile: 'entity_eligibility.txt',
      fileType: 'ELIG'
    },
    {
      fileClass: require('../../app/apm-files/Beneficiary'),
      type: 'beneficiary',
      sampleFile: 'beneficiary.txt'
    },
    { fileClass: require('../../app/apm-files/BeneficiaryMdm'),
      type: 'beneficiary_mdm',
      sampleFile: 'beneficiary_mdm.txt' }
  ];

  const cmmiEntity = fileClasses.find(file => file.type === 'cmmi-entity');

  describe('CMMI Entity', () => {
    it('should set blank id to null', () => {
      const entity = {
        start_date: '',
        end_date: '',
        provider_start_date: '',
        provider_end_date: '',
        dob: '',
        id: '',
        apm_id: '',
        subdiv_id: '',
        entity_id: ''
      };
      const result = cmmiEntity.fileClass.fromRawFile(entity);

      expect(result.id).to.equal(null);
    });
    it('should set id `0`to `00`', () => {
      const entity = {
        start_date: '',
        end_date: '',
        provider_start_date: '',
        provider_end_date: '',
        dob: '',
        id: '0',
        apm_id: '',
        subdiv_id: '',
        entity_id: ''
      };
      const result = cmmiEntity.fileClass.fromRawFile(entity);

      expect(result.id).to.equal('00');
    });
  });

  fileClasses.forEach((fileClassData) => {
    const type = fileClassData.type;
    const fileType = fileClassData.fileType;

    describe(type, () => {
      const fileClass = fileClassData.fileClass;
      const allLines = fs.readFileSync(path.join(__dirname, `../test_files/mock_files/${fileClassData.sampleFile}`),
      'utf-8').split('\n');
      const header = (allLines.find(row => row[0] === 'H') || allLines[0]).trim();
      const trailer = (allLines.find(row => row[0] === 'T') || '').trim();
      const details = fileClassData.detailParserRule ? fileClassData.detailParserRule(allLines) :
      allLines.filter(row => row[0] === 'D').map(row => row.trim());
      const summaryRecords = allLines.filter(row => row[0] === 'S').map(r => r.trim());

      it('should invalidate an empty header', () => {
        try {
          fileClass.validateHeader('');
        } catch (err) {
          expect(err.message).to.equal('headerLine is required.');
        }
      });

      it('should validate a valid header', () => {
        expect(fileClass.validateHeader(header)).to.equal(true);
      });

      it('should validate a valid trailer', () => {
        if (fileClass.validateParsedTrailer) {
          const parser = fileClass.parser((warning) => {
            throw new Error(warning);
          });
          const jsonHeader = parser({ text: header });
          const jsonTrailer = parser({ text: trailer });
          const validationResult =
          fileClass.validateParsedTrailer(jsonTrailer.data, jsonHeader.data, details.length, summaryRecords.length);
          expect(validationResult.error).to.be.false;
          expect(validationResult.warning).to.be.false;
        }
      });

      if (fileType && fileClass.validateHeader) {
        describe('validateHeader', () => {
          const sampleHeader = `H|20180216|${fileType}|PY2018|1|1`;
          const sampleInvalidHeader = `H|20180216|${fileType}|PY2018|2`;

          it('should return true for a valid header', () => {
            const validationResult = fileClass.validateHeader(sampleHeader);
            expect(validationResult).to.be.true;
          });

          it('should return false for in invalid header', () => {
            const validationResult = fileClass.validateHeader(sampleInvalidHeader);
            expect(validationResult).to.be.false;
          });
        });
      }

      if (fileType && fileClass.validateParsedTrailer) {
        describe('validateParsedTrailer', () => {
          const head = {
            'File Date': '20180101',
            'MIPS Performance Year': 'PY2018',
            'Run Number': '1',
            Snapshot: '1',
            Execution: '1'
          };

          it('should return error value false for a valid trailer', () => {
            const trail = {
              'Record Identifier': 'T',
              'Record Count': '1',
              'Summary Record Count': '1',
              'File Date': '20180101',
              'File Type': fileType,
              'MIPS Performance Year': 'PY2018',
              'Run Number': '1',
              Snapshot: '1',
              Execution: '1'
            };
            const trailerValidationResult = fileClass.validateParsedTrailer(trail, head, 1, 1);
            expect(trailerValidationResult.error).to.be.false;
          });

          it('should return an error for an invalid file type', () => {
            const trail = {
              'Record Identifier': 'T',
              'Record Count': '1',
              'File Date': '20180101',
              'File Type': 'FAKEFILETYPE',
              'MIPS Performance Year': 'PY2018',
              'Run Number': '1',
              Snapshot: '1',
              Execution: '1'
            };
            const trailerValidationResult = fileClass.validateParsedTrailer(trail, head, 1);
            expect(trailerValidationResult).to.be.defined;
            expect(trailerValidationResult.error).to.be.defined;
            expect(trailerValidationResult.error).to.be.true;
            expect(trailerValidationResult.message).to.include('Invalid File Type');
          });

          it('should return an error when the date in the header does not match the date in the trailer', () => {
            const trail = {
              'Record Identifier': 'T',
              'Record Count': '1',
              'File Date': '20180102',
              'File Type': fileType,
              'MIPS Performance Year': 'PY2018',
              'Run Number': '1',
              Snapshot: '1',
              Execution: '1'
            };
            const trailerValidationResult = fileClass.validateParsedTrailer(trail, head, 1);
            expect(trailerValidationResult).to.be.defined;
            expect(trailerValidationResult.error).to.be.defined;
            expect(trailerValidationResult.error).to.be.true;
            expect(trailerValidationResult.message).to.include('Header Date and Trailer Date must match');
          });

          it('should return warning when the trailercount != number of detail rows', () => {
            const trail = {
              'Record Identifier': 'T',
              'Record Count': '3',
              'File Date': '20180101',
              'File Type': fileType,
              'MIPS Performance Year': 'PY2018',
              'Run Number': '1',
              Snapshot: '1',
              Execution: '1'
            };
            const trailerValidationResult = fileClass.validateParsedTrailer(trail, head, 2);
            expect(trailerValidationResult.warning).to.be.true;
          });

          it('should return error value true when the record identifier is null', () => {
            const trail = {
              'Record Identifier': null,
              'Record Count': '1',
              'File Date': '20180101',
              'File Type': fileType,
              'MIPS Performance Year': 'PY2018',
              'Run Number': '1',
              Snapshot: '1',
              Execution: '1'
            };
            const trailerValidationResult = fileClass.validateParsedTrailer(trail, head, 1);
            expect(trailerValidationResult.error).to.be.true;
          });

          it('should return error value when snapshot value is null', () => {
            const trail = {
              'Record Identifier': 'T',
              'Record Count': '1',
              'File Date': '20180101',
              'File Type': fileType,
              'MIPS Performance Year': 'PY2018',
              'Run Number': '1',
              Snapshot: null,
              Execution: '1'
            };
            const trailerValidationResult = fileClass.validateParsedTrailer(trail, head, 1);
            expect(trailerValidationResult.error).to.be.true;
          });

          it('should return error value when snapshot value is a bad pattern', () => {
            const trail = {
              'Record Identifier': 'T',
              'Record Count': '1',
              'File Date': '20180101',
              'File Type': fileType,
              'MIPS Performance Year': 'PY2018',
              'Run Number': '1',
              Snapshot: 'a',
              Execution: '1'
            };
            const trailerValidationResult = fileClass.validateParsedTrailer(trail, head, 1);
            expect(trailerValidationResult.error).to.be.true;
          });

          it('should return error value when snapshot values do not match', () => {
            const trail = {
              'Record Identifier': 'T',
              'Record Count': '1',
              'File Date': '20180101',
              'File Type': fileType,
              'MIPS Performance Year': 'PY2018',
              'Run Number': '1',
              Snapshot: '2',
              Execution: '1'
            };
            const trailerValidationResult = fileClass.validateParsedTrailer(trail, head, 1);
            expect(trailerValidationResult.error).to.be.true;
          });

          it('should return error value when execution value is null', () => {
            const trail = {
              'Record Identifier': 'T',
              'Record Count': '1',
              'File Date': '20180101',
              'File Type': fileType,
              'MIPS Performance Year': 'PY2018',
              'Run Number': '1',
              Snapshot: '1',
              Execution: null
            };
            const trailerValidationResult = fileClass.validateParsedTrailer(trail, head, 1);
            expect(trailerValidationResult.error).to.be.true;
          });

          it('should return error value when execution value is a bad pattern', () => {
            const trail = {
              'Record Identifier': 'T',
              'Record Count': '1',
              'File Date': '20180101',
              'File Type': fileType,
              'MIPS Performance Year': 'PY2018',
              'Run Number': '1',
              Snapshot: '1',
              Execution: 'a'
            };
            const trailerValidationResult = fileClass.validateParsedTrailer(trail, head, 1);
            expect(trailerValidationResult.error).to.be.true;
          });

          it('should return error value when execution values do not match', () => {
            const trail = {
              'Record Identifier': 'T',
              'Record Count': '1',
              'File Date': '20180101',
              'File Type': fileType,
              'MIPS Performance Year': 'PY2018',
              'Run Number': '1',
              Snapshot: '1',
              Execution: '2'
            };
            const trailerValidationResult = fileClass.validateParsedTrailer(trail, head, 1);
            expect(trailerValidationResult.message).to.include('Header Execution and Trailer Execution must match');
            expect(trailerValidationResult.error).to.be.true;
          });
        });
      }

      describe('invalid other files', () => {
        fileClasses.forEach((invalidFileClassData) => {
          if (invalidFileClassData.type === type) {
            return;
          }

          it(`should invalidate ${invalidFileClassData.type} file`, () => {
            const invalidFirstLine = fs.readFileSync(path.join(__dirname,
               `../test_files/mock_files/${invalidFileClassData.sampleFile}`),
              'utf-8').split('\n')[0].trim();
            expect(fileClass.validateHeader(invalidFirstLine)).to.equal(false);
          });
        });
      });

      it('should parse a file and return a json object', () => {
        const parser = fileClass.parser(() => { });
        parser({ text: header });
        const jsonDetailRow = parser({ text: details[0] });
        expect(jsonDetailRow.data).to.be.defined;
      });
    });
  });

  describe('Date Parsing:', () => {
    const types = [
      { type: 'aco-entity', format: 'MM/DD/YYYY', dateFields: ['start_date', 'end_date'] },
      { type: 'ngaco-preferred-provider', format: 'MM/DD/YYYY', dateFields: ['start_date', 'end_date'] },
      { type: 'cmmi-entity', format: 'YYYYMMDD', dateFields: ['start_date', 'end_date'] },
      { type: 'cmmi-provider', format: 'YYYYMMDD', dateFields: ['start_date', 'end_date'] },
      { type: 'mdm-provider', format: 'YYYYMMDD', dateFields: ['start_date', 'end_date'] },
      { type: 'beneficiary', format: 'YYYYMMDD', dateFields: ['start_date', 'end_date'] },
      { type: 'beneficiary_mdm', format: 'YYYYMMDD', dateFields: ['start_date', 'end_date', 'dob'] }
    ];

    types.forEach((dataType) => {
      const type = dataType.type;
      const format = dataType.format;
      const dateFields = dataType.dateFields;
      const fileClass = fileClasses.filter(c => c.type === type)[0].fileClass;

      describe(type, () => {
        it('should return valid dates', () => {
          const startDate = format === 'MM/DD/YYYY' ? '01/01/2000' : '20000101';
          const endDate = format === 'MM/DD/YYYY' ? '01/01/2000' : '20000101';
          const dob = format === 'MM/DD/YYYY' ? '01/01/2000' : '20000101';

          const dates = {
            start_date: startDate,
            end_date: endDate,
            provider_start_date: startDate,
            provider_end_date: endDate,
            dob,
            id: '',
            apm_id: '',
            subdiv_id: '',
            entity_id: ''
          };
          const result = fileClass.fromRawFile(dates);
          dateFields.forEach((field) => {
            expect(result[field]).to.equal('2000-01-01');
          });
        });

        it('should set blank dates to null', () => {
          const startDate = '';
          const endDate = '';
          const dob = '';

          const dates = {
            start_date: startDate,
            end_date: endDate,
            provider_start_date: startDate,
            provider_end_date: endDate,
            dob,
            id: '',
            apm_id: '',
            subdiv_id: '',
            entity_id: ''
          };
          const result = fileClass.fromRawFile(dates);
          dateFields.forEach((field) => {
            expect(!!result[field]).to.equal(false);
          });
        });

        it('should set invalid dates to null', () => {
          const startDate = format === 'MM/DD/YYYY' ? '13/01/2000' : '20001301';
          const endDate = format === 'MM/DD/YYYY' ? '13/31/2000' : '20001331';
          const dob = format === 'MM/DD/YYYY' ? '13/31/2000' : '20001331';
          const dates = {
            start_date: startDate,
            end_date: endDate,
            provider_start_date: startDate,
            provider_end_date: endDate,
            dob,
            id: '',
            apm_id: '',
            subdiv_id: '',
            entity_id: ''
          };
          const result = fileClass.fromRawFile(dates);
          dateFields.forEach((field) => {
            expect(result[field]).to.equal(null);
          });
        });

        it('should handle odd dates such as 01/01/0001', () => {
          const startDate = format === 'MM/DD/YYYY' ? '01/01/0000' : '00000101';
          const endDate = format === 'MM/DD/YYYY' ? '01/01/0000' : '00000101';
          const dob = format === 'MM/DD/YYYY' ? '01/01/0000' : '00000101';
          const dates = {
            start_date: startDate,
            end_date: endDate,
            provider_start_date: startDate,
            provider_end_date: endDate,
            dob,
            id: '',
            apm_id: '',
            subdiv_id: '',
            entity_id: ''
          };
          const result = fileClass.fromRawFile(dates);
          dateFields.forEach((field) => {
            expect(result[field]).to.equal('0000-01-01');
          });
        });

        it('should handle odd dates such as 12/31/9999', () => {
          const startDate = format === 'MM/DD/YYYY' ? '12/31/9999' : '99991231';
          const endDate = format === 'MM/DD/YYYY' ? '12/31/9999' : '99991231';
          const dob = format === 'MM/DD/YYYY' ? '12/31/9999' : '99991231';
          const dates = {
            start_date: startDate,
            end_date: endDate,
            provider_start_date: startDate,
            provider_end_date: endDate,
            dob,
            id: '',
            apm_id: '',
            subdiv_id: '',
            entity_id: ''
          };
          const result = fileClass.fromRawFile(dates);
          dateFields.forEach((field) => {
            expect(result[field]).to.equal('9999-12-31');
          });
        });
      });
    });
  });
});
