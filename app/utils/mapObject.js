const logger = require('../services/logger');

// TODO: should probably just use lodash's _.set method instead
function deepSet(obj, path, value) {
  if (!path) {
    return obj;
  }
  const ref = obj;
  const map = path.split('.');

  if (map.length === 1) {
    ref[map[0]] = value;
    return ref;
  }

  ref[map[0]] = ref[map[0]] || {};
  return deepSet(ref[map[0]], map.slice(1).join('.'), value);
}
/**
 * Maps the key names of obj to desired key names using the given map object parameter
 * @param obj - the object with unmapped keys
 * @param map - the key/value object used to map old key names [key] to new key names [value]
 * @returns an object with mapped keys
 */
module.exports = (obj, map) => {
  if (!map) {
    logger.warn('No mapping for object provided. Returning unmapped object.');
    return obj;
  }

  return Object.keys(obj).reduce((currentObject, currentKey) => {
    const normalizedPropertyName = map[currentKey];
    deepSet(currentObject, normalizedPropertyName, obj[currentKey]);
    return currentObject;
  }, {});
};
