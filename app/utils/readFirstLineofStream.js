const readline = require('readline');

/**
 * Reads the first line of a given Readstream
 * @param readStream
 */
module.exports = readStream => new Promise((resolve) => {
  const rl = readline.createInterface({
    input: readStream
  });

  let headerLine = null;

  rl.on('line', (line) => {
    headerLine = line;
    rl.close();
  }).on('close', () => {
    resolve(headerLine);
  });
});
