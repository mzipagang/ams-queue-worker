const padLeftIfNumber = require('./padLeftIfNumber');
const mapObject = require('./mapObject');
const readFirstLineOfStream = require('./readFirstLineofStream');
const toChunks = require('./toChunks');

module.exports = {
  padLeftIfNumber,
  toChunks,
  transform: mapObject,
  readFirstLineOfStream
};
