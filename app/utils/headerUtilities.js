module.exports = {
  getPerformanceYear: (header) => {
    const headerParams = header.split('|');
    return parseInt(headerParams[3].replace('PY', ''));
  },
  getRunNumber: (header) => {
    const headerParams = header.split('|');
    return headerParams[4].trim();
  },
  getRunSnapshot: (header) => { // run_snapshot: P, S, I, F
    const headerParams = header.split('|');
    return headerParams[4].trim();
  },
  getSnapshot: (header) => { // snapshot: P, 1, 2, 3, 4
    const headerParams = header.split('|');
    return headerParams[4].trim();
  },
  getSnapshotId: (header) => {
    const headerParams = header.split('|');
    return headerParams[5].trim();
  },
  getExecution(header) {
    const headerParams = header.split('|');
    return headerParams[5].trim();
  }
};
