module.exports = (inStr, length) => {
  let str = inStr.toString();
  if (!isNaN(str)) {
    while (str.length < length) {
      str = `0${str}`;
    }
  }
  return str;
};
