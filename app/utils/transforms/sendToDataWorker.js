const through = require('through2');
const rowsProcessor = require('../../services/rowsProcessor');

module.exports = (fileImportGroupId, fileId, file, onLineParseError, fileClass) =>
  through.obj((chunk, enc, callback) =>
    rowsProcessor.onBatch(fileImportGroupId, fileId, file, onLineParseError, chunk, fileClass).then(() => callback())
  );
