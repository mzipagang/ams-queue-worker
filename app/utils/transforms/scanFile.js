const through2 = require('through2');

const trailingPipe = new RegExp('\\|$');

/**
* Takes a row and determines the type: Header, Detail, or Trailer.
* @param {string} text the row of text
*/
const getRowTypeAbbreviation = (text) => {
  const results = /^([HDT])\|/.exec(text);

  if (!results || !results[1]) {
    return '';
  }

  return results[1];
};

const isHeader = text => getRowTypeAbbreviation(text) === 'H';
const isTrailer = text => getRowTypeAbbreviation(text) === 'T';
const isHeaderOrTrailer = text => isHeader(text) || isTrailer(text);

/**
* A TransformStream that checks for header/footer warnings. These include: extra trailing delimiters.
* If one of these warnings is found, a 'warning' event is emitted but the chunk is still passed along
*/
module.exports = (needsCheck = true) => through2.obj(function check(chunk, enc, callback) {
  if (!needsCheck) {
    return callback(null, chunk);
  }

  const text = chunk.text && chunk.text.trim();

  if (isHeaderOrTrailer(text) && trailingPipe.test(text)) {
    const recordTypeString = isHeader(text) ? 'Header Record' : 'Trailer Record';

    this.emit(
      'warning',
      `Too many fields in the ${recordTypeString} (excess pipes "|" at the end of the record)`
    );
  }

  return callback(null, chunk);
});
