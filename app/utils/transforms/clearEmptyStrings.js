const through = require('through2');

module.exports = () => through.obj((chunk, enc, callback) => {
  const data = chunk.data;
  Object.keys(chunk.data)
    .filter(key => chunk.data[key] === '')
    .forEach((emptyStringKey) => { data[emptyStringKey] = null; });

  const row = Object.assign({}, chunk, {
    data: Object.assign({}, chunk.data, data)
  });

  callback(null, row);
});
