const through = require('through2');

module.exports = (validateTrailerFunction, errorCallback) => {
  let headerRowCount = 0;
  let detailRowCount = 0;
  let trailerRowCount = 0;
  let summaryRowCount = 0;
  let trailerRow;
  let headerRow;
  return through.obj(function validateTrailer(chunk, _enc, callback) {
    if (!validateTrailerFunction) {
      return callback(null, chunk);
    }

    const recordIdentifier = chunk.data['Record Identifier'];
    if (recordIdentifier === 'H') {
      headerRow = chunk;
      headerRowCount += 1;
      // after reducing all header information we do not want to send it down the pipe
      // this is because we send through data to the data-worker and we don't want to send the header rows
      // instead we want to only use this data for validation at the end
      return callback();
    }
    if (recordIdentifier === 'D') {
      detailRowCount += 1;
    }

    if (recordIdentifier === 'S') {
      summaryRowCount += 1;
    }

    if (recordIdentifier === 'T') {
      trailerRow = chunk; // this will be overwritten but that's okay because there can only be one trailer row
      trailerRowCount += 1;
      // after reducing all trailer information we do not want to send it down the pipe
      // this is because we send through data to the data-worker and we don't want to send the trailer rows
      // instead we want to only use this data for validation at the end
      return callback();
    }

    this.push(chunk);
    return callback();
  }, function flush(callback) {
    if (!validateTrailerFunction) {
      return callback();
    }
    let message;
    if (headerRowCount > 1 || trailerRowCount > 1 || detailRowCount === 0) {
      if (headerRowCount > 1) {
        message = 'More than 1 header was found in file.';
      } else if (trailerRowCount > 1) {
        message = 'More than 1 trailer was found in file.';
      } else if (detailRowCount === 0) {
        message = 'No detail records found in file.';
      }
    } else if (!trailerRow) {
      this.emit('warning', 'Missing Trailer in file');
    } else {
      const validation = trailerRow &&
        validateTrailerFunction(trailerRow.data, headerRow.data, detailRowCount, summaryRowCount);

      if (validation.error) {
        message = validation.message;
      }
      if (validation.warning) {
        if (trailerRow && validation.message) {
          this.emit('warning', validation.message);
        }
      }
    }

    if (message) {
      return errorCallback(message).then(callback);
    }
    return callback();
  });
};
