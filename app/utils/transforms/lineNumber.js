const through = require('through2');

/**
 * Adds a line to the streaming row data as `lineNumber` starting at 1
 */
module.exports = () => {
  let lineNumber = 1;

  return through.obj(function countLine(chunk, enc, callback) {
    this.push({
      text: chunk,
      lineNumber
    });

    lineNumber += 1;

    callback();
  });
};
