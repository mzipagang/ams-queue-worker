const through = require('through2');

module.exports = meta => through.obj((chunk, enc, callback) => {
  const row = Object.assign({}, chunk, {
    data: Object.assign({}, chunk.data, {
      'Performance Year': meta.performance_year,
      'Run Snapshot': meta.run_snapshot,
      'Run Number': meta.run_number,
      'Snapshot ID': meta.snapshot_id,
      Execution: meta.execution,
      Snapshot: meta.snapshot,
      Run: meta.run
    })
  });

  callback(null, row);
});
