const through = require('through2');

class ParserUtility {
  static getStreamHandler() {
    return through.obj;
  }

  static getStreamChunkHandler(parseFunction, errorFunction) {
    return (chunk, _enc, callback) => {
      try {
        const values = parseFunction(chunk);
        return callback(null, values);
      } catch (error) {
        return Promise.resolve(errorFunction(chunk.lineNumber, chunk.text, error)).then(() => {
          callback();
        });
      }
    };
  }

  static getStreamParser(parseFunction, errorFunction) {
    const streamHandler = this.getStreamHandler();
    return streamHandler(this.getStreamChunkHandler(parseFunction, errorFunction));
  }
}

module.exports = (parseFunction, errorFunction) => ParserUtility.getStreamParser(parseFunction, errorFunction);
module.exports.ParserUtility = ParserUtility;
