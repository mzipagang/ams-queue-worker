const through = require('through2');
const Promise = require('bluebird');

module.exports = Promise.coroutine(function* filter(filterFunctions) {
  const filters = filterFunctions && (yield Promise.all(filterFunctions.map(filterfunction => filterfunction())));

  return through.obj(function filterRow(chunk, enc, callback) {
    if (!Array.isArray(filters)) {
      return callback(null, chunk);
    }

    return Promise.all(filters.map(filterFn => filterFn(chunk))).then((values) => {
      if (values.every(v => v === true)) {
        this.push(chunk);
      }
      callback();
    });
  });
});
