const dataWorker = require('../../services/dataWorker');
const querystring = require('querystring');

module.exports = async (header, instance) => {
  if (!instance.getPerformanceYear || !instance.getExecution || !(instance.getSnapshot || instance.getRunNumber)) {
    return true;
  }
  try {
    const qs = querystring.stringify({
      performance_year: instance.getPerformanceYear(header),
      snapshot: (instance.getSnapshot || instance.getRunNumber)(header),
      import_file_type: instance.fileType
    });

    const resp = await dataWorker.get(`/api/v1/publish_history/execution_number?${qs}`);
    const executionNumber = parseInt(instance.getExecution(header));
    return (executionNumber > resp.body.execution);
  } catch (err) {
    return Promise.reject(err);
  }
};
