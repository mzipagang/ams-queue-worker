/* caveat
Normally you would provide the same names as the usual methods being used but in this case
the method names existing kick off logic in the API validation so these method names
had "Utility" appended to ensure that auto-magic behavior doesn't kick in.
*/
class FileValidator {
  static validateHeaderUtility({
    headerLine,
    fileType,
    fileDate = FileValidator.commonPatterns().date,
    mipsPattern = FileValidator.commonPatterns().mips,
    snapshotPattern = FileValidator.commonPatterns().snapshot,
    executionPattern = FileValidator.commonPatterns().execution,
    appendPattern
  }) {
    if (!headerLine) {
      throw new Error('headerLine is required.');
    }
    if (!fileType) {
      throw new Error('fileType is required.');
    }
    // pipe delimited i.e. "H|12345678|QPST|PY2017|P|9"
    const regexParts = [
      'H',              // (0) record identifier must be 'H'
      fileDate,         // (1) file date i.e "12345678"
      fileType,         // (2) file type i.e "QPST"
      mipsPattern,      // (3) MIPS performance year i.e. "PY2017"

      // used interchangeably with run since parsing is the same
      snapshotPattern,  // (4) Snapshot must be character 'P' or digit 1-4

      executionPattern  // (5) Execution must be digit 1-9
    ];
    // eslint-disable-next-line quotes
    const reStr = `^${regexParts.join(`\\|`)}${appendPattern || ''}$`;
    return new RegExp(reStr).test(headerLine && headerLine.trim());
  }

  static commonPatterns() {
    return {
      date: '[0-9]{8}',
      mips: 'PY20(1[7-9]|[2-9][0-9])',
      snapshot: '(P|[1-4])',
      execution: '[1-9]'
    };
  }

  static validateParsedTrailerUtility({
    trailer,
    header,
    recordsCount,
    summaryRecordCount,
    fileType,
    validateRecordIdentifierPattern = true,
    validateRecordCount = true,
    validateSummaryCount = false,
    validateTrailerDateEquality = true,
    validatePerformanceYearEquality = true,

    // used interchangeablye with run as its parsed exactly the same
    snapshotPattern = FileValidator.commonPatterns().snapshot,
    snapshotPatternErrorMessage = 'Snapshot must be character P or a digit between 1-4',
    snapshotMatchErrorMessage = 'Header Snapshot and Trailer Snapshot must match',

    validateSnapshotPattern = true,
    validateSnapshotEquality = true,
    executionPattern = FileValidator.commonPatterns().execution,
    executionPatternErrorMessage = 'Execution must be a digit between 1-9',
    validateExecutionPattern = true,
    validateExecutionEquality = true
  }) {
    if (!header) {
      throw new Error('header is required.');
    }
    if (!trailer) {
      throw new Error('trailer is required.');
    }
    if (!fileType) {
      throw new Error('fileType is required.');
    }
    if (!recordsCount) {
      throw new Error('recordsCount is required.');
    }

    const messages = [];
    let warning = false;
    let error = false;

    if (validateRecordCount && parseInt(trailer['Record Count'] || trailer['Detail Record Count']) !== recordsCount) {
      warning = true;
      messages.push('Record count in the Trailer does not equal the number of Detail records');
    }

    if (validateSummaryCount && summaryRecordCount === 0) {
      error = true;
      messages.push('File must contain at least one Summary Record');
    } else if (validateSummaryCount && parseInt(trailer['Summary Record Count']) !== summaryRecordCount) {
      warning = true;
      messages.push('Summary Record count in the Trailer does not equal the number of Summary records');
    }

    if (trailer['File Type'] !== fileType) {
      error = true;
      messages.push('Invalid File Type');
    }

    if (validateRecordIdentifierPattern && trailer['Record Identifier'] !== 'T') {
      error = true;
      // eslint-disable-next-line quotes
      messages.push(`Record Identifier must be a 'T' character`);
    }

    if (validateTrailerDateEquality && trailer['File Date'] !== header['File Date']) {
      error = true;
      messages.push('Header Date and Trailer Date must match');
    }

    if (validatePerformanceYearEquality && trailer['MIPS Performance Year'] !== header['MIPS Performance Year']) {
      error = true;
      messages.push('Header MIPS Performance Year and Trailer MIPS Performance Year must match');
    }

    // !! used interchangeably with snapshot since it is parsed exactly the same in the same pipe delimeter placement
    if (validateSnapshotPattern && !(new RegExp(`^${snapshotPattern}$`)).test(trailer.Snapshot || trailer.Run)) {
      error = true;
      messages.push(snapshotPatternErrorMessage);
    }

    if (validateSnapshotEquality && trailer.Snapshot !== header.Snapshot) {
      error = true;
      messages.push(snapshotMatchErrorMessage);
    }

    if (validateExecutionPattern && !(new RegExp(`^${executionPattern}$`)).test(trailer.Execution)) {
      error = true;
      messages.push(executionPatternErrorMessage);
    }

    if (validateExecutionEquality && trailer.Execution !== header.Execution) {
      error = true;
      messages.push('Header Execution and Trailer Execution must match');
    }

    return {
      error,
      warning,
      message: `${messages.join('. ')}.`
    };
  }
}

module.exports = FileValidator;
