const AWS = require('aws-sdk');
const config = require('../../config');
const Promise = require('bluebird');

AWS.config.setPromisesDependency(Promise);

AWS.config.update({
  region: 'us-east-1',
  accessKeyId: config.AWS.ACCESS_KEY_ID,
  secretAccessKey: config.AWS.SECRET_ACCESS_KEY,
  s3ForcePathStyle: true,
  apiVersions: {
    sqs: '2012-11-05'
  }
});

const S3 = new AWS.S3({
  endpoint: config.AWS.LOCAL_AWS ? new AWS.Endpoint(config.DEV.S3) : undefined
});

const SQS = new AWS.SQS({
  endpoint: config.AWS.LOCAL_AWS ? new AWS.Endpoint(config.DEV.SQS) : undefined
});

/* it doesn't really make sense to test these as they are merely passing in properties to
   a 3rd party library; the test would be just as vulnerable to failing as the code
*/
/* istanbul ignore next */
const getS3FileFromBucket = (bucket, key) => S3.getObject({
  Bucket: bucket,
  Key: key
});


const deleteObject = Promise.promisify(S3.deleteObject.bind(S3));

/* it doesn't really make sense to test these as they are merely passing in properties to
   a 3rd party library; the test would be just as vulnerable to failing as the code
*/
/* istanbul ignore next */
const deleteS3FileInBucket = (bucket, key) => deleteObject({
  Bucket: bucket,
  Key: key
});

module.exports = {
  S3,
  SQS,
  getS3FileFromBucket,
  deleteS3FileInBucket
};
