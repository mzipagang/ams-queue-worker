const dataWorker = require('../dataWorker');

module.exports = {
  getAMSModels: () => dataWorker('get', '/api/v1/pac/model')
      .then(response => response.body.pac_models)
};
