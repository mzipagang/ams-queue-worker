const request = require('request-promise');
const jwt = require('jsonwebtoken');
const Promise = require('bluebird');
const logger = require('../logger');

const { transform } = require('../../utils');

const config = require('../../config');

const SYSTEM_TOKEN = jwt.sign({ type: 'system' }, config.JWT.SECRET);

const onBatch = (fileImportGroupId, fileId, file, errorCallback, rows, fileClass) => {
  const { fromRawFile, schemaMap } = fileClass;

  let values = [];
  if (typeof schemaMap === 'function') {
    values = rows.map((row) => {
      const map = schemaMap(row.data);
      return { meta: { file_line_number: row.lineNumber }, value: fromRawFile(transform(row.data, map)) };
    });
  } else {
    values = rows.map(row => ({
      meta: {
        file_line_number: row.lineNumber
      },
      value: fromRawFile(transform(row.data, schemaMap))
    }));
  }

  const requestOptions = {
    method: 'POST',
    uri: `${config.API_FILE_IMPORT}/${fileImportGroupId}/file/${fileId}` +
      `/${fileClass.urlForTypeMappings}/batch`,
    body: {
      data: {
        meta: {
          file_name: file.file_name,
          file_location: file.file_location
        },
        values
      }
    },
    json: true,
    simple: false,
    resolveWithFullResponse: true,
    headers: {
      Authorization: SYSTEM_TOKEN
    }
  };

  return Promise.try(() => request(requestOptions))
    .then((response) => {
      if (requestOptions.body.data.values.length > config.LOG_IF_MORE_THEN_LINES) {
        const lastLine = (requestOptions.body.data.values[requestOptions.body.data.values.length - 1])
        .meta.file_line_number;

        logger.info(
`file: ${requestOptions.body.data.meta.file_location}
rows: ${requestOptions.body.data.values.length}
lastLine: ${lastLine}
size: ${JSON.stringify(requestOptions).length / 1024 / 1024} mb
StatusCode: ${response.statusCode}`
        );
      }
      if (response.statusCode !== 200) {
        return errorCallback(0, null, new Error(response.statusMessage));
      }

      const responseData = response.body.data;
      return Promise.all(responseData.values.map((result, index) => {
        if (result.success) {
          return null;
        }

        const failedData = values[index];
        return errorCallback(failedData.meta.file_line_number, null,
          new Error(result.message || 'There was an error while processing this line.'));
      }));
    });
};

const onError = (fileImportGroupId, fileId, lineNumber, text, error) => Promise.try(() => request({
  method: 'POST',
  uri: `${config.API_FILE_IMPORT}/${fileImportGroupId}/error`,
  body: {
    file_import_group_id: fileImportGroupId,
    file_id: fileId,
    error: error.message,
    line_number: lineNumber
  },
  json: true,
  simple: false,
  resolveWithFullResponse: true,
  headers: {
    Authorization: SYSTEM_TOKEN
  }
}));

module.exports = {
  onError,
  onBatch
};
