const jobs = [
  require('../../jobs/publish_history_build_cache_batch'),
  require('../../jobs/publish_history_build_cache_queue'),
  require('../../jobs/publish_history_merge_file_queue'),
  require('../../jobs/publish_history_merge_file_batch'),
  require('../../jobs/publish_history_merge_finished'),
  require('../../jobs/generate_qp_metrics_report'),
  require('../../jobs/generate_ec_count_report')
];

module.exports = jobs.reduce((current, job) => {
  current[job.jobName] = job;
  return current;
}, {});
