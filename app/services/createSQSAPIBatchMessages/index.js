const Promise = require('bluebird');
const uuid = require('uuid');
const config = require('../../config');
const AWS = require('../aws');

const SQS = AWS.SQS;

module.exports = (priority, requests) => {
  if ([1, 2].indexOf(priority) === -1) {
    throw new Error('Priority can only be values 1 to 2.');
  }

  const entries = requests.map((request) => {
    const { jobName, body } = request;

    const Body = {
      jobName,
      body
    };

    return {
      Id: uuid.v4(),
      DelaySeconds: 0,
      MessageBody: JSON.stringify(Body)
    };
  });

  const sendMessageBatch = Promise.promisify(SQS.sendMessageBatch.bind(SQS));

  const params = {
    Entries: entries,
    QueueUrl: `${config.AWS.SQS_REQUEST_QUEUE_URL}-${priority}`
  };

  return sendMessageBatch(params);
};
