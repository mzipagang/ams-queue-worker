const Promise = require('bluebird');
const SQS = require('../aws').SQS;
const logger = require('../logger');
const config = require('../../config');
const Consumer = require('sqs-consumer');
const processFile = require('../../process-file');
const jobs = require('../jobs');

const priorityJobConsumer = priority =>
  Consumer.create({
    queueUrl: `${config.AWS.SQS_REQUEST_QUEUE_URL}-${priority}`,
    waitTimeSeconds: 5,
    visibilityTimeout: config.AWS.SQS_VISIBILITY_TIME,
    terminateVisibilityTimeout: true,
    batchSize: config.AWS.SQS_REQUEST_BATCH_SIZE,
    handleMessage: (message, finish) => {
      const { jobName, body } = JSON.parse(message.Body || '{}');
      /* istanbul ignore next */
      const changeMessageVisibilityInterval = config.NODE_ENV !== 'production' ? 0 : setInterval(() => {
        SQS.changeMessageVisibility({
          QueueUrl: config.AWS.SQS_QUEUE_URL,
          ReceiptHandle: message.ReceiptHandle,
          VisibilityTimeout: config.AWS.SQS_VISIBILITY_TIME
        }, (err) => {
          /* istanbul ignore next */
          if (err) {
            logger.error('Failed to change message visibility', err);
          }
        });
      }, 1000 * (config.AWS.SQS_VISIBILITY_TIME / 2));

      const done = (err) => {
        if (err) {
          logger.error(err);
        }
        finish(err);
        clearInterval(changeMessageVisibilityInterval);
      };

      logger.info(`Received ${jobName}`);

      const job = jobs[jobName];

      if (!job) {
        done(new Error(`Unknown job ${jobName}`));
        return;
      }

      new Promise((resolve, reject) => {
        job.handler(body, (err) => {
          if (err) {
            reject(err);
          } else {
            resolve();
          }
        });
      })
          .then(() => done())
          .catch((err) => {
            logger.error(err);
            done(err);
          });
    },
    sqs: SQS
  });

const priorityJobConsumers = [
  priorityJobConsumer(1),
  priorityJobConsumer(2)
];

const fileProcessingConsumer = Consumer.create({
  queueUrl: config.AWS.SQS_QUEUE_URL,
  waitTimeSeconds: 1,
  visibilityTimeout: config.AWS.SQS_VISIBILITY_TIME,
  terminateVisibilityTimeout: true,
  handleMessage: (message, finish) => {
    /* istanbul ignore next */
    const changeMessageVisibilityInterval = config.NODE_ENV === 'production' ? setInterval(() => {
      SQS.changeMessageVisibility({
        QueueUrl: config.AWS.SQS_QUEUE_URL,
        ReceiptHandle: message.ReceiptHandle,
        VisibilityTimeout: config.AWS.SQS_VISIBILITY_TIME
      }, (err) => {
        /* istanbul ignore next */
        if (err) {
          logger.error('Failed to change message visibility', err);
        }
      });
    }, 1000 * (config.AWS.SQS_VISIBILITY_TIME / 2)) : 0;

    const done = (err) => {
      finish(err);
      clearInterval(changeMessageVisibilityInterval);
    };

    const scanResult = JSON.parse(JSON.parse(message.Body || '{}').Message || '{}').scanResult;

    if (!scanResult) {
      logger.warn('Unknown message type', JSON.stringify(message));
      done(new Error('Unknown message type'));
      return;
    }

    new Promise((resolve, reject) => {
      processFile(scanResult, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    })
      .then(() => done())
      .catch(err => done(err));
  },
  sqs: SQS
});

module.exports = {
  start: () => Promise.try(() => {
    fileProcessingConsumer.start();
    priorityJobConsumers.forEach(jobConsumer => jobConsumer.start());
  }),
  stop: () => new Promise((resolve, reject) => {
    /* istanbul ignore next */
    const timeout = setTimeout(() => reject(new Error('Queue not stop within 30 seconds')), 30 * 1000);

    let stopCount = 0;
    const onStop = () => {
      stopCount += 1;

      if (stopCount === priorityJobConsumers.length + 1) {
        clearTimeout(timeout);
        resolve();
      }
    };

    fileProcessingConsumer.on('stopped', onStop);
    fileProcessingConsumer.stop();

    priorityJobConsumers.forEach((jobConsumer) => {
      jobConsumer.on('stopped', onStop);
      jobConsumer.stop();
    });
  })
};
