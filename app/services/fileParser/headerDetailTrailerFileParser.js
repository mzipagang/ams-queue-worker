const firstCharacter = require('../../utils/firstCharacter');
const characterDelimitedFileParser = require('../fileParser/characterDelimitedFileParser');

/**
* One format of file includes rows that could be considered either a header, detail, or trailer. This parser
* is able to treat each one by using the supplied headers for each type of row. Note: "headers" are more
* or less column labels while "header" is usually the first row in the file.
* @param {string[]} headerHeaders the headers for the header rows of the file
* @param {string[]} detailHeaders the headers for the detail rows of the file
* @param {string[]} trailerHeaders the headers for the trailer rows of the file
* @param {string[]} character a string that will be used to delimit the file; default is `|`
*/
module.exports =
(headerHeaders, detailHeaders, trailerHeaders, warningCallback, summaryHeaders = [], character = '|') => {
  const headerFileParser = characterDelimitedFileParser(character, headerHeaders).parser();
  const detailFileParser = characterDelimitedFileParser(character, detailHeaders).parser();
  const trailersFileParser = characterDelimitedFileParser(character, trailerHeaders).parser();
  const summaryRecordParser = characterDelimitedFileParser(character, summaryHeaders).parser();

  return (line) => {
    const rowType = firstCharacter(line.text);

    if (rowType === 'H') {
      return headerFileParser(line, warningCallback);
    }

    if (rowType === 'D') {
      return detailFileParser(line, warningCallback);
    }

    if (rowType === 'T') {
      return trailersFileParser(line, warningCallback);
    }

    if (rowType === 'S') {
      return summaryRecordParser(line, warningCallback);
    }

    throw new Error('Could not parse row');
  };
};
