const rowParser = require('../rowParser');
const isUndefined = require('lodash.isundefined');

module.exports = (char, definedHeaders) => ({
  /**
   * A higher-order-function that returns a function for parsing a line of text
   * If the line cannot be parsed then an Error is thrown with the given line number and the line of text
   * that threw the Error. If no headers are passed to the parser factory function, the first time it is ran it will store the
   * row as headers and subsequent calls will rely on those stored values for parsing.
   */
  parser: () => {
    let headers = definedHeaders;

    return (line, warningCallback) => {
      if (line.text.trim().length === 0) {
        return null;
      }

      if (!headers) {
        headers = rowParser(line.text, char);
        return null;
      }

      const row = {
        data: {},
        lineNumber: line.lineNumber
      };

      const rowData = rowParser(line.text, char);

      if (!rowData) {
        const parseError = new Error('Failed to parse row');
        parseError.lineNumber = line.lineNumber;
        parseError.text = line.text;
        throw parseError;
      }

      headers.forEach((header, index) => {
        const value = rowData[index];
        row.data[header] = isUndefined(value) ? null : value;
      });

      const splitCheck = line.text.split('|');

      if (warningCallback && (rowData.length < headers.length && splitCheck.length < headers.length)) {
        warningCallback(
          // eslint-disable-next-line max-len
          'Missing fields at the end of Detail records (if no data exists, set values to null and generate additional pipes "|")',
          row,
          rowData
        );
      }

      if (warningCallback && rowData.length > headers.length) {
        warningCallback(
          'Too many fields in the Detail Record (excess pipes "|" at the end of the record)',
          row,
          rowData
        );
      }

      return row;
    };
  }
});
