const characterDelimitedFileParser = require('./characterDelimitedFileParser');

function firstCharacter(line) {
  return line.text.trim()[0];
}

module.exports = (character, headers) => {
  const pipeDelimitedFileParser = characterDelimitedFileParser(character, headers);

  return {
    parser: warningCallback => (line) => {
      const parser = pipeDelimitedFileParser.parser();
      return (firstCharacter(line) !== 'H' && firstCharacter(line) !== 'T') ? parser(line, warningCallback) : null;
    }
  };
};
