const Promise = require('bluebird');
const request = require('request-promise');
const noderequest = require('request');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const config = require('../../config');
const awsService = require('../aws');

const SYSTEM_TOKEN = jwt.sign({ type: 'system' }, config.JWT.SECRET);

const updateFile = (fileImportGroupId, fileId, data) => Promise.try(() => request({
  method: 'PATCH',
  body: data,
  uri: `${config.API_FILE_IMPORT}/${fileImportGroupId}/file/${fileId}`,
  json: true,
  headers: {
    Authorization: SYSTEM_TOKEN
  }
})
.then(response => response.data && response.data.file_import_group)
.then(fileImport => fileImport && fileImport.files && fileImport.files.find(file => file.id === fileId)));

const streamFromS3ToDataWorker = (bucket, key) => Promise.try(() => {
  const s3FileStream = awsService.getS3FileFromBucket(bucket, key).createReadStream();
  s3FileStream.path = key;

  return request({
    method: 'post',
    url: `${config.DATA_WORKER.ROOT_URL}/api/v1/file_transfer/${key}`,
    formData: {
      file: s3FileStream
    },
    headers: {
      Authorization: SYSTEM_TOKEN
    }
  });
});

/*
  It is important that we use the original Request library (https://github.com/request/request)
  here. From the request-promise library:

  STREAMING THE RESPONSE (e.g. .pipe(...)) is DISCOURAGED because Request-Promise
  would grow the memory footprint for large requests unnecessarily high. Use the original
  Request library for that. You can use both libraries in the same project.
 */
const downloadFileFromDataWorker = (key, toFilePath) => Promise.try(() => noderequest({
  method: 'GET',
  uri: `${config.DATA_WORKER.ROOT_URL}/api/v1/file_transfer/${key}`,
  json: true,
  headers: {
    Authorization: SYSTEM_TOKEN
  }
})).then(res => new Promise((resolve, reject) => {
  const piped = res.pipe(fs.createWriteStream(toFilePath));
  piped.on('error', (err) => {
    reject(err);
  });

  piped.on('finish', () => {
    resolve();
  });
}));

const getFileImportGroup = id => Promise.try(() => request({
  method: 'GET',
  uri: `${config.API_FILE_IMPORT}/${id}`,
  json: true,
  headers: {
    Authorization: SYSTEM_TOKEN
  }
}).then(response => response.data && response.data.file_import_group));

const sendRemoveDuplicateRequest = Promise.coroutine(function* sendDuplicates(fileImportGroupId, fileId, providers) {
  yield request({
    method: 'POST',
    uri: `${config.API_FILE_IMPORT}/${fileImportGroupId}/file/${fileId}/duplicates`,
    body: {
      duplicates_providers: providers
    },
    json: true,
    simple: false,
    resolveWithFullResponse: true,
    headers: {
      Authorization: SYSTEM_TOKEN
    }
  });
});

const removeDuplicatesFromPacProvider = Promise.coroutine(function* removeDuplicates(fileImportGroupId, fileId) {
  const response = yield request({
    method: 'GET',
    uri: `${config.API_FILE_IMPORT}/${fileImportGroupId}/file/${fileId}/duplicates`,
    json: true,
    headers: {
      Authorization: SYSTEM_TOKEN
    }
  });

  const batch = [];

  for (let i = 0; i < response.data.duplicates.length; i += 1) {
    const d = response.data.duplicates[i];
    const tin = d.tin;

    for (let j = 0; j < d.entities.length; j += 1) {
      const e = d.entities[j];
      const entityId = e.entity_id;

      for (let k = 0; k < e.npis.length; k += 1) {
        const npi = e.npis[k];
        batch.push({ tin, entity_id: entityId, npi });

        if (batch.length >= 5000) {
          yield sendRemoveDuplicateRequest(fileImportGroupId, fileId, batch);
          batch.length = 0;
        }
      }
    }
  }

  if (batch.length > 0) {
    yield sendRemoveDuplicateRequest(fileImportGroupId, fileId, batch);
    batch.length = 0;
  }
});

module.exports = {
  updateFile,
  transferFileToDataWorker: streamFromS3ToDataWorker,
  downloadFileFromDataWorker,
  getFileImportGroup,
  removeDuplicatesFromPacProvider
};
