// create a function that takes a parser delimiter and a string
// parses through string using delimiter rules and then returns array of parsed vals

// TODO: break regex line into multiple, readable lines
const rules = {
  ',': {
    parse: /(?!\s*$)\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^,'"\s\\]*(?:\s+[^,'"\s\\]+)*))\s*(?:,|$)/g, // eslint-disable-line max-len
    validation: /^\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^,'"\s\\]*(?:\s+[^,'"\s\\]+)*))\s*(?:,\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*)*$/ // eslint-disable-line max-len
  },
  '|': {
    parse: /(?!\s*$)\s*(?:'([^|\\]*(?:\\[\S\s][^|\\]*)*)'|"([^|\\]*(?:\\[\S\s][^|\\]*)*)"|([^|\s\\]*(?:\s+[^|\s\\]+)*))\s*(?:\||$)/g, // eslint-disable-line max-len
    validation: /^\s*(?:'([^|\\]*(?:\\[\S\s][^|\\]*)*)'|"([^|\\]*(?:\\[\S\s][^|\\]*)*)"|([^|\s\\]*(?:\s+[^|"\s\\]+)*))\s*(?:\|\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^|"\s\\]*(?:\s+[^|\s\\]+)*)\s*)*$/ // eslint-disable-line max-len
  },
  '\uFFFD': {
    parse: /(?!\s*$)\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^\uFFFD'"\s\\]*(?:\s+[^\uFFFD'"\s\\]+)*))\s*(?:\uFFFD|$)/g, // eslint-disable-line max-len
    validation: /^\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^\uFFFD'"\s\\]*(?:\s+[^\uFFFD'"\s\\]+)*))\s*(?:\uFFFD\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^\uFFFD'"\s\\]*(?:\s+[^\uFFFD'"\s\\]+)*)\s*)*$/ // eslint-disable-line max-len
  }
};

const parse = (text, delimiter) => {
  const validationRules = rules[delimiter].validation;
  const parseRules = rules[delimiter].parse;
  if (!validationRules.test(text)) {
    return null;  // TODO: maybe this should throw an Error instead; the calling function can decide how to handle it
  }

  const a = [];
  text.replace(parseRules,
      (m0, m1, m2, m3) => {
        /* istanbul ignore else */
        if (m1 !== undefined) {
          a.push(m1.replace(/\\'/g, '\''));
        } else if (m2 !== undefined) {
          a.push(m2.replace(/\\"/g, '"'));
        } else if (m3 !== undefined) {
          a.push(m3);
        }
        return '';
      });

  return a;
};

module.exports = parse;
