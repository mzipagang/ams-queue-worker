const jwt = require('jsonwebtoken');
const config = require('../../config');
const Promise = require('bluebird');
const request = require('request-promise');
const logger = require('../logger');

const SYSTEM_TOKEN = jwt.sign({ type: 'system' }, config.JWT.SECRET);

const dataWorkerRequest = (method, path, body, headers) =>
  new Promise((resolve, reject) => {
    request({
      method,
      uri: `${config.DATA_WORKER.ROOT_URL}${path}`,
      body,
      json: true,
      simple: false,
      resolveWithFullResponse: true,
      headers: Object.assign({}, headers, { Authorization: SYSTEM_TOKEN }),
      forever: true
    }).then((response) => {
      if (/^5/.test(`${response.statusCode}`)) {
        // Status Codes of 5xx
        logger.error('dataWorkerRequest got a 5XX error', JSON.stringify(response));
        return reject(response);
      }

      if (!/^2/.test(`${response.statusCode}`)) {
        // Status Codes other than 2xx
        logger.warn('dataWorkerRequest got a non 2xx response code', JSON.stringify(response));
      }
      return resolve(response);
    })
    .catch((err) => {
      logger.error('Unhandled error in dataWorkerRequest', err);
      return reject(err);
    });
  });

module.exports = (method, url, body, headers) =>
  dataWorkerRequest(method, url, body, headers);
module.exports.get = dataWorkerRequest.bind(null, 'get');
module.exports.post = dataWorkerRequest.bind(null, 'post');
module.exports.put = dataWorkerRequest.bind(null, 'put');
module.exports.patch = dataWorkerRequest.bind(null, 'patch');
module.exports.delete = dataWorkerRequest.bind(null, 'delete');
module.exports.options = dataWorkerRequest.bind(null, 'options');
module.exports.head = dataWorkerRequest.bind(null, 'head');
