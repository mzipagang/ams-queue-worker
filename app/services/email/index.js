const Promise = require('bluebird');
const jwt = require('jsonwebtoken');
const request = require('request-promise');
const config = require('../../config');

const SYSTEM_TOKEN = jwt.sign({ type: 'system' }, config.JWT.SECRET);

const sendStatus = (id, fileName, status) => Promise.try(() => request({
  method: 'POST',
  uri: config.API_STATUS_EMAIL,
  body: {
    id,
    fileName,
    status
  },
  json: true,
  headers: {
    Authorization: SYSTEM_TOKEN
  }
}));

module.exports = {
  sendStatus
};
