const dataWorkerUtils = require('../services/dataWorkerUtils');
const Promise = require('bluebird');

module.exports = Promise.coroutine(function* programIdHasMatchingApmId() {
  const apmIds = (yield dataWorkerUtils.getAMSModels()).map(model => model.id);

  return row => apmIds.indexOf(row.data['Program ID']) !== -1;
});
