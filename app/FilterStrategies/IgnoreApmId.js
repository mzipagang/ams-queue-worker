const ignoredApmIds = ['08'];

module.exports = () => row => ignoredApmIds.indexOf(row.data['APM ID']) === -1;
