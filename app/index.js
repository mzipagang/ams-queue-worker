require('dotenv').config({ path: require('path').join(__dirname, '../.env') });

const moment = require('moment');
const queue = require('./services/queue');
const logger = require('./services/logger');

module.exports = {
  queue: queue.start()
      .then(() => {
        logger.info(`Queue started at ${moment().format()}`);
      })
      .catch((err) => {
        logger.error('Queue failed to start:', err.message);
        logger.error(err);
      })
};

const handle = () => {
  logger.info('Request to exit');

  queue.stop()
      .then(() => {
        process.exit(0);
      })
      .catch((err) => {
        logger.error(err);
        process.exit(1);
      });
};

process.on('SIGINT', handle);
// process.on('SIGTERM', handle);
