/**
 * Configuration File - Use this config rather than directly using process.env.
 *
 * NEVER put environment specific configuration in here.
 * Always require environment specific configurations to be passed in
 * as environment variables.
 */

const LOCAL_AWS = (process.env.NODE_ENV === 'production') ?
    !(!process.env.LOCAL_AWS) : !(process.env.AWS_ACCESS_KEY_ID || process.env.AWS_SECRET_ACCESS_KEY);
const API_URL = process.env.API_URL || 'http://localhost:5000';
const DATA_WORKER_ROOT_URL = process.env.DATA_WORKER_ROOT_URL || 'http://localhost:7000';

const BUILD_CACHE_ENABLED = process.env.BUILD_CACHE_ENABLED ? (process.env.BUILD_CACHE_ENABLED === 'true') : true;

const config = {
  NODE_ENV: process.env.NODE_ENV || 'dev',
  AWS: {
    LOCAL_AWS,
    ACCESS_KEY_ID: process.env.AWS_ACCESS_KEY_ID || (LOCAL_AWS ? 'id' : ''),
    SECRET_ACCESS_KEY: process.env.AWS_SECRET_ACCESS_KEY || (LOCAL_AWS ? 'key' : ''),
    SQS_QUEUE_URL: process.env.SQS_QUEUE_URL || (LOCAL_AWS ? 'http://0.0.0.0:4100/ams-uploads-queue' : ''),
    SQS_REQUEST_QUEUE_URL:
      process.env.SQS_REQUEST_QUEUE_URL || (LOCAL_AWS ? 'http://0.0.0.0:4100/ams-request-queue' : ''),
    SQS_VISIBILITY_TIME: parseInt(process.env.SQS_VISIBILITY_TIME) || 300,
    SQS_REQUEST_BATCH_SIZE: parseInt(process.env.SQS_REQUEST_BATCH_SIZE) || 2
  },
  API_FILE_IMPORT: `${DATA_WORKER_ROOT_URL}/api/v1/file_import_group`,
  API_STATUS_EMAIL: `${API_URL}/api/v1/email`,
  DATA_WORKER: {
    ROOT_URL: DATA_WORKER_ROOT_URL,
    BATCH_LINE_COUNT: parseInt(process.env.FILE_BATCH_LINE_COUNT) || 100000,
    S3_BUCKET: process.env.S3_BUCKET || (LOCAL_AWS ? 'ams-uploads-clean' : '')
  },
  API: {
    ROOT_URL: API_URL
  },
  JWT: {
    SECRET: process.env.JWT_SECRET || 'secret'
  },
  FEATURE_FLAGS: {
    DISABLE_SOURCE_FILES: (process.env.DISABLE_SOURCE_FILES === 'true')
  },
  DEV: {
    SQS: process.env.DEV_SQS || 'http://localhost:4100',
    S3: process.env.DEV_S3 || 'http://localhost:4569'
  },
  CACHE: {
    BUILD_CACHE_ENABLED,
    WAIT_FOR_CACHE_TO_BUILD: BUILD_CACHE_ENABLED && process.env.WAIT_FOR_CACHE_TO_BUILD === 'true',
    BATCH_NPI_COUNT: parseInt(process.env.CACHE_BATCH_NPI_COUNT) || 500,
    BATCH_TIN_COUNT: parseInt(process.env.CACHE_BATCH_TIN_COUNT) || 50
  },

  MAX_ERROR_BYTES: 49 * 1024 * 1024,
  LOG_IF_MORE_THEN_LINES: 50000


};
module.exports = config;
