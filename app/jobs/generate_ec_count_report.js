const { dataWorker, logger } = require('../services');

const handler = async (body, done) => {
  try {
    const { performanceYear, runSnapshot } = body;
    await dataWorker.get(
      `/api/v2/report/ec/count?performance_year=${performanceYear}&run_snapshot=${runSnapshot}&bypass_cache=1`
    );
    logger.info(`EC Count Report Cached: ${performanceYear} ${runSnapshot}`);
    done();
  } catch (err) {
    logger.error(err);
    done();
  }
};

module.exports = {
  jobName: 'GENERATE_EC_COUNT_REPORT',
  handler
};
