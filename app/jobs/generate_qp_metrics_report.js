const { dataWorker, logger } = require('../services');

const handler = async (body, done) => {
  try {
    const { performanceYear, runSnapshot } = body;
    await dataWorker.get(
      `/api/v2/report/qp/metrics?performance_year=${performanceYear}&run_snapshot=${runSnapshot}&bypass_cache=1`
    );
    logger.info(`QP Metrics Report Cached: ${performanceYear} ${runSnapshot}`);
    done();
  } catch (err) {
    logger.error(err);
    done();
  }
};

module.exports = {
  jobName: 'GENERATE_QP_METRICS_REPORT',
  handler
};
