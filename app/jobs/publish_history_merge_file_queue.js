const { dataWorker, createSQSAPIMessage } = require('../services');

const SQSMessageApi = createSQSAPIMessage.SQSMessageApi;

class PublishHistoryMergeFileQueue {
  static get BATCH_SIZE() {
    return 5000;
  }

  static get jobName() {
    return 'PUBLISH_HISTORY_MERGE_FILE_QUEUE';
  }

  static async getDetailRowsInformation(fileImportGroupId, fileId) {
    const fileMetadata = await dataWorker.get(`/api/v1/file_import_group/${fileImportGroupId}/file/${fileId}`);
    return {
      first_detail_row_index: fileMetadata.body.file.first_detail_row || 0,
      last_detail_row_index: fileMetadata.body.file.last_detail_row || 0
    };
  }

  static getFromRow(batchNumber, firstRowIndex) {
    return batchNumber === 1 ? firstRowIndex : // 2 because first detail record starts at 2nd row, first row is the header
      (PublishHistoryMergeFileQueue.BATCH_SIZE * (batchNumber - 1)) + 1;
  }

  static getToRow(batchNumber, lastBatch) {
    if (batchNumber === 1) {
      return lastBatch;
    }

    return lastBatch === 0 ?
        (PublishHistoryMergeFileQueue.BATCH_SIZE * batchNumber) + lastBatch :
        (PublishHistoryMergeFileQueue.BATCH_SIZE * (batchNumber - 1)) + lastBatch;
  }
  static async handler(body, done) {
    try {
      const { publishHistoryId, fileImportGroupId, fileIds, groupType } = body;

      if (groupType === 'pac') {
        SQSMessageApi.create(1, 'PUBLISH_HISTORY_MERGE_FILE_BATCH', {
          fileImportGroupId,
          publishHistoryId,
          fileIds,
          fromRowNumber: null,
          toRowNumber: null
        });
      } else {
        const recordIndices =
          await PublishHistoryMergeFileQueue.getDetailRowsInformation(fileImportGroupId, fileIds[0]);

        const numberOfBatches = recordIndices.last_detail_row_index / PublishHistoryMergeFileQueue.BATCH_SIZE;
        const lastBatch = recordIndices.last_detail_row_index % PublishHistoryMergeFileQueue.BATCH_SIZE;
        let batchNumber;

        for (batchNumber = 1; batchNumber < numberOfBatches; batchNumber += 1) {
          SQSMessageApi.create(1, 'PUBLISH_HISTORY_MERGE_FILE_BATCH', {
            fileImportGroupId,
            publishHistoryId,
            fileIds,
            fromRowNumber: PublishHistoryMergeFileQueue.getFromRow(batchNumber, recordIndices.first_detail_row_index),
            toRowNumber: PublishHistoryMergeFileQueue.BATCH_SIZE * batchNumber
          });
        }
        SQSMessageApi.create(1, 'PUBLISH_HISTORY_MERGE_FILE_BATCH', {
          fileImportGroupId,
          publishHistoryId,
          fileIds,
          fromRowNumber: PublishHistoryMergeFileQueue.getFromRow(batchNumber, recordIndices.first_detail_row_index),
          toRowNumber: PublishHistoryMergeFileQueue.getToRow(batchNumber, lastBatch)
        });
      }

      done();
    } catch (err) {
      done(err);
    }
  }
}

module.exports = PublishHistoryMergeFileQueue;
