const Promise = require('bluebird');
const { BATCH_NPI_COUNT, BATCH_TIN_COUNT } = require('../config').CACHE;
const { dataWorker, createSQSAPIBatchMessages, logger } = require('../services');
const { toChunks } = require('../utils');

const createMessages = async (array, type, perChunk, publishHistoryId) => {
  if (array.length === 0) {
    return;
  }

  const messages = toChunks(array, perChunk).map(chunk => ({
    jobName: 'PUBLISH_HISTORY_BUILD_CACHE_BATCH',
    body: {
      publishHistoryId,
      [type]: chunk
    }
  }));

  createSQSAPIBatchMessages(2, messages);
};

const handler = async (body, done) => {
  try {
    const publishHistoryId = body.publishHistoryId;

    const response = await dataWorker.patch(`/api/v1/publish_history/${publishHistoryId}/cache`, {
      status: 'building'
    });
    const publishHistory = response.body.data.publish_history;

    const dataResponses = await Promise.all(['npi', 'tin'].map(type =>
      dataWorker.get(`/api/v1/publish_history/${publishHistory.id}/data/${type}`)
    ));

    const npis = dataResponses[0].body.data.npis;
    const tins = dataResponses[1].body.data.tins;

    await dataWorker.patch(`/api/v1/publish_history/${publishHistory.id}/cache`, {
      expected: npis.length + tins.length
    });

    await createMessages(npis, 'npis', BATCH_NPI_COUNT, publishHistory.id);
    await createMessages(tins, 'tins', BATCH_TIN_COUNT, publishHistory.id);
    await dataWorker.patch(`/api/v1/publish_history/${publishHistory.id}`, {
      status: 'published',
      'cache.status': 'finished'
    });

    done();
  } catch (err) {
    logger.error(err);
    done(err);
  }
};

module.exports = {
  jobName: 'PUBLISH_HISTORY_BUILD_CACHE_QUEUE',
  handler
};
