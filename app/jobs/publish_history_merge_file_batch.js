const { dataWorker, createSQSAPIMessage } = require('../services');

const SQSMessageApi = createSQSAPIMessage.SQSMessageApi;

class PublishHistoryMergeFileBatch {
  static get jobName() {
    return 'PUBLISH_HISTORY_MERGE_FILE_BATCH';
  }

  static async mergeRowsOfFile(publishHistoryId, fileId, fromRowNumber, toRowNumber) {
    const response = await dataWorker.post(`/api/v1/publish_history/${publishHistoryId}/file/${fileId}/merge`, {
      fromRowNumber,
      toRowNumber
    });
    return response.body.data.publish_history;
  }

  static async handler(body, done) {
    try {
      const { publishHistoryId, fileImportGroupId, fileIds, fromRowNumber, toRowNumber } = body;

      const publishHistories = await Promise.all(fileIds.map(fileId =>
        PublishHistoryMergeFileBatch.mergeRowsOfFile(publishHistoryId, fileId, fromRowNumber, toRowNumber)));

      const finished = publishHistories.some(publishHistory =>
        publishHistory.status === 'publishing' && publishHistory.files.every(file => file.status === 'published')
      );

      if (finished) {
        SQSMessageApi.create(1, 'PUBLISH_HISTORY_MERGE_FINISHED', {
          fileImportGroupId,
          publishHistoryId
        });
      }

      done();
    } catch (err) {
      done(err);
    }
  }
}

module.exports = PublishHistoryMergeFileBatch;
