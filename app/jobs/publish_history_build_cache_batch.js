const config = require('../config');
const { dataWorker, logger } = require('../services');
const Promise = require('bluebird');
const { toChunks } = require('../utils');

const handler = async (body, done) => {
  try {
    const { publishHistoryId } = body;
    const npis = body.npis || [];
    const tins = body.tins || [];

    if (config.NODE_ENV === 'dev') {
      await Promise.delay(10);
    }

    const promises = [
      ...toChunks(npis, config.CACHE.BATCH_NPI_COUNT).map(chunk =>
        dataWorker.post(`/api/v2/qpp/npi?publish_history_id=${publishHistoryId}`, { npis: chunk })),
      ...toChunks(tins, config.CACHE.BATCH_TIN_COUNT).map(chunk =>
        dataWorker.post(`/api/v2/qpp/tin?publish_history_id=${publishHistoryId}`, { tins: chunk }))
    ];

    for (let i = 0; i < promises.length; i += 1) {
      // eslint-disable-next-line no-await-in-loop
      await promises[i];
    }

    const publishHistoryResponse =
      await dataWorker.put(`/api/v1/publish_history/${publishHistoryId}/cache/finished_batch`, {
        finished: npis.length + tins.length
      });
    const publishHistory = publishHistoryResponse.body.data.publish_history;

    if (publishHistory && publishHistory.cache && publishHistory.cache.finished === publishHistory.cache.expected) {
      await dataWorker.patch(`/api/v1/publish_history/${publishHistory.id}`, {
        status: 'published',
        'cache.status': 'finished'
      });

      logger.info(`Fully cached ${publishHistory.id}`);
    }

    done();
  } catch (err) {
    done(err);
  }
};

module.exports = {
  jobName: 'PUBLISH_HISTORY_BUILD_CACHE_BATCH',
  handler
};
