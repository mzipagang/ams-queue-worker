const config = require('../config');
const services = require('../services');

const handler = async (body, done) => {
  const { dataWorker, createSQSAPIMessage } = services;
  try {
    const fileImportGroupId = body.fileImportGroupId;
    const publishHistoryId = body.publishHistoryId;

    const res = await dataWorker.get(`/api/v1/file_import_group/${fileImportGroupId}`);
    const fileImportGroup = res.body.data.file_import_group;

    const files = fileImportGroup.files.map((file) => {
      if (file.status === 'finished') {
        file.publishedAt = fileImportGroup.updatedAt;
        return file;
      }
      return file;
    });

    await dataWorker.patch(`/api/v1/file_import_group/${fileImportGroupId}`, {
      status: 'published',
      files
    });

    const response = await dataWorker.get(`/api/v1/publish_history/${publishHistoryId}`);
    const publishHistory = response.body.data.publish_history;

    if (config.CACHE.BUILD_CACHE_ENABLED && publishHistory.group_type === 'pac') {
      createSQSAPIMessage(1, 'PUBLISH_HISTORY_BUILD_CACHE_QUEUE', {
        fileImportGroupId,
        publishHistoryId
      });
    }
    // If APM files or we don't want to wait for the cache to build
    if (publishHistory.group_type !== 'pac' || !config.CACHE.WAIT_FOR_CACHE_TO_BUILD) {
      await dataWorker.patch(`/api/v1/publish_history/${publishHistoryId}`, {
        status: 'published'
      });
    }

    done();
  } catch (err) {
    done(err);
  }
};

module.exports = {
  jobName: 'PUBLISH_HISTORY_MERGE_FINISHED',
  handler
};
