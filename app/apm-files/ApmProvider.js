
class ApmProvider {
  constructor(entityId, tin, tinTypeCode, npi, ccn, startDate, endDate, specialtyCode, participantTypeCode,
    organizationName, firstName, middleName, lastName) {
    this.entity_id = entityId;
    this.tin = tin;
    this.tin_type_code = tinTypeCode;
    this.npi = npi;
    this.ccn = ccn;
    this.start_date = startDate;
    this.end_date = endDate;
    this.specialty_code = specialtyCode;
    this.participant_type_code = participantTypeCode;
    this.organization_name = organizationName;
    this.first_name = firstName;
    this.middle_name = middleName;
    this.last_name = lastName;
  }

  static get fileType() {
    return 'cmmi-provider';
  }

  static get urlForTypeMappings() {
    return 'apm/provider';
  }
}

module.exports = ApmProvider;
