class ApmEntity {
  constructor(id, apmId, subdivId, startDate, endDate, name, dba, type, tin, npi, ccn, additionalInformation, address) {
    this.id = id;
    this.apm_id = apmId;
    this.subdiv_id = subdivId;
    this.start_date = startDate;
    this.end_date = endDate;
    this.name = name;
    this.dba = dba;
    this.type = type;
    this.tin = tin;
    this.npi = npi;
    this.ccn = ccn;
    this.additional_information = additionalInformation;
    this.address = address;
  }

  static get fileType() {
    return 'cmmi-entity';
  }

  static get urlForTypeMappings() {
    return 'apm/entity';
  }
}

module.exports = ApmEntity;
