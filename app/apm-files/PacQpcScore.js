const headerDetailTrailerFileParser = require('../services/fileParser/headerDetailTrailerFileParser');
const { getPerformanceYear, getRunNumber, getExecution } = require('../utils/headerUtilities');
const FileValidator = require('../utils/FileValidator');

class QpcScoreSummary {
  constructor(values) {
    this.entity_id = values.entity_id;
    this.final_qpc_score = values.final_qpc_score;
    this.current_achievement_points = values.current_achievement_points;
    this.prior_achievement_points = values.prior_achievement_points;
    this.high_priority_bonus_points = values.high_priority_bonus_points;
    this.cehrt_reporting_bonus = values.cehrt_reporting_bonus;
    this.quality_improvement_points = values.quality_improvement_points;
    this.scored_measures = values.scored_measures;
    this.excluded_measures = values.excluded_measures;
    this.excluded_measures_pay_for_reporting = values.excluded_measures_pay_for_reporting;
    this.excluded_measures_case_size = values.excluded_measures_case_size;
    this.performance_year = values.performance_year;
    this.run = values.run;
    this.execution = values.execution;
  }
}

class QpcScore extends FileValidator {
  constructor(entityId, measureId, measureScore, decileScore, meetsScoringCriteria,
    highPriorityMeasure, cehrtMeasure, performanceYear, run, execution) {
    super();
    this.entity_id = entityId;
    this.measure_id = measureId;
    this.measure_score = measureScore;
    this.decile_score = decileScore;
    this.meets_scoring_criteria = meetsScoringCriteria;
    this.high_priority_measure = highPriorityMeasure;
    this.cehrt_measure = cehrtMeasure;
    this.performance_year = performanceYear;
    this.run = run;
    this.execution = execution;
  }

  static get fileType() {
    return 'pac-qp_category_score';
  }

  static get urlForTypeMappings() {
    return 'pac/qpc_score';
  }

  static get schemaMap() {
    return function getSchemaMap(rowData) {
      const rowIdentifier = rowData['Record Identifier'];

      if (rowIdentifier === 'D') {
        return QpcScore.detailSchemaMap;
      }

      if (rowIdentifier === 'S') {
        return QpcScore.summarySchemaMap;
      }

      throw new Error('No Schema Mapping exists for this Row Identifier');
    };
  }

  static get detailSchemaMap() {
    return {
      'APM Entity ID': 'entity_id',
      'Measure ID': 'measure_id',
      'Measure Score': 'measure_score',
      'Decile Score': 'decile_score',
      'Meets Scoring Criteria Flag': 'meets_scoring_criteria',
      'High Priority Measure Flag': 'high_priority_measure',
      'CEHRT Measure Flag': 'cehrt_measure',
      'Performance Year': 'performance_year',
      Run: 'run',
      Execution: 'execution'
    };
  }

  static get summarySchemaMap() {
    return {
      'APM Entity ID': 'entity_id',
      'Final QPC Score': 'final_qpc_score',
      'Current Achievement Points': 'current_achievement_points',
      'Prior Achievement Points': 'prior_achievement_points',
      'High-Priority Bonus Points': 'high_priority_bonus_points',
      'CEHRT Reporting Bonus': 'cehrt_reporting_bonus',
      'Quality Improvement Points': 'quality_improvement_points',
      'Scored Measures': 'scored_measures',
      'Excluded Measures': 'excluded_measures',
      'Excluded Measures: pay for reporting': 'excluded_measures_pay_for_reporting',
      'Excluded Mesures: case size': 'excluded_measures_case_size',
      'Performance Year': 'performance_year',
      Run: 'run',
      Execution: 'execution'
    };
  }

  static fromRawFile(fileData) {
    if (fileData.final_qpc_score) {
      return new QpcScoreSummary(fileData);
    }

    return new QpcScore(fileData.entity_id, fileData.measure_id, fileData.measure_score, fileData.decile_score,
      fileData.meets_scoring_criteria, fileData.high_priority_measure, fileData.cehrt_measure,
      fileData.performance_year, fileData.run, fileData.execution);
  }

  static validateHeader(headerLine) {
    return super.validateHeaderUtility({
      headerLine,
      fileType: 'QPCS',
      snapshotPattern: '1'
    });
  }

  static validateParsedTrailer(trailer, header, recordsCount, summaryRecordCount) {
    return super.validateParsedTrailerUtility({
      header,
      trailer,
      recordsCount,
      validateSummaryCount: true,
      summaryRecordCount,
      fileType: 'QPCS',
      // !! run is used interchanagebly with snapshot as its parsed the same
      snapshotPattern: '1',
      snapshotPatternErrorMessage: 'Run must be a digit between 1',
      snapshotMatchErrorMessage: 'Header Run and Trailer Run must match'
    });
  }

  static get parser() {
    const headers = ['Record Identifier', 'Date', 'File Type', 'Performance Year', 'Run', 'Execution'];
    const trailers = [
      'Record Identifier',
      'Date',
      'File Type',
      'Performance Year',
      'Run',
      'Execution',
      'Summary Record Count',
      'Detail Record Count',
      'Total Record Count'
    ];
    const summaryHeaders = ['Record Identifier', 'APM Entity ID', 'Final QPC Score', 'Current Achievement Points',
      'Prior Achievement Points', 'High-Priority Bonus Points', 'CEHRT Reporting Bonus', 'Quality Improvement Points',
      'Scored Measures', 'Excluded Measures', 'Excluded Measures: pay for reporting', 'Excluded Mesures: case size'];
    const detailHeaders = ['Record Identifier', 'APM Entity ID', 'Measure ID', 'Measure Score',
      'Decile Score', 'Meets Scoring Criteria Flag', 'High Priority Measure Flag', 'CEHRT Measure Flag'];

    return warningCallback =>
      headerDetailTrailerFileParser(headers, detailHeaders, trailers, warningCallback, summaryHeaders);
  }
}

const mixins = {
  getPerformanceYear,
  getRunNumber,
  getExecution
};

Object.assign(QpcScore, mixins);
module.exports = QpcScore;
