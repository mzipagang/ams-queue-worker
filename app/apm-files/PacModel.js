const headerDetailTrailerFileParser = require('../services/fileParser/headerDetailTrailerFileParser');
const { getPerformanceYear, getSnapshot, getExecution } = require('../utils/headerUtilities');
const FileValidator = require('../utils/FileValidator');

class PacModel extends FileValidator {
  constructor(
    id, name, advancedApmFlag, mipsApmFlag, qualityReportingCategoryCode, performanceYear, snapshot, execution) {
    super();
    this.id = id;
    this.name = name;
    this.advanced_apm_flag = advancedApmFlag;
    this.mips_apm_flag = mipsApmFlag;
    this.quality_reporting_category_code = qualityReportingCategoryCode;
    this.performance_year = performanceYear;
    this.snapshot = snapshot;
    this.execution = execution;
  }

  static get fileType() {
    return 'pac-model';
  }

  static get urlForTypeMappings() {
    return 'pac/model';
  }

  static get schemaMap() {
    return {
      'APM ID': 'id',
      'APM Name': 'name',
      'Advanced APM Flag': 'advanced_apm_flag',
      'MIPS APM Flag': 'mips_apm_flag',
      'Quality Reporting Category Code': 'quality_reporting_category_code',
      'Performance Year': 'performance_year',
      Snapshot: 'snapshot',
      Execution: 'execution'
    };
  }

  static fromRawFile(fileData) {
    return new PacModel(fileData.id, fileData.name, fileData.advanced_apm_flag, fileData.mips_apm_flag,
      fileData.quality_reporting_category_code, fileData.performance_year, fileData.snapshot, fileData.execution);
  }

  static validateHeader(headerLine) {
    return super.validateHeaderUtility({ headerLine, fileType: 'APM' });
  }

  static validateParsedTrailer(trailer, header, recordsCount) {
    return super.validateParsedTrailerUtility({
      header, trailer, recordsCount, fileType: 'APM'
    });
  }

  static get parser() {
    const headers = ['Record Identifier', 'Date', 'File Type', 'Performance Year', 'Snapshot', 'Execution'];
    const trailers = [
      'Record Identifier', 'Date', 'File Type', 'Performance Year', 'Snapshot', 'Execution', 'Record Count'];
    const detailHeaders = ['Record Identifier', 'APM ID', 'APM Name', 'Advanced APM Flag',
      'MIPS APM Flag', 'Quality Reporting Category Code'];

    return warningCallback => headerDetailTrailerFileParser(headers, detailHeaders, trailers, warningCallback);
  }
}

const mixins = {
  getPerformanceYear,
  getSnapshot,
  getExecution
};
Object.assign(PacModel, mixins);
module.exports = PacModel;
