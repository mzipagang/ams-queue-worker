const moment = require('moment');
const characterDelimitedFileParser = require('../services/fileParser/characterDelimitedFileParser');
const ApmProvider = require('./ApmProvider');

class NgacoPreferredProvider extends ApmProvider {
  constructor(entityId, tin, npi, ccn, startDate, endDate, participantTypeCode, organizationName, firstName, lastName) {
    super(entityId, tin, null, npi, ccn, startDate, endDate, null, participantTypeCode, organizationName, firstName,
        null, lastName);
  }

  static get fileType() {
    return 'ngaco-preferred-provider';
  }

  static get urlForTypeMappings() {
    return 'apm/provider';
  }

  static get schemaMap() {
    return {
      '#PRVDR_TYPE_CD': 'provider_type_code',
      ACO_ID: 'aco_id',
      PRVDR_TIN_NUM: 'tin',
      PRVDR_ORG_NPI: 'organization_npi',
      PRVDR_IND_NPI: 'individual_npi',
      PRVDR_CCN_ID: 'ccn',
      ACO_EFCTV_DT: 'aco_start_date',
      ACO_TRMTN_DT: 'aco_end_date',
      ACO_PRVDR_EFCTV_DT: 'provider_start_date',
      ACO_PRVDR_TRMNTN_DT: 'provider_end_date',
      PRVDR_LGL_NAME: 'provider_lgl_name',
      PRVDR_CCN_LGL_NAME: 'provider_ccn_lgl_name',
      CCN_TYPE_CD: 'ccn_type_code',
      PRVDR_NPI_LGL_NAME: 'provider_npi_lgl_name',
      NPI_FIRST_NAME: 'npi_first_name',
      NPI_LAST_NAME: 'npi_last_name',
      BNFT_ENHNCMT_CD: 'benefit_enhancement_code',
      BNFT_ENHNCMT_EFCTV_DT: 'benefit_enhancement_effective_date',
      BNFT_ENHNCMT_TRMNTN_DT: 'benefit_enhancement_treatment_date',
      PTA_RDCTN_PCT: 'pta_rdctn_percentage',
      PTB_RDCTN_PCT: 'ptb_rdctn_percentage'
    };
  }

  static fromRawFile(fileData) {
    let startDate = null;
    let endDate = null;

    if (fileData.provider_start_date) {
      startDate = moment.utc(fileData.provider_start_date, 'MM/DD/YYYY');
      startDate = startDate.isValid() ? startDate.format('YYYY-MM-DD') : null;
    }

    if (fileData.provider_end_date) {
      endDate = moment.utc(fileData.provider_end_date, 'MM/DD/YYYY');
      endDate = endDate.isValid() ? endDate.format('YYYY-MM-DD') : null;
    }

    return new NgacoPreferredProvider(fileData.aco_id, fileData.tin,
        fileData.individual_npi || fileData.organization_npi, fileData.ccn, startDate, endDate,
        (fileData.provider_type_code === 'PF' ? 'Secondary' : 'Primary'), fileData.provider_lgl_name,
        fileData.npi_first_name, fileData.npi_last_name);
  }

  static validateHeader(headerLine) {
    if (!headerLine) {
      return false;
    }

    const headers = headerLine && headerLine.trim().split('|');
    const expectedHeaders = [
      '#PRVDR_TYPE_CD',
      'ACO_ID',
      'PRVDR_TIN_NUM',
      'PRVDR_ORG_NPI',
      'PRVDR_IND_NPI',
      'PRVDR_CCN_ID',
      'ACO_EFCTV_DT',
      'ACO_TRMTN_DT',
      'ACO_PRVDR_EFCTV_DT',
      'ACO_PRVDR_TRMNTN_DT',
      'PRVDR_LGL_NAME',
      'PRVDR_CCN_LGL_NAME',
      'CCN_TYPE_CD',
      'PRVDR_NPI_LGL_NAME',
      'NPI_FIRST_NAME',
      'NPI_LAST_NAME',
      'BNFT_ENHNCMT_CD',
      'BNFT_ENHNCMT_EFCTV_DT',
      'BNFT_ENHNCMT_TRMNTN_DT',
      'PTA_RDCTN_PCT',
      'PTB_RDCTN_PCT'
    ];

    return headers.length === 21 && !expectedHeaders.find(headerName => (headers.indexOf(headerName) === -1));
  }

  static get parser() {
    return characterDelimitedFileParser('|', this.headers).parser;
  }
}

module.exports = NgacoPreferredProvider;
