const moment = require('moment');
const { padLeftIfNumber } = require('../utils');

const recordIdentifierFileParser = require('../services/fileParser/recordIdentifierFileParser');
const ApmEntity = require('./ApmEntity');

class CmmiEntity extends ApmEntity {
  static get schemaMap() {
    return {
      'APM ID': 'apm_id',
      'SubDivision ID': 'subdiv_id',
      'Entity ID': 'id',
      'Entity Start Date': 'start_date',
      'Entity End Date': 'end_date',
      'Entity Name': 'name',
      'Entity DBA': 'dba',
      'Entity Type': 'type',
      'Entity TIN': 'tin',
      'Entity NPI': 'npi',
      'Entity CCN': 'ccn',
      'Entity Additional Information': 'additional_information',
      'Entity Address 1': 'address.address_1',
      'Entity Address 2': 'address.address_2',
      'Entity City': 'address.city',
      'Entity State': 'address.state',
      'Entity Zip5': 'address.zip5',
      'Entity Zip4': 'address.zip4'
    };
  }

  static get headers() {
    return [
      'Record Identifier',
      'APM ID',
      'SubDivision ID',
      'Entity ID',
      'Entity Start Date',
      'Entity End Date',
      'Entity Name',
      'Entity DBA',
      'Entity Type',
      'Entity TIN',
      'Entity NPI',
      'Entity CCN',
      'Entity Additional Information',
      'Entity Address 1',
      'Entity Address 2',
      'Entity City',
      'Entity State',
      'Entity Zip5',
      'Entity Zip4'
    ];
  }

  static fromRawFile(fileData) {
    const id = fileData.id ? padLeftIfNumber(fileData.id, 2) : null;
    const apmId = padLeftIfNumber(fileData.apm_id, 2);
    const subdivId = fileData.subdiv_id ? padLeftIfNumber(fileData.subdiv_id, 2) : null;

    let startDate = null;
    let endDate = null;

    if (fileData.start_date) {
      startDate = moment.utc(fileData.start_date, 'YYYYMMDD');
      startDate = startDate.isValid() ? startDate.format('YYYY-MM-DD') : null;
    }

    if (fileData.end_date) {
      endDate = moment.utc(fileData.end_date, 'YYYYMMDD');
      endDate = endDate.isValid() ? endDate.format('YYYY-MM-DD') : null;
    }

    return new CmmiEntity(id, apmId, subdivId, startDate, endDate, fileData.name, fileData.dba, fileData.type,
        fileData.tin, fileData.npi, fileData.ccn, fileData.additional_information, fileData.address);
  }

  static validateHeader(headerLine) {
    return !!(headerLine && headerLine.trim().match(/(H)\|([0-9]{8})\|APME\|([0-9]{2})(\|+)?$/));
  }

  static get parser() {
    return recordIdentifierFileParser('|', this.headers).parser;
  }

  /**
   * @argument {string} header the header to parse for the APM ID
   * @returns the APM ID associated with the file
   */
  static getApmId(header) {
    const headerParams = header.split('|');
    return headerParams[headerParams.length - 1];
  }
}

module.exports = CmmiEntity;
