//  Individual QP Status Analytic Information
const headerDetailTrailerFileParser = require('../services/fileParser/headerDetailTrailerFileParser');
const { getPerformanceYear, getSnapshot, getExecution } = require('../utils/headerUtilities');
const FileValidator = require('../utils/FileValidator');

class IndividualQpStatus extends FileValidator {
  constructor(npi, qpStatus, performanceYear, snapshot, execution) {
    super();
    this.npi = npi;
    this.qp_status = qpStatus;
    this.performance_year = performanceYear;
    this.snapshot = snapshot;
    this.execution = execution;
  }

  static get fileType() {
    return 'individual_qp_status';
  }

  static get urlForTypeMappings() {
    return 'apm/individual_qp_status';
  }

  static get schemaMap() {
    return {
      NPI: 'npi',
      'QP Status': 'qp_status',
      'Performance Year': 'performance_year',
      Snapshot: 'snapshot',
      Execution: 'execution'
    };
  }

  static fromRawFile(fileData) {
    return new IndividualQpStatus(
        fileData.npi, fileData.qp_status, fileData.performance_year, fileData.snapshot, fileData.execution);
  }

  static validateHeader(headerLine) {
    return super.validateHeaderUtility({
      headerLine,
      fileType: 'IANT',
      snapshotPattern: '[1-4]'
    });
  }

  static validateParsedTrailer(trailer, header, recordsCount) {
    return super.validateParsedTrailerUtility({
      header,
      trailer,
      recordsCount,
      fileType: 'IANT',
      snapshotPattern: '[1-4]',
      snapshotPatternErrorMessage: 'Snapshot must be a digit between 1-4'
    });
  }

  static get parser() {
    const headers = ['Record Identifier', 'Date', 'File Type', 'Performance Year', 'Snapshot', 'Execution'];
    const detailHeaders = ['Record Identifier', 'NPI', 'QP Status'];
    const trailers = [
      'Record Identifier', 'Date', 'File Type', 'Performance Year', 'Snapshot', 'Execution', 'Record Count'];

    return () => headerDetailTrailerFileParser(headers, detailHeaders, trailers);
  }
}

const mixins = {
  getPerformanceYear,
  getSnapshot,
  getExecution
};

Object.assign(IndividualQpStatus, mixins);

module.exports = IndividualQpStatus;
