const config = require('../config');

const AcoOsEntity = require('./AcoEntity');
const CmmiEntity = require('./CmmiEntity');
const CmmiProvider = require('./CmmiProvider');
const MdmProvider = require('./MdmProvider');
const NgacoPreferredProvider = require('./NgacoPreferredProvider');
const PacEntity = require('./PacEntity');
const PacLvt = require('./PacLvt');
const PacModel = require('./PacModel');
const PacProvider = require('./PacProvider');
const PacQpScore = require('./PacQpScore');
const PacQpStatus = require('./PacQpStatus');
const PacSubdivision = require('./PacSubdivision');
const IndividualQpThreshold = require('./IndividualQpThreshold');
const IndividualQpStatus = require('./IndividualQpStatus');
const EntityQpThreshold = require('./EntityQpThreshold');
const EntityQpStatus = require('./EntityQpStatus');
const EntityEligibility = require('./EntityEligibility');

const AbridgedQpStatus = require('./AbridgedQpStatus');
const AbridgedLvt = require('./AbridgedLvt');
const AbridgedQpScore = require('./AbridgedQpScore');

const Beneficiary = require('./Beneficiary');
const BeneficiaryMdm = require('./BeneficiaryMdm');
const QpcScore = require('./PacQpcScore');

// TODO: this object could be melded into the default exports as well which would eliminate the need for exporting another object
// For instance, it could be used like:
//
// var fileTypes = require(...);
// var fileTypeInstance = fileTypes[type]
//
// instead of
//
// var fileTypes = require(...);
// var fileTypeInstance = fileTypes.importFileTypeMappings[type];
const sourceFiles = [
  AcoOsEntity,
  CmmiEntity,
  CmmiProvider,
  MdmProvider,
  NgacoPreferredProvider,
  EntityEligibility,
  IndividualQpStatus,
  IndividualQpThreshold,
  EntityQpStatus,
  EntityQpThreshold
];

const pacFiles = [
  PacEntity,
  PacLvt,
  PacModel,
  PacProvider,
  PacQpScore,
  PacQpStatus,
  PacSubdivision,
  QpcScore,
  AbridgedQpStatus,
  AbridgedQpScore,
  AbridgedLvt,
  Beneficiary,
  BeneficiaryMdm
];

const fileClasses = [
  ...(config.FEATURE_FLAGS.DISABLE_SOURCE_FILES ? [] : sourceFiles),
  ...pacFiles
];

const manualFiles = {
  'cmmi-entity': true,
  'cmmi-provider': true,
  'pac-entity': true,
  'pac-lvt': true,
  'pac-model': true,
  'pac-provider': true,
  'pac-qp_score': true,
  'pac-qp_status': true,
  'pac-subdivision': true,
  beneficiary: true
};

/**
 * Gets the file type by running header validation against all known types for the passed in header argument.
 * If the header agrument passes a given files header validation function it can be assumed it is of that type
 * @param {string} header the raw header string that will be used to determine the file type
 * @return {Object} meta information about the matched file type
 * @property {string} name the name of the matched file type
 * @property {Object} instance the Object associated with the matched file type
 * @throws {InvalidHeaderError} when a given header string matches 0 or multiple file types; a header should have a 1:1 with a file type
 */
const getFileType = (header) => {
  const matchedFileTypes = fileClasses.filter(fileClass => fileClass.validateHeader(header));

  // this check is more of a precaution because no header should be associated with more than 1 file type
  if (matchedFileTypes.length !== 1) {
    return {
      error: true,
      message: 'Invalid header in file'
    };
  }

  const name = matchedFileTypes[0].fileType;
  const instance = matchedFileTypes[0];

  return {
    name,
    instance
  };
};

/**
 * Some files are created manually. This function determines if the given file's type
 * is associated with those manually created files. It does not guarantee that the file
 * is a manual file but it does gurantee that the type is associated with a manual file.
 * @param {string} type the file type to check
 * @returns {boolean} true if the type is associated with a manual file: false otherwise
 */
function isManualFile(type) {
  return !!manualFiles[type];
}


module.exports = {
  ...config.FEATURE_FLAGS.DISABLE_SOURCE_FILES ? [] : {
    AcoOsEntity,
    CmmiEntity,
    CmmiProvider,
    MdmProvider,
    NgacoPreferredProvider,
    EntityEligibility,
    IndividualQpStatus,
    IndividualQpThreshold,
    EntityQpStatus,
    EntityQpThreshold

  },
  ...{
    PacEntity,
    PacLvt,
    PacModel,
    PacProvider,
    PacQpScore,
    PacQpStatus,
    PacSubdivision,
    AbridgedQpStatus,
    AbridgedQpScore,
    AbridgedLvt,
    Beneficiary
  },
  fileClasses,
  getFileType,
  isManualFile
};
