const moment = require('moment');

const recordIdentifierFileParser = require('../services/fileParser/recordIdentifierFileParser');
const ApmProvider = require('./ApmProvider');

class CmmiProvider extends ApmProvider {
  constructor(entityId, tin, npi, ccn, startDate, endDate) {
    super(entityId, tin, null, npi, ccn, startDate, endDate, null, null,
        null, null, null, null);
  }

  static get headers() {
    return [
      'Record Identifier',
      'Entity ID',
      'Provider TIN',
      'Provider NPI',
      'Provider CCN',
      'Provider Start Date',
      'Provider End Date'
    ];
  }

  static get schemaMap() {
    return {
      'Entity ID': 'entity_id',
      'Provider TIN': 'provider_tin',
      'Provider NPI': 'provider_npi',
      'Provider CCN': 'provider_ccn',
      'Provider Start Date': 'start_date',
      'Provider End Date': 'end_date'
    };
  }

  static fromRawFile(fileData) {
    let startDate = null;
    let endDate = null;

    if (fileData.start_date) {
      startDate = moment.utc(fileData.start_date, 'YYYYMMDD');
      startDate = startDate.isValid() ? startDate.format('YYYY-MM-DD') : null;
    }

    if (fileData.end_date) {
      endDate = moment.utc(fileData.end_date, 'YYYYMMDD');
      endDate = endDate.isValid() ? endDate.format('YYYY-MM-DD') : null;
    }

    return new CmmiProvider(fileData.entity_id, fileData.provider_tin, fileData.provider_npi,
        fileData.provider_ccn, startDate, endDate);
  }

  static validateHeader(headerLine) {
    return !!(headerLine && headerLine.trim().match(/(H)\|([0-9]{8})\|PRVD\|([0-9]{2})(\|+)?$/));
  }

  static get parser() {
    return recordIdentifierFileParser('|', this.headers).parser;
  }

  /**
   * @argument {string} header the header to parse for the APM ID
   * @returns the APM ID associated with the file
  */
  static getApmId(header) {
    const headerParams = header.split('|');
    return headerParams[headerParams.length - 1];
  }
}

module.exports = CmmiProvider;
