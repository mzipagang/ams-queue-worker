const headerDetailTrailerFileParser = require('../services/fileParser/headerDetailTrailerFileParser');
const { getPerformanceYear, getRunNumber, getExecution } = require('../utils/headerUtilities');
const FileValidator = require('../utils/FileValidator');

const RUN_PATTERN = '(P|[1-2])';

class PacLvt extends FileValidator {
  constructor(entityId, status, patients, payments, smallStatus, complexPatientScore, performanceYear,
    run, execution) {
    super();
    this.entity_id = entityId;
    this.status = status;
    this.patients = patients;
    this.payments = payments;
    this.small_status = smallStatus;
    this.complex_patient_score = complexPatientScore;
    this.performance_year = performanceYear;
    this.run = run;
    this.execution = execution;
  }

  static get fileType() {
    return 'pac-lvt';
  }

  static get urlForTypeMappings() {
    return 'pac/lvt';
  }

  static get schemaMap() {
    return {
      'APM Entity ID': 'entity_id',
      'APM LVT Status': 'status',
      'Low Volume Bene Count': 'patients',
      'Low Volume Part B Expenditures': 'payments',
      'Small Status APM Entity Code': 'small_status',
      'Complex Patient Score': 'complex_patient_score',
      'Performance Year': 'performance_year',
      Run: 'run',
      Execution: 'execution'
    };
  }

  static fromRawFile(fileData) {
    return new PacLvt(
      fileData.entity_id,
      fileData.status,
      fileData.patients,
      fileData.payments,
      fileData.small_status,
      fileData.complex_patient_score,
      fileData.performance_year,
      fileData.run,
      fileData.execution
    );
  }

  static validateHeader(headerLine) {
    return super.validateHeaderUtility({
      headerLine,
      fileType: 'ALVT',
      snapshotPattern: RUN_PATTERN // alias to "run number" is in same placement
    });
  }

  static validateParsedTrailer(trailer, header, recordsCount) {
    return super.validateParsedTrailerUtility({
      header,
      trailer,
      recordsCount,

      // !! run is used interchanagebly with snapshot as its parsed the same
      snapshotPattern: RUN_PATTERN,
      snapshotPatternErrorMessage: 'Run must be character P or a digit between 1-2',
      snapshotMatchErrorMessage: 'Header Run and Trailer Run must match',

      fileType: 'ALVT'
    });
  }

  static get parser() {
    const headers = ['Record Identifier', 'Date', 'File Type', 'Performance Year', 'Run', 'Execution'];
    const trailers = [
      'Record Identifier', 'Date', 'File Type', 'Performance Year', 'Run', 'Execution', 'Record Count'];
    const detailHeaders = [
      'Record Identifier',
      'APM Entity ID',
      'APM LVT Status',
      'Low Volume Part B Expenditures',
      'Low Volume Bene Count',
      'Small Status APM Entity Code',
      'Complex Patient Score'
    ];

    return warningCallback => headerDetailTrailerFileParser(headers, detailHeaders, trailers, warningCallback);
  }
}

const mixins = {
  getPerformanceYear,
  getRunNumber,
  getExecution
};

Object.assign(PacLvt, mixins);
module.exports = PacLvt;
