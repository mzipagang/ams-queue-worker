const headerDetailTrailerFileParser = require('../services/fileParser/headerDetailTrailerFileParser');
const { getPerformanceYear, getRunNumber, getExecution } = require('../utils/headerUtilities');
const FileValidator = require('../utils/FileValidator');

const RUN_PATTERN = '[1-2]';

class EntityEligibility extends FileValidator {
  constructor(
    entityId,
    claimsTypes,
    lvStatusCode,
    lvStatusReason,
    lvPartBExpenditures,
    lvBeneCount,
    smallStatusApmEntityCode,
    smallStatusClinicianCount,
    complexPatientScore,
    performanceYear,
    run,
    execution
  ) {
    super();
    this.entity_id = entityId;
    this.claims_types = claimsTypes;
    this.lv_status_code = lvStatusCode;
    this.lv_status_reason = lvStatusReason;
    this.lv_part_b_expenditures = lvPartBExpenditures;
    this.lv_bene_count = lvBeneCount;
    this.small_status_apm_entity_code = smallStatusApmEntityCode;
    this.small_status_clinician_count = smallStatusClinicianCount;
    this.complex_patient_score = complexPatientScore;
    this.performance_year = performanceYear;
    this.run = run;
    this.execution = execution;
  }

  static get fileType() {
    return 'entity-eligibility';
  }

  static get urlForTypeMappings() {
    return 'apm/entity-eligibility';
  }

  static get schemaMap() {
    return {
      'APM Entity Id': 'entity_id',
      'Claims Types': 'claims_types',
      'Low Volume Status Code': 'lv_status_code',
      'Low Volume Status Reason': 'lv_status_reason',
      'Low Volume Part B Expenditures': 'lv_part_b_expenditures',
      'Low Volume Bene Count': 'lv_bene_count',
      'Small Status APM Entity Code': 'small_status_apm_entity_code',
      'Small Status Clinician Count': 'small_status_clinician_count',
      'Complex Patient Score': 'complex_patient_score',
      'Performance Year': 'performance_year',
      Run: 'run',
      Execution: 'execution'
    };
  }

  static fromRawFile(fileData) {
    const partBExpenditures = Math.round(fileData.lv_part_b_expenditures * 100) / 100;
    const lvBeneCount = Number(fileData.lv_bene_count);
    const smallStatusClinicianCount = Number(fileData.small_status_clinician_count);

    return new EntityEligibility(
      fileData.entity_id,
      fileData.claims_types,
      fileData.lv_status_code,
      fileData.lv_status_reason,
      partBExpenditures,
      lvBeneCount,
      fileData.small_status_apm_entity_code,
      smallStatusClinicianCount,
      fileData.complex_patient_score,
      fileData.performance_year,
      fileData.run,
      fileData.execution
    );
  }

  static validateHeader(headerLine) {
    return super.validateHeaderUtility({
      headerLine,
      snapshotPattern: RUN_PATTERN, // alias to "run number" is in same placement
      fileType: 'ELIG'
    });
  }

  static validateParsedTrailer(trailer, header, recordsCount) {
    return super.validateParsedTrailerUtility({
      header,
      trailer,
      recordsCount,

      // !! run is used interchanagebly with snapshot as its parsed the same
      snapshotPattern: RUN_PATTERN,
      snapshotPatternErrorMessage: 'Run must be a digit between 1-2',
      snapshotMatchErrorMessage: 'Header Run and Trailer Run must match',

      fileType: 'ELIG'
    });
  }

  static get parser() {
    const headers = ['Record Identifier', 'File Date', 'File Type', 'MIPS Performance Year', 'Run', 'Execution'];
    const detailHeaders = [
      'Record Identifier',
      'APM Entity Id',
      'Claims Types',
      'Low Volume Status Code',
      'Low Volume Status Reason',
      'Low Volume Part B Expenditures',
      'Low Volume Bene Count',
      'Small Status APM Entity Code',
      'Small Status Clinician Count',
      'Complex Patient Score'
    ];

    const trailers = ['Record Identifier', 'File Date', 'File Type',
      'MIPS Performance Year', 'Run', 'Execution', 'Record Count'];

    return () => headerDetailTrailerFileParser(headers, detailHeaders, trailers);
  }
}

const mixins = {
  getPerformanceYear,
  getRunNumber,
  getExecution
};

Object.assign(EntityEligibility, mixins);

module.exports = EntityEligibility;
