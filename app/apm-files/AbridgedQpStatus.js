const recordIdentifierFileParser = require('../services/fileParser/recordIdentifierFileParser');
const { getPerformanceYear, getRunSnapshot, getSnapshotId } = require('../utils/headerUtilities');
/* istanbul ignore next */
/* This is the same as the PacQpScore file - per Andy we will remove since they're identical */
/* Will remove in a future sprint, leaving for now 2018-09-28 - JM */
class AbridgedQpStatus {
  constructor(npi, qpStatus, performanceYear, runSnapshot, snapshotId) {
    this.npi = npi;
    this.qp_status = qpStatus;
    this.performance_year = performanceYear;
    this.run_snapshot = runSnapshot;
    this.snapshot_id = snapshotId;
  }

  static get fileType() {
    return 'abridged_qp_status';
  }

  static get urlForTypeMappings() {
    return 'pac/abridged_qp_status';
  }

  static get schemaMap() {
    return {
      NPI: 'npi',
      'QP Status': 'qp_status',
      'Performance Year': 'performance_year',
      'Run Snapshot': 'run_snapshot',
      'Snapshot ID': 'snapshot_id'
    };
  }

  static fromRawFile(fileData) {
    return new AbridgedQpStatus(
      fileData.npi, fileData.qp_status, fileData.performance_year, fileData.run_snapshot, fileData.snapshot_id
    );
  }

  static validateHeader() {
    // NOTE: This file type is the same as the PacQpStatus file, will be removing in a future sprint
    return false;
    // return !!(headerLine && headerLine.trim().match(/(H)\|([0-9]{8})\|QPST\|PY([0-9]{4})\|[PISF]\|([^|]+)$/));
  }

  static get parser() {
    return recordIdentifierFileParser('|', ['Record Identifier', 'NPI', 'QP Status']).parser;
  }
}

const mixins = {
  getPerformanceYear,
  getRunSnapshot,
  getSnapshotId
};
Object.assign(AbridgedQpStatus, mixins);
module.exports = AbridgedQpStatus;
