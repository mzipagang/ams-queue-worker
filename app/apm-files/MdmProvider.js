const moment = require('moment');
const { padLeftIfNumber } = require('../utils');
const recordIdentifierFileParser = require('../services/fileParser/recordIdentifierFileParser');
const programIdHasMatchingApmId = require('../FilterStrategies/ProgramIdHasMatchingApmId');
const ApmProvider = require('./ApmProvider');

class MdmProvider extends ApmProvider {
  constructor(entityId, tin, tinTypeCode, npi, ccn, startDate, endDate, specialtyCode, organizationName, firstName,
    middleName, lastName) {
    super(entityId, tin, tinTypeCode, npi, ccn, startDate, endDate, specialtyCode, null, organizationName, firstName,
        middleName, lastName);
  }

  static get fileType() {
    return 'mdm-provider';
  }

  static get urlForTypeMappings() {
    return 'apm/provider';
  }

  static fromRawFile(fileData) {
    const entityId = padLeftIfNumber(fileData.entity_id, 2);
    let startDate = null;
    let endDate = null;

    if (fileData.start_date) {
      startDate = moment.utc(fileData.start_date, 'YYYYMMDD');
      startDate = startDate.isValid() ? startDate.format('YYYY-MM-DD') : null;
    }

    if (fileData.end_date) {
      endDate = moment.utc(fileData.end_date, 'YYYYMMDD');
      endDate = endDate.isValid() ? endDate.format('YYYY-MM-DD') : null;
    }

    return new MdmProvider(entityId, fileData.tin, fileData.tin_type_code, fileData.npi, fileData.ccn, startDate,
        endDate, fileData.specialty_code, fileData.organization_name,
        fileData.first_name, fileData.middle_name, fileData.last_name);
  }

  static get schemaMap() {
    return {
      'Program Organization ID': 'entity_id',
      'Health Care Provider Tax Identification Number': 'tin',
      'Federal Tax Identification Type Code': 'tin_type_code',
      'National Provider Identifier for the Health Care Provider': 'npi',
      'CMS CCN': 'ccn',
      'Provider Specialty Code': 'specialty_code',
      'Provider Affiliation Effective Date': 'start_date',
      'Provider Affiliation End Date': 'end_date',
      'Provider Organization Name': 'organization_name',
      'Person First Name': 'first_name',
      'Person Middle Name': 'middle_name',
      'Person Last Name': 'last_name'
    };
  }

  static get headers() {
    return [
      'Record ID',
      'Program ID',
      'Program Organization ID',
      'Health Care Provider Tax Identification Number',
      'Federal Tax Identification Type Code',
      'National Provider Identifier for the Health Care Provider',
      'Provider Legacy ID',
      'CMS CCN',
      'Provider Specialty Code',
      'Provider Affiliation Effective Date',
      'Provider Affiliation End Date',
      'Provider Organization Name',
      'Person First Name',
      'Person Middle Name',
      'Person Last Name',
      'Person Name Suffix',
      'Program Organization Payment Track',
      'Program Organization Payment Track Effective Date',
      'Program Organization Payment Track End Date',
      'Provider Other ID Type'
    ];
  }

  static validateHeader(headerLine) {
    return !!(headerLine && headerLine.trim().match(/(H)\|([0-9]{14})\|PRVEXT$/));
  }

  static get parser() {
    return recordIdentifierFileParser('|', this.headers).parser;
  }

  static get filters() {
    return [programIdHasMatchingApmId];
  }
}

module.exports = MdmProvider;
