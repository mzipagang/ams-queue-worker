const moment = require('moment');
const headerDetailTrailerFileParser = require('../services/fileParser/headerDetailTrailerFileParser');

class BeneficiaryMdm {
  constructor(
    programId,
    programOrgId,
    beneId,
    hicn,
    firstName,
    middleName,
    lastName,
    dob,
    gender,
    startDate,
    endDate,
    categoryCode,
    mbi,
    programOrgPaymentTrack,
    poptStartDate,
    poptEndDate,
    voluntaryAlignment,
    otherId,
    otherIdType,
  ) {
    this.program_id = programId;
    this.program_org_id = programOrgId;
    this.id = beneId;
    this.hicn = hicn;
    this.first_name = firstName;
    this.middle_name = middleName;
    this.last_name = lastName;
    this.dob = dob;
    this.gender = gender;
    this.start_date = startDate;
    this.end_date = endDate;
    this.category_code = categoryCode;
    this.program_org_payment_track = programOrgPaymentTrack;
    this.program_org_payment_track_start_date = poptStartDate;
    this.program_org_payment_track_end_date = poptEndDate;
    this.voluntary_alignment = voluntaryAlignment;
    this.other_id = otherId;
    this.other_id_type = otherIdType;
    this.mbi = mbi;
  }

  static get fileType() {
    return 'beneficiary_mdm';
  }

  static get urlForTypeMappings() {
    return 'apm/beneficiary';
  }

  static get schemaMap() {
    return {
      'Program ID': 'program_id',
      'Program Org Id': 'program_org_id',
      'Beneficiary Id': 'id',
      'Beneficiary HICN': 'hicn',
      'Beneficiary First Name': 'first_name',
      'Beneficiary Middle Name': 'middle_name',
      'Beneficiary Last Name': 'last_name',
      'Beneficiary Date of Birth': 'dob',
      'Beneficiary Gender': 'gender',
      'Beneficiary Start Date': 'start_date',
      'Beneficiary End Date': 'end_date',
      'Beneficiary Category Code': 'category_code',
      'Program Org Payment Track': 'program_org_payment_track',
      'Program Org Payment Track Start Date': 'program_org_payment_track_start_date',
      'Program Org Payment Track End Date': 'program_org_payment_track_end_date',
      'Voluntary Alignment': 'voluntary_alignment',
      'Beneficiary Other Id': 'other_id',
      'Beneficiary Other Id Type': 'other_id_type',
      'Beneficiary MBI': 'mbi'
    };
  }

  static fromRawFile(fileData) {
    let startDate = null;
    let endDate = null;
    let dob; // if there is no dob save as undefined; if dob date is invalid saves as null
    // dob validation needs to distinguish the two, whereas start and end date do not

    if (fileData.start_date) {
      startDate = moment.utc(fileData.start_date, 'YYYYMMDD');
      startDate = startDate.isValid() ? startDate.format('YYYY-MM-DD') : null;
    }

    if (fileData.end_date) {
      endDate = moment.utc(fileData.end_date, 'YYYYMMDD');
      endDate = endDate.isValid() ? endDate.format('YYYY-MM-DD') : null;
    }

    if (fileData.dob) {
      dob = moment.utc(fileData.dob, 'YYYYMMDD');
      dob = dob.isValid() ? dob.format('YYYY-MM-DD') : null;
    }

    return new BeneficiaryMdm(
      fileData.program_id,
      fileData.program_org_id,
      fileData.id,
      fileData.hicn,
      fileData.first_name,
      fileData.middle_name,
      fileData.last_name,
      dob,
      fileData.gender,
      startDate,
      endDate,
      fileData.category_code,
      fileData.mbi,
      fileData.program_org_payment_track,
      fileData.program_org_payment_track_start_date,
      fileData.program_org_payment_track_end_date,
      fileData.voluntary_alignment,
      fileData.other_id,
      fileData.other_id_type,
    );
  }

  static validateHeader(headerLine) {
    return !!(headerLine && headerLine.trim().match(/(H)\|([0-9]{14})\|BENEXT/));
  }

  static validateParsedTrailer(trailer, header, recordsCount) {
    if (trailer['File Type'] !== 'BENEXT') {
      return {
        error: true,
        warning: false,
        message: 'File Type must be "BENEXT".'
      };
    }

    if (trailer.Date !== header.Date) {
      return {
        error: true,
        warning: false,
        message: 'Date in Trailer Record must match Date in Header Record.'
      };
    }
    if (trailer['Record Identifier'] !== 'T') {
      return {
        error: true,
        warning: false,
        message: 'Invalid trailer in file.'
      };
    }
    return {
      error: false,
      warning: parseInt(trailer['Record Count']) !== recordsCount,
      message: 'Record count in the Trailer does not equal the number of Detail records'
    };
  }

  static get parser() {
    const headers = ['Record Identifier', 'Date', 'File Type'];
    const trailers = ['Record Identifier', 'Date', 'File Type', 'Record Count'];
    const detailHeaders = [
      'Record Identifier',
      'Program ID',
      'Program Org Id',
      'Beneficiary Id',
      'Beneficiary HICN',
      'Beneficiary First Name',
      'Beneficiary Middle Name',
      'Beneficiary Last Name',
      'Beneficiary Date of Birth',
      'Beneficiary Gender',
      'Beneficiary Start Date',
      'Beneficiary End Date',
      'Beneficiary Category Code',
      'Beneficiary MBI',
      'Program Org Payment Track',
      'Program Org Payment Track Start Date',
      'Program Org Payment Track End Date',
      'Voluntary Alignment',
      'Beneficiary Other Id',
      'Beneficiary Other Id Type'
    ];

    return warningCallback =>
      headerDetailTrailerFileParser(headers, detailHeaders, trailers, warningCallback);
  }
}

module.exports = BeneficiaryMdm;
