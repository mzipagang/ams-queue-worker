//  Entity QP Threshold Analytic Information
const headerDetailTrailerFileParser = require('../services/fileParser/headerDetailTrailerFileParser');
const { getPerformanceYear, getSnapshot, getExecution } = require('../utils/headerUtilities');
const FileValidator = require('../utils/FileValidator');

class EntityQpThreshold extends FileValidator {
  constructor(apmId, subdivisionId, entityId, tin, npi, qpStatus, expendituresNumerator, expendituresDenominator,
    paymentThresholdScore, paymentThresholdMet, beneficiariesNumerator, beneficiariesDenominator,
    patientThresholdScore, patientThresholdMet, performanceYear, snapshot, execution) {
    super();
    this.apm_id = apmId;
    this.subdivision_id = subdivisionId;
    this.entity_id = entityId;
    this.tin = tin;
    this.npi = npi;
    this.qp_status = qpStatus;
    this.expenditures_numerator = expendituresNumerator;
    this.expenditures_denominator = expendituresDenominator;
    this.payment_threshold_score = paymentThresholdScore;
    this.payment_threshold_met = paymentThresholdMet;
    this.beneficiaries_numerator = beneficiariesNumerator;
    this.beneficiaries_denominator = beneficiariesDenominator;
    this.patient_threshold_score = patientThresholdScore;
    this.patient_threshold_met = patientThresholdMet;
    this.performance_year = performanceYear;
    this.snapshot = snapshot;
    this.execution = execution;
  }

  static get fileType() {
    return 'entity_qp_threshold';
  }

  static get urlForTypeMappings() {
    return 'apm/entity_qp_threshold';
  }

  static get schemaMap() {
    return {
      'APM ID': 'apm_id',
      'Subdivision ID': 'subdivision_id',
      'APM Entity ID': 'entity_id',
      'Provider TIN': 'tin',
      'Provider NPI': 'npi',
      'QP Status': 'qp_status',
      'Expenditures Numerator': 'expenditures_numerator',
      'Expenditures Denominator': 'expenditures_denominator',
      'Payment Threshold Score': 'payment_threshold_score',
      'Payment Threshold Met': 'payment_threshold_met',
      'Beneficiaries Numerator': 'beneficiaries_numerator',
      'Beneficiaries Denominator': 'beneficiaries_denominator',
      'Patient Threshold Score': 'patient_threshold_score',
      'Patient Threshold Met': 'patient_threshold_met',
      'Performance Year': 'performance_year',
      Snapshot: 'snapshot',
      Execution: 'execution'
    };
  }

  static fromRawFile(fileData) {
    return new EntityQpThreshold(
      fileData.apm_id,
      fileData.subdivision_id,
      fileData.entity_id,
      fileData.tin,
      fileData.npi,
      fileData.qp_status,
      fileData.expenditures_numerator,
      fileData.expenditures_denominator,
      fileData.payment_threshold_score,
      fileData.payment_threshold_met,
      fileData.beneficiaries_numerator,
      fileData.beneficiaries_denominator,
      fileData.patient_threshold_score,
      fileData.patient_threshold_met,
      fileData.performance_year,
      fileData.snapshot,
      fileData.execution
    );
  }

  static validateHeader(headerLine) {
    return super.validateHeaderUtility({
      headerLine,
      fileType: 'ANSC'
    });
  }

  static validateParsedTrailer(trailer, header, recordsCount) {
    return super.validateParsedTrailerUtility({
      trailer, header, recordsCount, fileType: 'ANSC'
    });
  }

  static get parser() {
    const headers = ['Record Identifier', 'Date', 'File Type', 'Performance Year', 'Snapshot', 'Execution'];
    const detailHeaders = [
      'Record Identifier',
      'APM ID',
      'Subdivision ID',
      'APM Entity ID',
      'Provider TIN',
      'Provider NPI',
      'QP Status',
      'Expenditures Numerator',
      'Expenditures Denominator',
      'Payment Threshold Score',
      'Payment Threshold Met',
      'Beneficiaries Numerator',
      'Beneficiaries Denominator',
      'Patient Threshold Score',
      'Patient Threshold Met'
    ];
    const trailers = [
      'Record Identifier', 'Date', 'File Type', 'Performance Year', 'Snapshot', 'Execution', 'Record Count'];

    return () => headerDetailTrailerFileParser(headers, detailHeaders, trailers);
  }

}
const mixins = {
  getPerformanceYear,
  getSnapshot,
  getExecution
};

Object.assign(EntityQpThreshold, mixins);
module.exports = EntityQpThreshold;
