const headerDetailTrailerFileParser = require('../services/fileParser/headerDetailTrailerFileParser');
const { getPerformanceYear, getSnapshot, getExecution } = require('../utils/headerUtilities');
const FileValidator = require('../utils/FileValidator');

class PacProvider extends FileValidator {
  constructor(entityId, tin, npi, providerRelationshipCode, mipsEcIndicator, performanceYear, snapshot, execution) {
    super();
    this.entity_id = entityId;
    this.tin = tin;
    this.npi = npi;
    this.provider_relationship_code = providerRelationshipCode;
    if (mipsEcIndicator === undefined) {
      this.mips_ec_indicator = '';
    } else {
      this.mips_ec_indicator = mipsEcIndicator;
    }
    this.performance_year = performanceYear;
    this.snapshot = snapshot;
    this.execution = execution;
  }

  static get fileType() {
    return 'pac-provider';
  }

  static get urlForTypeMappings() {
    return 'pac/provider';
  }

  static get schemaMap() {
    return {
      'APM Entity ID': 'entity_id',
      'Provider TIN': 'tin',
      'Provider NPI': 'npi',
      'Provider Relationship Code': 'provider_relationship_code',
      'Performance Year': 'performance_year',
      'MIPS EC Indicator': 'mips_ec_indicator',
      Snapshot: 'snapshot',
      Execution: 'execution'
    };
  }

  static fromRawFile(fileData) {
    return new PacProvider(
      fileData.entity_id, fileData.tin, fileData.npi, fileData.provider_relationship_code, fileData.mips_ec_indicator,
      fileData.performance_year, fileData.snapshot, fileData.execution);
  }

  static validateHeader(headerLine) {
    return super.validateHeaderUtility({ headerLine, fileType: 'PRVD' });
  }

  static validateParsedTrailer(trailer, header, recordsCount) {
    return super.validateParsedTrailerUtility({
      header, trailer, recordsCount, fileType: 'PRVD'
    });
  }

  static get parser() {
    const headers = ['Record Identifier', 'Date', 'File Type', 'Performance Year', 'Snapshot', 'Execution'];
    const trailers = [
      'Record Identifier', 'Date', 'File Type', 'Performance Year', 'Snapshot', 'Execution', 'Record Count'];
    const detailHeaders = ['Record Identifier', 'APM Entity ID', 'Provider TIN',
      'Provider NPI', 'Provider Relationship Code', 'MIPS EC Indicator'];

    return warningCallback => headerDetailTrailerFileParser(headers, detailHeaders, trailers, warningCallback);
  }
}

const mixins = {
  getPerformanceYear,
  getSnapshot,
  getExecution
};

Object.assign(PacProvider, mixins);
module.exports = PacProvider;
