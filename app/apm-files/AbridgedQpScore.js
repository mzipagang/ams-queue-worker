const recordIdentifierFileParser = require('../services/fileParser/recordIdentifierFileParser');
const { getPerformanceYear, getRunSnapshot, getSnapshotId } = require('../utils/headerUtilities');
/* istanbul ignore next */
/* This is the same as the PacQpScore file - per Andy we will remove since they're identical */
/* Will remove in a future sprint, leaving for now 2018-09-28 - JM */
class AbridgedQpScore {
  constructor(
    entityId, tin, npi, paymentThresholdScore, patientThresholdScore, performanceYear, runSnapshot, snapshotId
  ) {
    this.entity_id = entityId;
    this.tin = tin;
    this.npi = npi;
    this.payment_threshold_score = paymentThresholdScore;
    this.patient_threshold_score = patientThresholdScore;
    this.performance_year = performanceYear;
    this.run_snapshot = runSnapshot;
    this.snapshot_id = snapshotId;
  }

  static get fileType() {
    return 'abridged_qp_score';
  }

  static get urlForTypeMappings() {
    return 'pac/abridged_qp_score';
  }

  static get schemaMap() {
    return {
      'APM Entity ID': 'entity_id',
      'Provider TIN': 'tin',
      'Provider NPI': 'npi',
      'Payment Threshold Score': 'payment_threshold_score',
      'Patient Threshold Score': 'patient_threshold_score',
      'Performance Year': 'performance_year',
      'Run Snapshot': 'run_snapshot',
      'Snapshot ID': 'snapshot_id'
    };
  }

  static fromRawFile(fileData) {
    return new AbridgedQpScore(fileData.entity_id, fileData.tin, fileData.npi, fileData.payment_threshold_score,
      fileData.patient_threshold_score, fileData.performance_year, fileData.run_snapshot, fileData.snapshot_id);
  }

  static validateHeader() {
    // NOTE: This file type is the same as the PacQpScore file, will be removing in a future sprint
    return false;
    // return !!(headerLine && headerLine.trim().match(/(H)\|([0-9]{8})\|QPSC\|PY([0-9]{4})\|[PISF]\|([^|]+)$/));
  }

  static get parser() {
    return recordIdentifierFileParser('|', ['Record Identifier', 'APM Entity ID', 'Provider TIN', 'Provider NPI',
      'Payment Threshold Score', 'Patient Threshold Score']).parser;
  }
}

const mixins = {
  getPerformanceYear,
  getRunSnapshot,
  getSnapshotId
};
Object.assign(AbridgedQpScore, mixins);
module.exports = AbridgedQpScore;
