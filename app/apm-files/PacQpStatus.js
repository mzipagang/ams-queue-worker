const headerDetailTrailerFileParser = require('../services/fileParser/headerDetailTrailerFileParser');
const { getPerformanceYear, getSnapshot, getExecution } = require('../utils/headerUtilities');
const FileValidator = require('../utils/FileValidator');

class PacQpStatus extends FileValidator {
  constructor(npi, qpStatus, performanceYear, snapshot, execution) {
    super();
    this.npi = npi;
    this.qp_status = qpStatus;
    this.performance_year = performanceYear;
    this.snapshot = snapshot;
    this.execution = execution;
  }

  static get fileType() {
    return 'pac-qp_status';
  }

  static get urlForTypeMappings() {
    return 'pac/qp_status';
  }

  static get schemaMap() {
    return {
      NPI: 'npi',
      'QP Status': 'qp_status',
      'Performance Year': 'performance_year',
      Snapshot: 'snapshot',
      Execution: 'execution'
    };
  }

  static fromRawFile(fileData) {
    return new PacQpStatus(
      fileData.npi, fileData.qp_status, fileData.performance_year, fileData.snapshot, fileData.execution);
  }

  static validateHeader(headerLine) {
    return super.validateHeaderUtility({ headerLine, fileType: 'QPST' });
  }

  static validateParsedTrailer(trailer, header, recordsCount) {
    return super.validateParsedTrailerUtility({
      header, trailer, recordsCount, fileType: 'QPST'
    });
  }

  static get parser() {
    const headers = ['Record Identifier', 'Date', 'File Type', 'Performance Year', 'Snapshot', 'Execution'];
    const trailers = [
      'Record Identifier', 'Date', 'File Type', 'Performance Year', 'Snapshot', 'Execution', 'Record Count'];
    const detailHeaders = ['Record Identifier', 'NPI', 'QP Status'];

    return warningCallback => headerDetailTrailerFileParser(headers, detailHeaders, trailers, warningCallback);
  }
}

const mixins = {
  getPerformanceYear,
  getSnapshot,
  getExecution
};

Object.assign(PacQpStatus, mixins);
module.exports = PacQpStatus;
