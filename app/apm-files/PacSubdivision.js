const headerDetailTrailerFileParser = require('../services/fileParser/headerDetailTrailerFileParser');
const { getPerformanceYear, getSnapshot, getExecution } = require('../utils/headerUtilities');
const FileValidator = require('../utils/FileValidator');

class PacSubdivision extends FileValidator {
  constructor(id, apmId, name, advancedApmFlag, mipsApmFlag, performanceYear, snapshot, execution) {
    super();
    this.id = id;
    this.apm_id = apmId;
    this.name = name;
    this.advanced_apm_flag = advancedApmFlag;
    this.mips_apm_flag = mipsApmFlag;
    this.performance_year = performanceYear;
    this.snapshot = snapshot;
    this.execution = execution;
  }

  static get fileType() {
    return 'pac-subdivision';
  }

  static get urlForTypeMappings() {
    return 'pac/subdivision';
  }

  static get schemaMap() {
    return {
      'APM ID': 'apm_id',
      'SubDivision ID': 'id',
      'SubDivision Name': 'name',
      'Advanced APM Flag': 'advanced_apm_flag',
      'MIPS APM Flag': 'mips_apm_flag',
      'Performance Year': 'performance_year',
      Snapshot: 'snapshot',
      Execution: 'execution'
    };
  }

  static fromRawFile(fileData) {
    return new PacSubdivision(fileData.id, fileData.apm_id, fileData.name, fileData.advanced_apm_flag,
      fileData.mips_apm_flag, fileData.performance_year, fileData.snapshot, fileData.execution);
  }

  static validateHeader(headerLine) {
    return super.validateHeaderUtility({ headerLine, fileType: 'SUB' });
  }

  static validateParsedTrailer(trailer, header, recordsCount) {
    return super.validateParsedTrailerUtility({
      header, trailer, recordsCount, fileType: 'SUB'
    });
  }

  static get parser() {
    const headers = ['Record Identifier', 'Date', 'File Type', 'Performance Year', 'Snapshot', 'Execution'];
    const trailers =
    ['Record Identifier', 'Date', 'File Type', 'Performance Year', 'Snapshot', 'Execution', 'Record Count'];
    const detailHeaders = ['Record Identifier', 'APM ID', 'SubDivision ID',
      'SubDivision Name', 'Advanced APM Flag', 'MIPS APM Flag'];

    return warningCallback => headerDetailTrailerFileParser(headers, detailHeaders, trailers, warningCallback);
  }
}

const mixins = {
  getPerformanceYear,
  getSnapshot,
  getExecution
};

Object.assign(PacSubdivision, mixins);
module.exports = PacSubdivision;
