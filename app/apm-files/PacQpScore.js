const headerDetailTrailerFileParser = require('../services/fileParser/headerDetailTrailerFileParser');
const { getPerformanceYear, getSnapshot, getExecution } = require('../utils/headerUtilities');
const FileValidator = require('../utils/FileValidator');

class PacQpScore extends FileValidator {
  constructor(entityId, tin, npi, paymentThresholdScore, patientThresholdScore, performanceYear, snapshot, execution) {
    super();
    this.entity_id = entityId;
    this.tin = tin;
    this.npi = npi;
    this.payment_threshold_score = paymentThresholdScore;
    this.patient_threshold_score = patientThresholdScore;
    this.performance_year = performanceYear;
    this.snapshot = snapshot;
    this.execution = execution;
  }

  static get fileType() {
    return 'pac-qp_score';
  }

  static get urlForTypeMappings() {
    return 'pac/qp_score';
  }

  static get schemaMap() {
    return {
      'APM Entity ID': 'entity_id',
      'Provider TIN': 'tin',
      'Provider NPI': 'npi',
      'Payment Threshold Score': 'payment_threshold_score',
      'Patient Threshold Score': 'patient_threshold_score',
      'Performance Year': 'performance_year',
      Snapshot: 'snapshot',
      Execution: 'execution'
    };
  }

  static fromRawFile(fileData) {
    return new PacQpScore(fileData.entity_id, fileData.tin, fileData.npi, fileData.payment_threshold_score,
      fileData.patient_threshold_score, fileData.performance_year, fileData.snapshot, fileData.execution);
  }

  static validateHeader(headerLine) {
    return super.validateHeaderUtility({ headerLine, fileType: 'QPSC' });
  }

  static validateParsedTrailer(trailer, header, recordsCount) {
    return super.validateParsedTrailerUtility({
      header, trailer, recordsCount, fileType: 'QPSC'
    });
  }

  static get parser() {
    const headers = ['Record Identifier', 'Date', 'File Type', 'Performance Year', 'Snapshot', 'Execution'];
    const trailers = [
      'Record Identifier', 'Date', 'File Type', 'Performance Year', 'Snapshot', 'Execution', 'Record Count'];
    const detailHeaders = ['Record Identifier', 'APM Entity ID', 'Provider TIN', 'Provider NPI',
      'Payment Threshold Score', 'Patient Threshold Score'];

    return warningCallback => headerDetailTrailerFileParser(headers, detailHeaders, trailers, warningCallback);
  }
}

const mixins = {
  getPerformanceYear,
  getSnapshot,
  getExecution
};

Object.assign(PacQpScore, mixins);
module.exports = PacQpScore;
