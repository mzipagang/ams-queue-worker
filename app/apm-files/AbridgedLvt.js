const recordIdentifierFileParser = require('../services/fileParser/recordIdentifierFileParser');
const { getPerformanceYear, getRunNumber, getSnapshotId } = require('../utils/headerUtilities');
/* istanbul ignore next */
/* This is the same as the PacLVT file - per Andy we will remove since they're identical */
/* Will remove in a future sprint, leaving for now 2018-09-28 - JM */
class AbridgedLvt {
  constructor(entityId, status, patients, payments, smallStatus, performanceYear, runNumber, snapshotId) {
    this.entity_id = entityId;
    this.status = status;
    this.patients = patients;
    this.payments = payments;
    this.small_status = smallStatus;
    this.performance_year = performanceYear;
    this.run_number = runNumber;
    this.snapshot_id = snapshotId;
  }

  static get fileType() {
    return 'abridged_lvt';
  }

  static get urlForTypeMappings() {
    return 'pac/abridged_lvt';
  }

  static get schemaMap() {
    return {
      'APM Entity ID': 'entity_id',
      'APM LVT Status': 'status',
      'Low Volume Bene Count': 'patients',
      'Low Volume Part B Expenditures': 'payments',
      'Small Status APM Entity Code': 'small_status',
      'Performance Year': 'performance_year',
      'Run Number': 'run_number',
      'Snapshot ID': 'snapshot_id'
    };
  }

  static fromRawFile(fileData) {
    return new AbridgedLvt(fileData.entity_id, fileData.status, fileData.patients, fileData.payments,
      fileData.small_status, fileData.performance_year, fileData.run_number, fileData.snapshot_id);
  }

  static validateHeader() {
    // NOTE: This file type is the same as the PacLVT file, will be removing in a future sprint
    return false;
    // return !!(headerLine && headerLine.trim().match(/(H)\|([0-9]{8})\|ALVT\|PY([0-9]{4})\|[0-9]\|([^|]+)$/));
  }

  static get parser() {
    return recordIdentifierFileParser('|', ['Record Identifier', 'APM Entity ID', 'APM LVT Status',
      'Low Volume Part B Expenditures', 'Low Volume Bene Count', 'Small Status APM Entity Code']).parser;
  }
}

const mixins = {
  getPerformanceYear,
  getRunNumber,
  getSnapshotId
};

Object.assign(AbridgedLvt, mixins);
module.exports = AbridgedLvt;
