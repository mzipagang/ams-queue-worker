const headerDetailTrailerFileParser = require('../services/fileParser/headerDetailTrailerFileParser');
const { getPerformanceYear, getSnapshot, getExecution } = require('../utils/headerUtilities');
const FileValidator = require('../utils/FileValidator');

class PacEntity extends FileValidator {
  constructor(id, apmId, subdivId, tin, name, performanceYear, snapshot, execution) {
    super();
    this.id = id;
    this.apm_id = apmId;
    this.subdiv_id = subdivId;
    this.tin = tin;
    this.name = name;
    this.performance_year = performanceYear;
    this.snapshot = snapshot;
    this.execution = execution;
  }

  static get fileType() {
    return 'pac-entity';
  }

  static get urlForTypeMappings() {
    return 'pac/entity';
  }

  static get schemaMap() {
    return {
      'APM ID': 'apm_id',
      'SubDivision ID': 'subdiv_id',
      'APM Entity ID': 'id',
      'APM Entity TIN': 'tin',
      'APM Entity Name': 'name',
      'Performance Year': 'performance_year',
      Snapshot: 'snapshot',
      Execution: 'execution'
    };
  }

  static fromRawFile(fileData) {
    return new PacEntity(fileData.id, fileData.apm_id, fileData.subdiv_id, fileData.tin,
      fileData.name, fileData.performance_year, fileData.snapshot, fileData.execution);
  }

  static validateHeader(headerLine) {
    return super.validateHeaderUtility({
      headerLine,
      fileType: 'APME'
    });
  }

  static validateParsedTrailer(trailer, header, recordsCount) {
    return super.validateParsedTrailerUtility({
      header, trailer, recordsCount, fileType: 'APME'
    });
  }

  static get parser() {
    const headers = ['Record Identifier', 'Date', 'File Type', 'Performance Year', 'Snapshot', 'Execution'];
    const detailHeaders = ['Record Identifier', 'APM ID', 'SubDivision ID', 'APM Entity ID',
      'APM Entity TIN', 'APM Entity Name'];
    const trailers = [
      'Record Identifier', 'Date', 'File Type', 'Performance Year', 'Snapshot', 'Execution', 'Record Count'];

    return warningCallback => headerDetailTrailerFileParser(headers, detailHeaders, trailers, warningCallback);
  }
}

const mixins = {
  getPerformanceYear,
  getSnapshot,
  getExecution
};

Object.assign(PacEntity, mixins);

module.exports = PacEntity;
