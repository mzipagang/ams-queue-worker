const moment = require('moment');
const headerDetailTrailerFileParser = require('../services/fileParser/headerDetailTrailerFileParser');

class Beneficiary {
  constructor(entityId, linkKey, hicn, mbi, dob, gender, startDate, endDate) {
    this.entity_id = entityId;
    this.link_key = linkKey;
    this.hicn = hicn;
    this.mbi = mbi;
    this.dob = dob;
    this.gender = gender;
    this.start_date = startDate;
    this.end_date = endDate;
  }

  static get fileType() {
    return 'beneficiary';
  }

  static get urlForTypeMappings() {
    return 'apm/beneficiary';
  }

  static get schemaMap() {
    return {
      'Entity ID': 'entity_id',
      'Bene Link Key': 'link_key',
      'Beneficiary HICN': 'hicn',
      'Beneficiary MBI': 'mbi',
      'Beneficiary Date of Birth': 'dob',
      'Beneficiary Gender': 'gender',
      'Beneficiary Start Date': 'start_date',
      'Beneficiary End Date': 'end_date'
    };
  }

  static fromRawFile(fileData) {
    let startDate = null;
    let endDate = null;
    let dob; // if there is no dob save as undefined; if dob date is invalid saves as null
    // dob validation needs to distinguish the two, whereas start and end date do not

    if (fileData.start_date) {
      startDate = moment.utc(fileData.start_date, 'YYYYMMDD');
      startDate = startDate.isValid() ? startDate.format('YYYY-MM-DD') : null;
    }

    if (fileData.end_date) {
      endDate = moment.utc(fileData.end_date, 'YYYYMMDD');
      endDate = endDate.isValid() ? endDate.format('YYYY-MM-DD') : null;
    }

    if (fileData.dob) {
      dob = moment.utc(fileData.dob, 'YYYYMMDD');
      dob = dob.isValid() ? dob.format('YYYY-MM-DD') : null;
    }

    return new Beneficiary(
      fileData.entity_id,
      fileData.link_key,
      fileData.hicn,
      fileData.mbi,
      dob,
      fileData.gender,
      startDate,
      endDate
    );
  }

  static validateHeader(headerLine) {
    return !!(headerLine && headerLine.trim().match(/(H)\|([0-9]{8})\|BENE\|\w{2}$/));
  }

  static validateParsedTrailer(trailer, header, recordsCount) {
    if (trailer['File Type'] !== 'BENE') {
      return {
        error: true,
        warning: false,
        message: 'File Type must be "BENE".'
      };
    }

    if (trailer['APM ID'] !== header['APM ID']) {
      return {
        error: true,
        warning: false,
        message: 'APM ID must match APM ID in Header Record and exist in AMS.'
      };
    }
    if (trailer['Record Count'] !== recordsCount.toString()) {
      return {
        error: false,
        warning: true,
        message: 'Record count in the Trailer does not equal the number of Detail records'
      };
    }
    if (trailer['Record Identifier'] !== 'T') {
      return {
        error: true,
        warning: false,
        message: 'Invalid trailer in file.'
      };
    }
    return {
      error: false,
      warning: false,
      message: ''
    };
  }

  static get parser() {
    const headers = ['Record Identifier', 'Date', 'File Type', 'APM ID'];
    const trailers = ['Record Identifier', 'Date', 'File Type', 'APM ID', 'Record Count'];
    const detailHeaders = [
      'Record Identifier',
      'Entity ID',
      'Bene Link Key',
      'Beneficiary HICN',
      'Beneficiary MBI',
      'Beneficiary Date of Birth',
      'Beneficiary Gender',
      'Beneficiary Start Date',
      'Beneficiary End Date'
    ];

    return warningCallback =>
      headerDetailTrailerFileParser(headers, detailHeaders, trailers, warningCallback);
  }
}

module.exports = Beneficiary;
