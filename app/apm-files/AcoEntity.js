const moment = require('moment');
const { padLeftIfNumber } = require('../utils');

const characterDelimitedFileParser = require('../services/fileParser/characterDelimitedFileParser');
const ApmEntity = require('./ApmEntity');

const IgnoreApmId = require('../FilterStrategies/IgnoreApmId');

class AcoEntity extends ApmEntity {

  constructor(id, apmId, subdivId, startDate, endDate, name, dba, type, tin, additionalInformation, address) {
    super(id, apmId, subdivId, startDate, endDate, name, dba, type, tin, null, null, additionalInformation, address);
  }

  static get fileType() {
    return 'aco-entity';
  }

  static get urlForTypeMappings() {
    return 'apm/entity';
  }

  static get headers() {
    return [
      'APM ID',
      'PYMT MODEL CD',
      'CCO ID',
      'EFCTV DT',
      'TRMNTN DT',
      'ORG NAME',
      'DBA ORG NAME',
      'ENTITY TYPE',
      'ORG TIN NUM',
      'OTH TIN NUM',
      'ENTITY ADDNL INFO',
      'ST ADR1',
      'ST ADR2',
      'CITY NAME',
      'SSA STD STATE CD',
      'ZIP CD',
      'ZIP PLUS 4 CD'
    ];
  }

  static get schemaMap() {
    return {
      'APM ID': 'apm_id',
      'PYMT MODEL CD': 'subdiv_id',
      'CCO ID': 'id',
      'EFCTV DT': 'start_date',
      'TRMNTN DT': 'end_date',
      'ORG NAME': 'name',
      'DBA ORG NAME': 'dba',
      'ENTITY TYPE': 'type',
      'ORG TIN NUM': 'tin',
      'ENTITY ADDNL INFO': 'additional_information',
      'ST ADR1': 'address.address_1',
      'ST ADR2': 'address.address_2',
      'CITY NAME': 'address.city',
      'SSA STD STATE CD': 'address.state',
      'ZIP CD': 'address.zip5',
      'ZIP PLUS 4 CD': 'address.zip4'
    };
  }

  static fromRawFile(fileData) {
    const id = padLeftIfNumber(fileData.id, 2);
    const apmId = padLeftIfNumber(fileData.apm_id, 2);
    const subdivId = fileData.subdiv_id ? padLeftIfNumber(fileData.subdiv_id, 2) : null;
    const address = fileData.address;

    let startDate = null;
    let endDate = null;

    if (fileData.start_date) {
      startDate = moment.utc(fileData.start_date, 'MM/DD/YYYY');
      startDate = startDate.isValid() ? startDate.format('YYYY-MM-DD') : null;
    }

    if (fileData.end_date) {
      endDate = moment.utc(fileData.end_date, 'MM/DD/YYYY');
      endDate = endDate.isValid() ? endDate.format('YYYY-MM-DD') : null;
    }

    return new AcoEntity(id, apmId, subdivId, startDate, endDate, fileData.name, fileData.dba, fileData.type,
        fileData.tin, fileData.additional_information, address);
  }

  static validateHeader(headerLine) {
    if (!headerLine) {
      return false;
    }

    return headerLine.split('|').length === 17;
  }

  static get parser() {
    return characterDelimitedFileParser('|', this.headers).parser;
  }

  static get filters() {
    return [IgnoreApmId];
  }
}

module.exports = AcoEntity;
