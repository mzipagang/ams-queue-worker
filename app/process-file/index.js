const fs = require('fs');
const tmp = require('tmp');

const config = require('../config');

const { logger, awsService, fileService, emailService, rowsProcessor } = require('../services');
const { readFirstLineOfStream } = require('../utils');
const { getFileType, isManualFile } = require('../apm-files');
const { getAMSModels } = require('../services/dataWorkerUtils');

const batch = require('through-batch');
const split2 = require('split2');

const lineNumber = require('../utils/transforms/lineNumber');
const parse = require('../utils/transforms/parse');
const filterRows = require('../utils/transforms/filterRows');
const validateTrailer = require('../utils/transforms/validateTrailer');
const sendToDataWorker = require('../utils/transforms/sendToDataWorker');
const addMetaData = require('../utils/transforms/addMetaData');
const clearEmptyStrings = require('../utils/transforms/clearEmptyStrings');
const scanFile = require('../utils/transforms/scanFile');
const isValidExecutionNumber = require('../utils/transforms/isValidExecutionNumber');

tmp.setGracefulCleanup();

module.exports = async (req, done) => {
  const localFile = tmp.fileSync();
  const { key: s3Filename, bucket: s3bucketName, infected: fileInfected } = req;
  let fileValidations = [];

  // s3Filename (which is the SQS message's key) has a structure of {fileImportGroupId}/{fileId}/{filename.ext}
  const [fileImportGroupId, fileId] = s3Filename.split('/');

  try {
    logger.info(`Processing ${s3Filename}`);

    // TODO: there should be middleware for this that filters out infected files
    if (fileInfected) {
      logger.info(`Deleting infected file ${s3Filename}`);
      await awsService.deleteS3FileInBucket(s3bucketName, s3Filename);
      await emailService.sendStatus(fileId, s3Filename, 'File has been detected as a virus, deleting infected file');
      await fileService.updateFile(fileImportGroupId, fileId, { status: 'error', error: 'Virus detected.' });
      done();
      return;
    }

    const fileImportGroup = await fileService.getFileImportGroup(fileImportGroupId);

    if (!fileImportGroup) {
      logger.info(`File Import Group does not exist ${s3Filename}`);
      done();
      return;
    }

    if (fileImportGroup.status !== 'open') {
      logger.info(`File is not available for modifications ${s3Filename}`);
      done();
      return;
    }

    await fileService.transferFileToDataWorker(s3bucketName, s3Filename);

    await fileService.updateFile(fileImportGroupId, fileId, {
      file_location: `${config.DATA_WORKER.S3_BUCKET}:${s3Filename}`
    });

    // this downloads from the data worker and outputs into `localFile.name`
    // do not be caught off guard that it does not await a value
    await fileService.downloadFileFromDataWorker(s3Filename, localFile.name);

    await emailService.sendStatus(fileId, s3Filename, 'File has successfully uploaded to s3, processing file');

    const header = await readFirstLineOfStream(fs.createReadStream(localFile.name));

    const invalidHeader = async (message) => {
      logger.info(`${message.slice(0, -1)} ${s3Filename}`);
      await emailService.sendStatus(fileId, s3Filename, message);
      await fileService.updateFile(fileImportGroupId, fileId, { status: 'error', error: message });
      done();
    };

    const fileType = getFileType(header);

    if (fileType.error) {
      await invalidHeader(fileType.message);
      return;
    }

    const validExecution = await isValidExecutionNumber(header, fileType.instance);
    if (!validExecution) {
      await invalidHeader('Execution number must be greater than previous');
      return;
    }

    const fileData = {
      status: 'processing',
      import_file_type: fileType.name,
      apm_id: fileType.instance.getApmId && fileType.instance.getApmId(header),
      performance_year: fileType.instance.getPerformanceYear && fileType.instance.getPerformanceYear(header),
      run: fileType.instance.getRunNumber && fileType.instance.getRunNumber(header),
      snapshot: fileType.instance.getSnapshot && fileType.instance.getSnapshot(header),
      execution: fileType.instance.getExecution && fileType.instance.getExecution(header)
    };

    const file = await fileService.updateFile(fileImportGroupId, fileId, fileData);

    const trailerValidationErrorCallback = (message) => {
      logger.info(`${message.slice(0, -1)} ${s3Filename}`);
      return Promise.all([
        emailService.sendStatus(fileId, s3Filename, message),
        fileService.updateFile(fileImportGroupId, fileId, { status: 'error', error: message })
      ]);
    };

    const needsScan = isManualFile(fileType.name);
    const detailWarningCallback = needsScan ? (message, row) => {
      fileValidations.push({ message, row: row.lineNumber });
    } : null;

    const parseFunction = fileType.instance.parser(detailWarningCallback);

    const onLineParseError = rowsProcessor.onError.bind(null, fileImportGroupId, fileId);

    if (fileType.name === 'beneficiary') {
      const headerValues = parseFunction({ text: header }).data;
      const modelIds = (await getAMSModels()).map(model => model.id);

      /** APM ID 23 has a special meaning:
       * APM ID 23 is not defined in AMS, which is intentional. There are some operational circumstances where
       * there is not a 1-to-1 match between the MDM APM ID and the AMS APM ID. In this case, MDM APM ID of 22 equates
       * to AMS APM ID of 22, SubDiv 01; MDM APM ID of 23 equates to AMS APM ID of 22, SubDiv 02. When processing the MDM file,
       * if the MDM APM ID is 23, these records are not discarded, but rather
       * replaced with the APM ID of 22 prior to validation.
       */
      const apmId = headerValues['APM ID'] === '23' ? '22' : headerValues['APM ID'];

      if (modelIds.indexOf(apmId) === -1) {
        const errorMessage = `No APM Model with ID of ${apmId} found`;
        logger.error(errorMessage);
        await fileService.updateFile(fileImportGroupId, fileId, { status: 'error', error: errorMessage });
        done();
        return;
      }
    }

    logger.info(`Starting line processing ${s3Filename}`);
    const process = new Promise(async (resolve, reject) => {
      fs.createReadStream(localFile.name)
        .pipe(split2())       // split file by line
        .pipe(lineNumber())     // keep track of line numbers, adds fields 'text' and 'lineNumber'
        .pipe(scanFile(needsScan))
        .on('warning', message => fileValidations.push({ message }))
        .pipe(parse(parseFunction, onLineParseError))          // parse each line (i.e. by splitting on the '|' character)
        .pipe(await filterRows(fileType.instance.filters)) // filter rows -- each file has its own unique rules
        .pipe(validateTrailer(fileType.instance.validateParsedTrailer, trailerValidationErrorCallback))
        .on('warning', message => fileValidations.push({ message }))
        .on('error', () => resolve())
        .pipe(addMetaData(fileData))    // adds metadata to each row based on the file itself; only applies to certain file types
        .pipe(clearEmptyStrings())
        .pipe(batch(config.DATA_WORKER.BATCH_LINE_COUNT))
        .pipe(sendToDataWorker(fileImportGroupId, fileId, file, onLineParseError, fileType.instance))
        .on('finish', async () => {
          logger.info(`Finishing up processing ${s3Filename}`);

          if (fileType.name === 'pac-provider') {
            await fileService.removeDuplicatesFromPacProvider(fileImportGroupId, fileId);
          }

          if (fileType.name.startsWith('pac-') || fileType.name.startsWith('abridged_')) {
            fileValidations = [];
          }

          if (JSON.stringify(fileValidations).length > config.MAX_ERROR_BYTES) {  // If we have more then 49mb of file validations, we will fail since we can't upload that much.
            fileValidations = [
              { message: 'Error in file uploading and processing, file had too many validation messages to upload.' }
            ];
            reject(
              new Error('Error in file uploading and processing, file had too many validation messages to upload.')
            );
          }

          await fileService.updateFile(fileImportGroupId, fileId, {
            status: 'finished',
            fileValidations,
            error: fileValidations.length > 0 ? fileValidations[0].message : ''
          });
          await emailService.sendStatus(fileId, s3Filename, 'File has been successfully processed');
          resolve();
        });
    });

    await process;
    done();
  } catch (err) {
    try {
      logger.error(err);
      const error = 'Error in file uploading and processing';
      await fileService.updateFile(fileImportGroupId, fileId, { status: 'error', error, fileValidations });
      await emailService.sendStatus(fileId, s3Filename, error);
      done(err);
    } catch (ex) {
      logger.error(ex);
      done(ex);
    }
  }

  logger.info(`Finished processing ${s3Filename}`);
  localFile.removeCallback();
};
